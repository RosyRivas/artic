@extends('layouts.app')
@section('titulo', 'Reservar material')

@section('css')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />

<style>
  .select2-selection__rendered {
      line-height: 38px !important;
  }

  .select2-container .select2-selection--single {
      height: 38px !important;
  }

  .select2-selection__arrow {
      height: 36px !important;
  }

  .selection {
    width: 100% !important;
    line-height: 38px !important;
  }
</style>
@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>

<script>
    $(document).ready(function() {
        $('.select2').select2({
            placeholder: 'Seleccione una opción',
            theme: 'classic',
            width: '100%',
        });
    });
</script>
@endsection

@section('contenido')
    <form class="card" action="{{route('asignacion.reservar', $asignacion)}}" method="post">
        @csrf
        <div class="card-header">
          <p>
               Usted está por aceptar una asignación.
          </p>
        </div>
        <div class="card-body">
        <div class="form-group">
              <label for="motivo">Indique los materiales que necesita para la prestación</label>

                <table class="col-12 m-0 w-100 table table-hover">
                  <thead>
                    <tr>
                        <th>Material</th>
                        <th>Cantidad</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($asignacion->servicio_contratado->servicio->recursos as $r)
                    <tr>
                        <td>{{$r->material->nombre}}</td>
                        <td>
                               <input type="number" name="recursos[{{$r->material->id}}]" id="recursos[{{$r->material->id}}]" placeholder="Seleccione la cantidad de materiales" class="form-control" min="0" max="{{$r->cantidad}}" step="1">

                                </select>
                        </td>
                    </tr>
                    @endforeach

                  </tbody>
                </table>


      </div>
      <div class="d-flex justify-content-center mt-4 ">
        <button type="submit" class="btn btn-primary mx-2" >Enviar</button>
      </div>
    </div>
    </form>
@endsection
