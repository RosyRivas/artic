@extends('layouts.app')
@section('titulo', 'Rechazar asignacion')
@section('contenido')
    <form class="card" action="{{route('asignacion.rechazar', $asignacion)}}" method="post">
        @csrf
        <div class="card-header">
          <p>
            Estas por rechazar la prestación del servicio {{$asignacion->servicio_contratado->servicio->nombre}}
          </p>
        </div>
        <div class="card-body">

        <div class="form-group">
              <label for="motivo">Motivo del rechazo</label>
              <textarea class="form-control @error('motivo') is-invalid @enderror" id="motivo" name="motivo" required placeholder="Detalle los motivos del rechazo">{{old('motivo')}}</textarea>
              @error('motivo')
                <span class="invalid-feedback" role="alert">
                    <strong>{{$message}}</strong>
                </span>
              @enderror
        </div>

        <div class="d-flex justify-content-center mt-4 ">
              <button type="submit" class="btn btn-primary mx-2" >Enviar</button>
        </div>

      </div>
    </form>
@endsection
