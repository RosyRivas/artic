@extends('layouts.app')

@section('titulo', 'Mis asignaciones')


@section('contenido')

    <div class="container-fluid">
        <div class="row">
            <div class="h2 col">Mis asignaciones </div>
        </div>

    </div>
    <div class="conteiner-fluid">
          @include('layouts.mensaje')
          <div class="container-fluid mt-3">
          <table class="col-12 m-0 w-100 table table-hover">
                <thead>
                  <tr>
                      <th>Fecha</th>
                      <th>Nombre del servicio</th>
                      <th class="text-right">Monto</th>
                      <th class="text-right">Accion</th>
                  </tr>
                </thead>
                @forelse($asignaciones as $a)
                <tbody>
                  <tr>
                      <td>{{date('d/m/y', strtotime($a->servicio_contratado->fecha_prestacion))}}</td>
                      <td>{{$a->servicio_contratado->servicio->nombre}}</td>

                      <td class="text-right"> ${{number_format($a->servicio_contratado->servicio->costo, 2,',', '.')}}</td>
                      <td class="text-right">
                            <button type="submit" class="btn btn-primary mx-2" class="btn btn-primary" data-toggle="modal" data-target="#modal{{$a->id}}"><i class="fas fa-eye"></i></button>

                      </td>
                      <td>
                          <div class="modal fade" id="modal{{$a->id}}" tabindex="-1" aria-hidden="true">
                            <div class="modal-dialog">
                              <form class="modal-content" action ="{{route('socios.asignaciones',$a->servicio_contratado)}}" method="post">
                                <div class="modal-header">
                                  <h3 class="modal-title">{{$a->servicio_contratado->servicio->nombre}}</h3>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body">
                                    @csrf
                                    <h3>Detalles </h3>
                                    <p>
                                        <h5>
                                            Cliente: {{$a->servicio_contratado->pedido->user->name}} {{$a->servicio_contratado->pedido->user->apellido}}
                                        </h5>
                                    </p>
                                    <p>
                                        <h5>
                                            Contacto: {{$a->servicio_contratado->pedido->user->email}}
                                        </h5>
                                    </p>
                                    <p>
                                        <h5>
                                            Materiales necesarios
                                        </h5>
                                    </p>
                                          <ul>
                                            @foreach($a->recursos_prestados() as $r)
                                                  <li>{{$r->cantidad}} {{$r->material->nombre}}</li>
                                            @endforeach
                                          </ul>
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Volver</button>

                                  @if( ($a->servicio_contratado->fecha_prestacion < now('America/Argentina/Buenos_Aires'))  && (! $a->servicio_contratado->terminado) )
                                      <button type="submit"  class="btn btn-primary">Terminar servicio</button>
                                  @endif
                                </div>
                            </form>
                            </div>
                          </div>
                      </td>
                      @empty
                          <tr>
                              <td colspan="4" class="text-center font-italic" >No hay asignaciones pendientes</td>
                          </tr>
                      @endforelse
                  </tr>
                </tbody>
          </table>
          </div>
      </div>

@endsection
