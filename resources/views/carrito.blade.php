@extends('layouts.app')

@section('titulo', 'Carrito')

@section('contenido')
<div class="container-fluid">
  <div class="row">
      <div class="h2 col">Mi carrito</div>
  </div>
  <div class="row my-3">
    <div class="col">
      @include('layouts.mensaje')
    </div>
  </div>
  <form class="row mt-3" action="{{route('carrito.pagar')}}" method="POST">
    @csrf
      <table class="col-12 m-0 display responsive nowrap w-100 table table-hover">
        <thead>
          <tr>
              <th>Imagen</th>
              <th>Nombre</th>
              <th class="text-right">Precio</th>
              <th class="text-center">Fecha</th>
              <th>Accion</th>
          </tr>
        </thead>
        <tbody>
          @foreach($items as $id => $i)
          <tr id="fila{{$id}}">

              <td><img src="{{asset('images/servicios/' . $i['imagen'])}}" alt="{{$i['nombre']}}" width="50px"></td>
              <td id= "nombre">
                  {{$i['nombre']}}
                  <input type='hidden' name="ids[]" id="ids[]" value="{{$id}}">
              </td>
              <td id="costos" class="text-right">
                <input type='hidden' name="precios[]" id="precios[]" value="{{$i['precio']}}" required>
                $ {{number_format($i['precio'], 2, ',', '')}}
              </td>
              <td>
                <input type="date" class="form-control" id="fecha[]"
                name="fecha[]" value="{{old('fecha.'. $id, $i['fecha'])}}"
                min="{{now()->format('Y-m-d')}}" required>
              </td>
              <td>
                  <button type="button" class="btn btn-danger" onclick="eliminar({{$id}});"><i class="fas fa-trash-alt fa-fw"></i></button>
              </td>

          </tr>
          @endforeach
          <tr>
            <th colspan="3">TOTAL</th>
            <th colspan="2"><h5 id="total">$ {{number_format($total, 2, ',', '')}}</h5></th>
          </tr>
          <tr>
            <th colspan="3">Seña a pagar</th>
            <th colspan="2"><h4 id="senia">$ {{number_format($total/2, 2, ',', '')}}</h4></th>
          </tr>
        </tbody>
      </table>
      <div class="col-12 text-center pt-5">
        <button type="submit" class="btn btn-lg btn-success">Comprar</button>
      </div>
    </form>
  </div>
@endsection

@section('js')
<script>
      function eliminar(servicio){
          //console.log(toggle);
          $.ajax({
              type: "GET",
              dataType: "json",
              url: `/carrito/quitar/${servicio}`,
              data: {},
              success: function(data){
                alert(data.success);
                if(data.cantidad==0)
                  window.location.href ="{{route('carrito')}}";
                $("#fila"+servicio).remove();
                $('#items').html(data.cantidad);
              //  console.log('pepe');
                calcular();
              }
          });
      }
      //calcula el total del costo
      function calcular(){
          //console.log('pipi');
          var precios=document.getElementsByName('precios[]');
          console.log(precios);
          total=0.0;
          for (var j=0; j<precios.length; j++){
              total += 1 * precios[j].attributes[3].nodeValue;
          }
          $("#total").html(new Intl.NumberFormat('es-AR', { style: 'currency', currency: 'ARS' }).format(total));
          $("#senia").html(new Intl.NumberFormat('es-AR', { style: 'currency', currency: 'ARS' }).format(total/2));
      }
</script>
@endsection
