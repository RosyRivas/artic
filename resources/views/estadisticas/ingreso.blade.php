@extends('layouts.app')

@section('titulo', 'Ingresos por servicios')

@section('js')
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  <script type="text/javascript">

    google.charts.load('current', {'packages':['corechart'], 'language': 'es-AR'});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
      var data = google.visualization.arrayToDataTable([
        ["Servicio", "Ingresos" ],
        @foreach ($ingresos as $i)
            ['{{$i->servicio}}' , {
                v: {{$i->total}},
                f: '{{$i->importe}}',
            }],
        @endforeach
      ]);

      var view = new google.visualization.DataView(data);

      var options = {
        title: "Ingresos por servicios",
        height: 400,
        bar: {groupWidth: "95%"},
        legend: {
            position: "none"
        },
         hAxis: { textPosition: 'none' },
        backgroundColor: '#fafafa',


      };
      var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values"));
      chart.draw(view, options);
    }
  </script>
@endsection

@section('contenido')
<div class="d-block">

<div class="container-fluid">
    <div class="row">
        <div class="h2 col">Ingresos por servicios</div>
    </div>
</div>
<div ad="filtro" class="container border mx-auto p-3 m-3 bg-light">
    <form action="{{route('estadisticas.ingresos')}}" method="POST">
        @csrf
        <div class="row">
            <div class="form-group col">
                <label for="desde"> Desde</label>
                <input type="date" class="form-control @error('desde') is-invalid @enderror"
                id="desde" name="desde" value="{{old('desde',$desde)}}" max="{{now()->format('Y-m-d')}}">

                @error('desde')
                <span class="invalid-feedback" role="alert">
                    <strong>{{$message}}</strong>
                </span>
                @enderror
            </div>
            <div class="form-group col">
                <label for="hasta"> Hasta </label>
                <input type="date" class="form-control @error('hasta') is-invalid @enderror"
                id="hasta" name="hasta" value="{{old('hasta', $hasta)}}" max="{{now()->format('Y-m-d')}}">

                @error('hasta')
                <span class="invalid-feedback" role="alert">
                    <strong>{{$message}}</strong>
                </span>
                @enderror
            </div>
            <div class="col-auto pt-4">
                <button name="filtrar" id="filtrar" type="submit" class="btn btn-outline-primary" data-toggle="tooltip" data-placement="bottom" title="Filtrar"><i class="fas fa-filter"></i></button>
            </div>
        </div>
    </form>
</div>
@if(count($ingresos) == 0)
  <h3 class="text-center mt-5">No hay servicios contratados en el período seleccionado</h3>
@else
<div class="container d-block" style="width: 900px; height: 400px;">
    <div id="columnchart_values" style="width: 900px; height: 400px;"></div>
</div>
@endif
</div>

@endsection
