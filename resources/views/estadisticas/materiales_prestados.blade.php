@extends('layouts.app')

@section('titulo', 'Materiales')

@section('js')
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
  google.charts.load("current", {packages:["corechart"]});
  google.charts.setOnLoadCallback(drawChart);
  function drawChart() {
    var data = google.visualization.arrayToDataTable([
      ['Materiales', 'Prestamos'],
      @foreach ($materiales as $m)
        ['{{$m->material}}' , {{$m->prestamos}}],
      @endforeach
    ]);

    var options = {
      title: 'Materiales mas reservados',
      pieHole: 0.4,
      backgroundColor: '#fafafa',
    };

    var chart = new google.visualization.PieChart(document.getElementById('donutchart'));
    chart.draw(data, options);
  }
</script>
@endsection

@section('contenido')
<div class="container-fluid">
    <div class="row">
        <div class="h2 col">Materiales</div>
    </div>
    <div ad="filtro" class="container border mx-auto p-3 m-3 bg-light">
        <form action="{{route('estadisticas.materiales')}}" method="POST">
            @csrf
            <div class="row">
                <div class="form-group col">
                    <label for="desde"> Desde</label>
                    <input type="date" class="form-control @error('desde') is-invalid @enderror"
                    id="desde" name="desde" value="{{old('desde',$desde)}}" max="{{now()->format('Y-m-d')}}">

                    @error('desde')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{$message}}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group col">
                    <label for="hasta"> Hasta </label>
                    <input type="date" class="form-control @error('hasta') is-invalid @enderror"
                    id="hasta" name="hasta" value="{{old('hasta', $hasta)}}" max="{{now()->format('Y-m-d')}}">

                    @error('hasta')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{$message}}</strong>
                    </span>
                    @enderror
                </div>
                <div class="col-auto pt-4">
                    <button name="filtrar" id="filtrar" type="submit" class="btn btn-outline-primary" data-toggle="tooltip" data-placement="bottom" title="Filtrar"><i class="fas fa-filter"></i></button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="container-fluid" id="donutchart" style="width: 900px; height: 500px;"></div>
@endsection
