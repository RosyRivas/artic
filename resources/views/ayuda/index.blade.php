@extends('layouts.app')

@section('titulo', 'Mis asignaciones')


@section('contenido')

    <div class="container-fluid">
        <div class="row">
            <div class="h2 col">Ayuda </div>
        </div>

    </div>
    <br>
    <div class="container-fluid">
        <!--SYSADMIN -->
        @if (auth()->user()->rol_id == 5)
            <a href="#" onClick="window.open('{{route('ayuda.video','vKptAjTk7Nw' ) }}','Ayuda','resizable,heigt=365,width=837');return false;">- Crear servicio</a>
            <br><a href="#" onClick="window.open('{{route('ayuda.video','ayTIAHNfXdQ' ) }}','Ayuda','resizable,heigt=365,width=837');return false;">- Crear socio</a>
            <br><a href="#" onClick="window.open('{{route('ayuda.video','hnxHHIN3CD0' ) }}','Ayuda','resizable,heigt=365,width=837');return false;">- Devolucion de materiales</a>
            <br><a href="#" onClick="window.open('{{route('ayuda.video','XD5mLaBM66U' ) }}','Ayuda','resizable,heigt=365,width=837');return false;">- Pago del servicio contratado</a>
        @endif
            <!--CLIENTE -->
        @if (auth()->user()->rol_id == 4)
            <a href="#" onClick="window.open('{{route('ayuda.video','JY5YU6z--_A' ) }}','Ayuda','resizable,heigt=365,width=837');return false;">- Contratar servicios en Artic</a>
            <br><a href="#" onClick="window.open('{{route('ayuda.video','oaEUBacICbQ' ) }}','Ayuda','resizable,heigt=365,width=837');return false;">- Pagar seña con mercado pago</a>
            <br><a href="#" onClick="window.open('{{route('ayuda.video','w71WP8ctdMA ' ) }}','Ayuda','resizable,heigt=365,width=837');return false;">- Calificar servicio contratado</a>
        @endif
            <!--SOCIO -->
        @if (auth()->user()->rol_id == 3)
            <a href="#" onClick="window.open('{{route('ayuda.video','GE7RNKTU6YQ' ) }}','Ayuda','resizable,heigt=365,width=837');return false;">- Aceptar asignacion de servicio</a>
            <br><a href="#" onClick="window.open('{{route('ayuda.video','-bAwNIKH4jE' ) }}','Ayuda','resizable,heigt=365,width=837');return false;">- Rechazar asignacion de servicio</a>
        @endif

    </div>

@endsection
