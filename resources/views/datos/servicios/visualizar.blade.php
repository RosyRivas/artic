@extends('layouts.app')

@section('titulo', 'Servicios')


@section('contenido')

    <div class="container-fluid">

        <div class="row">
          <div class="col">
            <div class="h2">{{$servicio->nombre}} </div>
          </div>
        </div>

        <div class="row my-3">
            <div class="col-auto">
              <img src="{{asset('images/servicios/' . $servicio->imagen)}}" alt="{{$servicio->nombre}}" width="400px">
            </div>

            <div class="col mx-0 container-fluid">
                <div class="row pb-2">
                    <div class="col">
                          <div class="card w-100">
                            <div class="card-header">
                                Descripción
                              </div>
                              <div class="card-body">
                                {{$servicio->descripcion}}
                              </div>
                          </div>
                    </div>
                </div>

                <div class="row pt-3">
                    <div class="col">
                          <div class="card w-100">
                              <div class="card-header">
                                    Jefatura
                            </div>
                            <div class="card-body">
                                  {{$servicio->jefatura->nombre}}
                            </div>
                          </div>
                    </div>

                    <div class="col">
                        <div class="card w-100">
                            <div class="card-header">
                                  Costo
                            </div>
                            <div class="card-body">
                                  $  {{$servicio->costo()}}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row pt-3">
                    <div class="col">
                          <div class="card w-100">
                              <div class="card-header">
                                  Cantidad por dia
                            </div>
                            <div class="card-body">
                                  {{$servicio->servicio_dia}}
                            </div>
                          </div>
                    </div>

                    <div class="col">
                        <div class="card w-100">
                            <div class="card-header">
                                  Dias de duracion
                            </div>
                            <div class="card-body">
                                    {{$servicio->duracion}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
          <div class="col">

          <div class="card-deck">
            <div class="card">
                <div class="card-header">
                    Materiales necesarios
                  </div>
                  <div class="card-body container-fluid">
                      @foreach ($servicio->recursos as $r)
                        <div class="row">
                            <div class="col">
                                {{$r->cantidad}}
                            </div>
                            <div class="col">
                              {{$r->material->nombre }}
                            </div>
                        </div>

                      @endforeach
                  </div>
            </div>

            <div class="card">
                <div class="card-header">
                    Brindado por:
              </div>

              <div class="card-body pl-0">
                  <ul>
                    @foreach ($servicio->socios as $s)
                      <div class="container">
                          <div class="row">
                              <div class="col">
                                  <li> {{$s->apellido}}, {{$s->nombres}}</li>
                              </div>
                              @foreach ($s->calificaciones_socio as $c)
                                    @if($servicio->id == $c->servicio->id)
                                        <div class="col-md-4">
                                            <div class="progress">
                                                <div class="progress-bar " role="progressbar" style="width: {{($c->calificacion - 1 ) * 25}}%;" aria-valuenow="{{($c->calificacion - 1 ) * 25}}" aria-valuemin="0" aria-valuemax="100">{{$c->calificacion}}/5</div>
                                            </div>
                                        </div>
                                    @endif
                               @endforeach
                            </div>
                        </div>
                      @endforeach
                      @foreach ($servicio->grupos as $g)
                        <li> {{$g->nombre}}</li>
                      @endforeach
                  </ul>
              </div>
          </div>
        </div>
          </div>
        </div>

        </div>

@endsection
