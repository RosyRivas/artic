@extends('layouts.app')

@section('titulo', 'Servicios')


@section('contenido')

    <div class="container-fluid">
        <div class="row">
            <div class="h2 col">Gestión de servicios </div>
            <div class="col-auto">
                <a href="{{route('servicios.crear')}}" class="btn btn-primary float-right"><i class="fas fa-plus"></i></a>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        @include ('layouts.mensaje')
        <!-- - - - - - - - - - - FILTRADO -------------->
        <form action="{{route('servicios')}}" class="container-fluid my-4 card" method="POST">
          @csrf

          <div class="row mt-3">
            <div class="form-group col">
              <label for="jefatura"> Jefatura</label>
              <select id="jefatura" class="select2 form-control" name="jefatura">
                    <option></option>

                    @foreach ($jefaturas as $j)
                        @if ( old('jefatura', $jefatura) == $j->id )
                              <option value="{{ $j->id }}" selected>{{ $j->nombre }}</option>
                        @else
                              <option value="{{ $j->id }}">{{ $j->nombre }}</option>
                        @endif
                    @endforeach
              </select>
            </div>
              <div class="form-group col">
                    <label for="minimo">Costo mínimo</label>
                    <input type="number"
                          id="minimo" name="minimo"  min="0.00" step="0.01" value="{{old('minimo', $minimo)}}">
                </div>
                <div class="form-group col">
                      <label for="maximo">Costo máximo</label>
                      <input type="number"
                            id="maximo" name="maximo"  min="0.00" step="0.01 " value="{{old('maximo', $maximo)}}">
                  </div>
                <div class="col-auto ">
                  <div class="d-flex justify-content-center mt-4 ">

                      <button class="btn btn-outline-primary" type="submit" name="filtro"><i class="fas fa-filter"></i></button>
                    </div>
                </div>
                <div class="col-auto ">
                  <div class="d-flex justify-content-center mt-4 ">

                      <button class="btn btn-outline-secondary" type="submit" name="pdf"  ><i class="far fa-file-pdf"></i></button>
                    </div>
                </div>
          </div>
    <!-- - - - - - - - - - - Tabla de servicio -------------->

        </form>
            <table id="servicios" class="display responsive nowrap w-100">
                <thead>
                    <tr>
                        <th>Imagen</th>
                        <th>Nombre</th>
                        <th>Jefatura</th>
                        <th>Costo</th>
                        <th>Acciones</th>
                      </tr>
                </thead>
                <tbody>
                    @foreach($servicios as $s)
                    <tr>
                        <td><img src="{{asset('images/servicios/' . $s['imagen'])}}" width="50px"></td>
                        <td>{{$s->nombre}}</td>
                        <td>{{$s->jefatura->nombre}}</td>
                        <td>$ {{$s->costo}}</td>


                        <td>

                            <a href="{{route('servicios.visualizar', $s->id)}}" class="btn btn-primary"><i class="fas fa-eye"></i></a>
                            <a href="{{route('servicios.editar', $s->id)}}" class="btn btn-success"><i class="fas fa-pencil-alt fa-fw"></i></a>

                            <form class="d-inline" action="{{route('servicios.eliminar', $s->id)}}" method="POST">
                                @csrf
                                @method ('DELETE')
                                <button type="submit" class="btn btn-danger m-1" onclick="return confirm ('¿Está seguro de que desea eliminar el registro?')"><i class="fas fa-trash-alt fa-fw"></i></button>

                            </form>
                            <div class="custom-control custom-switch">
                                <input data-id="{{$s->id}}" type="checkbox" class="custom-control-input" id="{{$s->id}}" {{ $s->esta_activo ? 'checked' : '' }} onchange="cambiar(this);">
                                <label class="custom-control-label" for="{{$s->id}}"></label>
                            </div>



                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

@endsection

@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />


    <style>
      .select2-selection__rendered {
          line-height: 38px !important;
      }

      .select2-container .select2-selection--single {
          height: 38px !important;
      }

      .select2-selection__arrow {
          height: 36px !important;
      }

      .selection {
        width: 100% !important;
        line-height: 38px !important;
      }
    </style>
@endsection

@section('js')

    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js">
    </script>

    <script>
        $(document).ready(function() {

            $('#servicios').DataTable({

                aaSorting: [], /*para que se muestre en el mismo orden que viene del controlador*/

                columnDefs: [
                    {
                        targets: 4,
                        className: 'dt-center',
                    }
                ],

                language: {
                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "Mostrar _MENU_ registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "Ningún dato disponible en esta tabla",
                    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            });
        } );
    </script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>

    <script>
        $(document).ready(function() {
            $('.select2').select2({
                placeholder: 'Seleccione una opción',
                theme: 'classic',
                width: '100%',
            });
        });

            $(document).ready(function(){
                $("#btn_add").click(function(){
                agregar();
                });
            });


            var i=0;
            total=0;
            $("#guardar").hide();

            function agregar(){
                cantidad=$("#cantidad").val();
                material=$("#material_id option:selected").text();
                material_id=$("#material_id option:selected").val();
                console.log(cantidad);
                if (cantidad!="" && material!="" ){
                    var fila = "<div class='row' id='fila" + i + "'>" +
                    "<div class='col-7 py-2 my-auto'><input type='hidden' name='materiales[]' id='materiales[]' value=" + material_id + ">" + material +
                    "</div><div class='col-3 py-2 my-auto text-right'><input class='align-middle' type='hidden' name='cantidades[]' id='cantidades[]' value=" + cantidad + ">" + cantidad +
                    "</div><div class='col-2 py-2 my-auto text-center'><button type='button' class='btn btn-danger' onclick='eliminar(" + i + ");'><i class='fas fa-trash-alt fa-fw'></i></button></div></div>";
                    i++;
                    limpiar();
                    $('#recursos').append(fila);
                }else{
                    alert("No se puede agregar el recurso, revise los datos cargados.");
                }
            }

            function limpiar(){
                $("#cantidad").val("");
                $("#material_id").val(null).trigger('change');

            }

            function eliminar(index){
                $("#fila"+index).remove();
                calcular();
            }

            $(document).ready(function(){
                $('.select2').select2({
                  placeholder: 'Seleccione una opción',
                  theme: 'classic',
                  width: '100%',
                });
            });
            $('#archivo').on('change' , function(){
                  var nombre = $(this).val().replace('C:\\fakepath\\', " ");
                  $(this).next('.custom-file-label').html(nombre);
            });

            function cambiar(toggle){

                var status = toggle.checked ? 1 : 0;
                var servicio = toggle.id;

                //console.log(toggle);
                $.ajax({
                    type: "GET",
                    dataType: "json",
                    url: `/datos/servicios/habilitar/${servicio}/${status}`,
                    data: {},
                    success: function(data){
                    alert(data.success)
                    }
                });
            }

        </script>


@endsection
