@extends('layouts.app')

@section('titulo', 'Crear Servicios')

@section('contenido')

    <div class="container">
          <div class="card mx-1 my-3 ">

                  <div class="card-header">
                      Crear servicio
                  </div>
                  <form class="card-body" action="{{route('servicios.crear')}}" method="post" enctype="multipart/form-data">
                          @csrf
                          <div class="text-danger mb-3 small">(*) Campo obligatorio</div>
                          <div class="row">

                              <div class="form-group col">
                                  <label for="nombre ">Nombre <span class="text-danger">*</span></label>
                                  <input type="text" class="form-control @error('nombre') is-invalid @enderror"
                                            id="nombre" name="nombre" value="{{old('nombre')}}">

                                    @error('nombre')
                                      <span class="invalid-feedback" role="alert">
                                          <strong>{{$message}}</strong>
                                      </span>
                                    @enderror

                              </div>

                              <div class="form-group col">
                                      <label for="jefatura_id"> Jefatura <span class="text-danger">*</span></label>
                                      <select id="jefatura_id" class="select2 form-control @error('jefatura_id') is-invalid @enderror" name="jefatura_id">
                                            <option></option>

                                            @foreach ($jefaturas as $j)
                                            @if ( old('jefatura_id') == $j->id )
                                                  <option value="{{ $j->id }}" selected>{{ $j->nombre }}</option>
                                            @else
                                                  <option value="{{ $j->id }}">{{ $j->nombre }}</option>
                                            @endif
                                            @endforeach
                                      </select>

                                      @error('jefatura_id')
                                      <span class="invalid-feedback" role="alert">
                                            <strong>{{$message}}</strong>
                                      </span>
                                      @enderror
                                </div>
                              </div>

                              <div class="row">
                                <div class="form-group col">
                                  <label for="archivo">Imagen <span class="text-danger">*</span></label>
                                  <div class="custom-file">

                                    <input type="file" class="custom-file-input @error('archivo') is-invalid @enderror"
                                              id="archivo" name="archivo" aria-describedby="imagenHelp" value="{{old('archivo')}}">
                                    <label class="custom-file-label" for="archivo">{{old('archivo','Seleccione un archivo')}}</label>
                                    <small id="imagenHelp" class="form-text text-muted">Tipos sopotados: JPG, PNG, JPEG.</small>
                                      @error('archivo')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{$message}}</strong>
                                        </span>
                                      @enderror
                                    </div>
                                </div>

                                <div class="form-group col">
                                      <label for="costo">Costo <span class="text-danger">*</span></label>
                                      <input type="number" class="form-control @error('costo') is-invalid @enderror"
                                                  id="costo" name="costo" value="{{old('costo')}}" min="0.00" step="0.01">
                                                  @error('costo')
                                                  <span class="invalid-feedback" role="alert">
                                                        <strong>{{$message}}</strong>
                                                  </span>
                                                  @enderror
                                </div>
                              </div>

                              <div class="row">

                                  <div class="form-group col">
                                      <label for="servicio_dia ">Cantidad por dia</label>
                                      <input type="number" class="form-control"
                                      id="servicio_dia" name="servicio_dia" value="{{old('servicio_dia')}}">
                                      <small id="imagenHelp" class="form-text text-muted">Cantidad de servicios que se puede prestar en un dia.</small>
                                  </div>
                                  <div class="form-group col">
                                      <label for="duracion ">Dias de duracion</label>
                                      <input type="number" class="form-control"
                                      id="duracion" name="duracion" value="{{old('duracion')}}">
                                      <small id="imagenHelp" class="form-text text-muted">Tiempo que puede durar la realizacion del servicio</small>
                                  </div>

                              </div>

                              <div class="form-group">
                              <label for="socio">Socios </label>
                              <div class="container-fluid p-0">
                                <select name="socios[]" id="socios[]" placeholder="Seleccione los socios" class="select2 js-example-basic-multiple form-control @error('socios[]') is-invalid @enderror" multiple="multiple">
                                    <option></option>
                                    @foreach ($socios as $s)
                                        @if (is_array(old('socios')))
                                            @if ( in_array($s->id, old('socios')))
                                                <option value="{{ $s->id }}" selected>{{ $s->apellido }}, {{ $s->nombres }}</option>
                                            @else
                                                <option value="{{ $s->id }}">{{ $s->apellido }}, {{ $s->nombres }}</option>
                                            @endif
                                        @else
                                            <option value="{{ $s->id }}">{{ $s->apellido }}, {{ $s->nombres }}</option>
                                        @endif
                                    @endforeach
                            </select>
                            @error('socios[]')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{$message}}</strong>
                            </span>
                            @enderror
                        </div>
                      </div>

                    <div class="form-group">
                        <label for="grupo">Grupos</label>
                        <div class="container-fluid p-0">
                          <select name="grupos[]" id="grupos[]" placeholder="Seleccione grupos" class="select2 js-example-basic-multiple form-control @error('grupos[]') is-invalid @enderror" multiple="multiple">
                              <option></option>
                              @foreach ($grupos as $g)
                                  @if (is_array(old('grupos')))
                                      @if ( in_array($g->id, old('grupos')))
                                          <option value="{{ $g->id }}" selected>{{ $g->nombre }}</option>
                                      @else
                                          <option value="{{ $g->id }}">{{ $g->nombre }}</option>
                                      @endif
                                  @else
                                      <option value="{{ $g->id }}"> {{ $g->nombre }}</option>
                                  @endif
                              @endforeach
                      </select>
                      @error('grupos[]')
                      <span class="invalid-feedback" role="alert">
                          <strong>{{$message}}</strong>
                      </span>
                      @enderror
                  </div>
                  <div>

                          <div class="form-group">
                                <label for="descripcion">Descripcion</label>
                                <textarea type="text" class="form-control" id="descripcion" name="descripcion" rows="3" maxlength="250">{{old('descripcion')}}</textarea>
                          </div>

                          <div class="card col-6 mx-auto">

                              <div class="card-header px-0 row font-weight-bold" >
                                  <div class="col-7 bg-light">Material</div>
                                  <div class="col-3 bg-light text-right">Cantidad</div>
                                  <div class="col-2 bg-light"></div>
                              </div>

                              <div id="recursos" class="container-fluid p-0">

                              </div>

                              <div class="row">
                                    <div class="col-7 py-2" >
                                      <select id='material_id' class="select2 form-control @error('recurso_id') is-invalid @enderror" name="recurso_id">
                                        <option></option>
                                        @foreach ($materiales as $m )
                                                @if (old('material_id')== $m->id)
                                                    <option value="{{$m->id}}" selected>{{$m->nombre}}</option>
                                                  @else
                                                  <option value="{{$m->id}}" selected>{{$m->nombre}}</option>
                                                @endif

                                         @endforeach

                                      </select>
                                    </div>
                                    <div class="col-3 py-2">
                                      <input type="number" class="form-control @error('cantidad') is-invalid @enderror"
                                      id="cantidad" name="cantidad" value="{{old('cantidad')}}"
                                      max="1000" min="1">
                                    </div>
                                    <div class="col-2 py-2 text-center"><button type="button" id="btn_add" class="btn btn-primary"><i class="fas fa-plus"></i></button>  </div>
                                </div>
                            </div>

                          <div class="d-flex justify-content-center mt-4 ">
                                <button type="submit" class="btn btn-primary mx-2" >Crear Servicio</button>
                                <a href="{{route('servicios')}}" class="btn btn-secondary mx-2">Volver</a>
                          </div>

                  </form>
          </div>

    </div>
@endsection
@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />

    <style>
      .select2-selection__rendered {
          line-height: 38px !important;
      }

      .select2-container .select2-selection--single {
          height: 38px !important;
      }

      .select2-selection__arrow {
          height: 36px !important;
      }

      .selection {
        width: 100% !important;
        line-height: 38px !important;
      }
    </style>
@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>

<script>
    $(document).ready(function() {
        $('.select2').select2({
            placeholder: 'Seleccione una opción',
            theme: 'classic',
            width: '100%',
        });
    });

        $(document).ready(function(){
            $("#btn_add").click(function(){
            agregar();
            });
        });


        var i=0;
        total=0;
        $("#guardar").hide();

        function agregar(){
            cantidad=$("#cantidad").val();
            material=$("#material_id option:selected").text();
            material_id=$("#material_id option:selected").val();
            console.log(cantidad);
            if (cantidad!="" && material!="" ){
                var fila = "<div class='row' id='fila" + i + "'>" +
                "<div class='col-7 py-2 my-auto'><input type='hidden' name='materiales[]' id='materiales[]' value=" + material_id + ">" + material +
                "</div><div class='col-3 py-2 my-auto text-right'><input class='align-middle' type='hidden' name='cantidades[]' id='cantidades[]' value=" + cantidad + ">" + cantidad +
                "</div><div class='col-2 py-2 my-auto text-center'><button type='button' class='btn btn-danger' onclick='eliminar(" + i + ");'><i class='fas fa-trash-alt fa-fw'></i></button></div></div>";
                i++;
                limpiar();
                $('#recursos').append(fila);
            }else{
                alert("No se puede agregar el recurso, revise los datos cargados.");
            }
        }

        function limpiar(){
            $("#cantidad").val("");
            $("#material_id").val(null).trigger('change');

        }

        function eliminar(index){
            $("#fila"+index).remove();
            calcular();
        }

        $(document).ready(function(){
            $('.select2').select2({
              placeholder: 'Seleccione una opción',
              theme: 'classic',
              width: '100%',
            });
        });
        $('#archivo').on('change' , function(){
              var nombre = $(this).val().replace('C:\\fakepath\\', " ");
              $(this).next('.custom-file-label').html(nombre);
        });

    </script>
@endsection

@section('css')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
@endsection
