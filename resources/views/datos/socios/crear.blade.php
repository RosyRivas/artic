@extends('layouts.app')

@section('titulo', 'Crear socio')

@section('contenido')

    <div class="conteiner">
          <div class="card mx-1 my-3 ">

                  <div class="card-header">
                      Crear socio
                  </div>

                        <form class="card-body" action="{{route('socios.crear')}}" method="post"  enctype="multipart/form-data">
                              @csrf
                                <div class="row">
                                  <div class="col">
                                    <div class="form-group">
                                          <label for="nombres">Nombres  <span class="text-danger">*</span></label>
                                          <input type="text" class="form-control  @error('nombres') is-invalid @enderror" id="nombres" name="nombres" value="{{old('nombres')}}">
                                          @error('nombres')
                                          <span class="invalid-feedback" role="alert">
                                                <strong>{{$message}}</strong>
                                          </span>
                                          @enderror

                                    </div>

                                    <div class="form-group">
                                        <label for="nombre_artistico">Nombre  artístico</label>
                                        <input type="text" class="form-control  @error('nombre_artistico') is-invalid @enderror" id="nombre_artistico" name="nombre_artistico" value="{{old('nombre_artistico')}}">
                                      @error('nombre_artistico')
                                          <span class="invalid-feedback" role="alert">
                                              <strong>{{$message}}</strong>
                                          </span>
                                      @enderror

                                    </div>

                                    <div class="form-group">
                                          <label for="email">Email<span class="text-danger">*</span></label>
                                          <input type="email" class="form-control  @error('email') is-invalid @enderror" id="email" name="email" value="{{old('email')}}">
                                          @error('email')
                                          <span class="invalid-feedback" role="alert">
                                                <strong>{{$message}}</strong>
                                          </span>
                                          @enderror

                                    </div>


                          </div>
                          <div class="col">

                                    <div class="form-group">
                                          <label for="apellido">Apellido  <span class="text-danger">*</span></label>
                                          <input type="text" class="form-control  @error('apellido') is-invalid @enderror" id="apellido" name="apellido" value="{{old('apellido')}}">
                                          @error('apellido')
                                          <span class="invalid-feedback" role="alert">
                                                <strong>{{$message}}</strong>
                                          </span>
                                          @enderror

                                    </div>

                                    <div class="form-group col">
                                            <label for="jefatura_id"> Jefatura <span class="text-danger">*</span></label>
                                            <select id="jefatura_id" class="select2 form-control @error('jefatura_id') is-invalid @enderror" name="jefatura_id">
                                                  <option></option>

                                                  @foreach ($jefaturas as $j)
                                                  @if ( old('jefatura_id') == $j->id )
                                                        <option value="{{ $j->id }}" selected>{{ $j->nombre }}</option>
                                                  @else
                                                        <option value="{{ $j->id }}">{{ $j->nombre }}</option>
                                                  @endif
                                                  @endforeach
                                            </select>

                                            @error('jefatura_id')
                                            <span class="invalid-feedback" role="alert">
                                                  <strong>{{$message}}</strong>
                                            </span>
                                            @enderror
                                      </div>


                                  <div class="form-group">
                                          <label for="password">Contraseña  <span class="text-danger">*</span></label>
                                          <input type="password" class="form-control  @error('password') is-invalid @enderror" id="password" name="password" value="{{old('password')}}">
                                          @error('password')
                                          <span class="invalid-feedback" role="alert">
                                                <strong>{{$message}}</strong>
                                          </span>
                                          @enderror

                                    </div>


                      </div>


                  </div>
                  <div class="card col-6 mx-auto">
                      <div class="card-header px-0 row font-weight-bold" >
                          <div class="col-7 bg-light">Material</div>
                          <div class="col-3 bg-light text-right">Cantidad</div>
                          <div class="col-2 bg-light"></div>
                      </div>

                      <div id="recursos" class="container-fluid p-0">

                      </div>
                      <div class="row">
                            <div class="col-7 py-2" >
                              <select id='material_id' class="select2 form-control @error('recurso_id') is-invalid @enderror" name="recurso_id">
                                <option></option>
                                @foreach ($materiales as $m )
                                        @if (old('material_id')== $m->id)
                                            <option value="{{$m->id}}" selected>{{$m->nombre}}</option>
                                          @else
                                          <option value="{{$m->id}}" selected>{{$m->nombre}}</option>
                                        @endif

                                 @endforeach

                              </select>
                            </div>
                            <div class="col-3 py-2">
                              <input type="number" class="form-control @error('cantidad') is-invalid @enderror"
                              id="cantidad" name="cantidad" value="{{old('cantidad')}}"
                              max="1000" min="1">
                            </div>
                            <div class="col-2 py-2 text-center"><button type="button" id="btn_add" class="btn btn-primary"><i class="fas fa-plus"></i></button>  </div>
                        </div>
                    </div>


                          <div class="d-flex justify-content-center mt-4 ">
                                <button type="submit" class="btn btn-primary mx-2" >Crear socio</button>
                                <a href="{{route('socios')}}" class="btn btn-secondary mx-2">Volver</a>
                          </div>
                  </form>
          </div>

    </div>
@endsection

@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />

    <style>
      .select2-selection__rendered {
          line-height: 38px !important;
      }

      .select2-container .select2-selection--single {
          height: 38px !important;
      }

      .select2-selection__arrow {
          height: 36px !important;
      }

      .selection {
        width: 100% !important;
        line-height: 38px !important;
      }
    </style>
@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>

<script>
    $(document).ready(function() {
        $('.select2').select2({
            placeholder: 'Seleccione una opción',
            theme: 'classic',
            width: '100%',
        });
    });

    $(document).ready(function(){
        $("#btn_add").click(function(){
        agregar();
        });
    });


    var i=0;
    total=0;
    $("#guardar").hide();

    function agregar(){
        cantidad=$("#cantidad").val();
        material=$("#material_id option:selected").text();
        material_id=$("#material_id option:selected").val();
        console.log(cantidad);
        if (cantidad!="" && material!="" ){
            var fila = "<div class='row' id='fila" + i + "'>" +
            "<div class='col-7 py-2 my-auto'><input type='hidden' name='materiales[]' id='materiales[]' value=" + material_id + ">" + material +
            "</div><div class='col-3 py-2 my-auto text-right'><input class='align-middle' type='hidden' name='cantidades[]' id='cantidades[]' value=" + cantidad + ">" + cantidad +
            "</div><div class='col-2 py-2 my-auto text-center'><button type='button' class='btn btn-danger' onclick='eliminar(" + i + ");'><i class='fas fa-trash-alt fa-fw'></i></button></div></div>";
            i++;
            limpiar();
            $('#recursos').append(fila);
        }else{
            alert("No se puede agregar el recurso, revise los datos cargados.");
        }
    }

    function limpiar(){
        $("#cantidad").val("");
        $("#material_id").val(null).trigger('change');

    }

    function eliminar(index){
        $("#fila"+index).remove();
        calcular();
    }
    $(document).ready(function(){
        $('.select2').select2({
          placeholder: 'Seleccione una opción',
          theme: 'classic',
          width: '100%',
        });
    });
</script>
@endsection
