@extends('layouts.app')

@section('titulo', 'Socios')


@section('contenido')

    <div class="container-fluid">

        <div class="row">
          <div class="col">
            <div class="h2">{{$socio->apellido}}, {{$socio->nombres}} </div>
          </div>
        </div>

        <div class="row my-3">

            <div class="col mx-0 container-fluid">

                <div class="row pb-2">
                    <div class="col">
                          <div class="card w-100">
                            <div class="card-header">
                                Calificaciones por servicios:
                              </div>
                              <div class="card-body container-fluid">
                                  <ul>
                                    @forelse ($socio->calificaciones_socio as $c)
                                        <div class="container">
                                            <div class="row">
                                                <div class="col" >{{$c->servicio->nombre}}</div>

                                                    <div class="col-md-6">
                                                        <div class="progress">
                                                            <div class="progress-bar " role="progressbar" style="width: {{($c->calificacion - 1 ) * 25}}%;" aria-valuenow="{{($c->calificacion - 1 ) * 25}}" aria-valuemin="0" aria-valuemax="100">{{$c->calificacion}}/5</div>
                                                        </div>
                                                    </div>

                                            </div>
                                        </div>


                                    @empty
                                        <div class="font-italic">
                                            Ninguno
                                        </div>
                                    @endforelse
                                  </ul>
                              </div>
                          </div>
                    </div>


                </div>

                <div class="row pt-3">
                    <div class="col">
                          <div class="card w-100">
                              <div class="card-header">
                                    Jefatura:
                            </div>
                            <div class="card-body">
                                  {{$socio->jefatura->nombre}}
                            </div>
                          </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="row">
          <div class="col">

          <div class="card-deck">
            <div class="card">
                <div class="card-header">
                    Materiales:
                  </div>
                  <div class="card-body container-fluid">
                    <ul>
                      @forelse ($socio->recursos as $r)
                          <li> {{$r->cantidad}} &times; {{$r->material->nombre}}</li>
                      @empty
                          <div class="font-italic">
                              Ninguno
                          </div>
                      @endforelse
                    </ul>
                  </div>
            </div>

            <div class="card">
                <div class="card-header">
                    Grupos a los que pertenece:
              </div>

              <div class="card-body pl-0">
                  <ul>
                    @forelse ($socio->grupos as $g)
                        <li>{{$g->nombre}}</li>
                    @empty
                        <div class="font-italic">
                            Ninguno
                        </div>
                    @endforelse
                  </ul>
              </div>
          </div>
        </div>
          </div>
        </div>

        </div>

@endsection
