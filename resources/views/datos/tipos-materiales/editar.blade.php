@extends('layouts.app')

@section('titulo', 'Editar tipo de material ')

@section('contenido')

    <div class="col-auto col-md-8 col-lg-6 offset-md-2 offset-lg-3 p-0">
          <div class="card mx-1 my-3 ">

                  <div class="card-header">
                      Editar tipo de material

                  </div>
                  <form class="card-body" action="{{route('tipos-materiales.editar', $tipo_material )}}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="text-danger mb-3 small">(*) Campo obligatorio</div>
                        <div class="form-group">
                              <label for="nombre ">Nombre <span class="text-danger">*</span></label>
                              <input type="text" class="form-control @error('nombre') is-invalid @enderror"
                                          id="nombre" name="nombre" value="{{old('nombre', $tipo_material->nombre)}}">

                              @error('nombre')
                                  <span class="invalid-feedback" role="alert">
                                        <strong>{{$message}}</strong>
                                  </span>
                              @enderror

                        </div>

                        <div class="form-group">
                              <label for="descripcion">Descripcion</label>
                              <input type="text" class="form-control"
                                          id="descripcion" name="descripcion" value="{{old('descripcion',$tipo_material->descripcion)}}">

                        </div>

                          <div class="d-flex justify-content-center mt-4 ">
                              <button type="submit" class="btn btn-primary mx-2" >Editar tipo de material </button>
                              <a href="{{route('tipos-materiales')}}" class="btn btn-secondary mx-2">Volver</a>
                          </div>
                  </form>
          </div>
    </div>
@endsection
