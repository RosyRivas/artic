@extends('layouts.app')

@section('titulo', 'Editar material')

@section('contenido')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="{{route('home')}}">Inicio</a></li>
      <li class="breadcrumb-item active" aria-current="page"><a href="{{route('materiales.editar', $material->id)}}">Editar material</a></li>
  </ol>
</nav>
    <div class="col-auto col-md-8 col-lg-6 offset-md-2 offset-lg-3 p-0">
          <div class="card mx-1 my-3 ">

                  <div class="card-header">
                      Editar material

                  </div>
                  <form class="card-body" action="{{route('materiales.editar', $material)}}" method="post">
                          @csrf
                          @method('PUT')
                          <div class="text-danger mb-3 small">(*) Campo obligatorio</div>
                          <div class="form-group">
                                <label for="nombre">Nombre <span class="text-danger">*</span></label>
                                <input type="text" class="form-control  @error('nombre') is-invalid @enderror" id="nombre" name="nombre" value="{{old('nombre',$material->nombre)}}">
                                @error('nombre')
                                <span class="invalid-feedback" role="alert">
                                      <strong>{{$message}}</strong>
                                </span>
                                @enderror

                          </div>
                          <div class="form-group ">
                                  <label for="tipo_material_id"> Tipo de material <span class="text-danger">*</span></label>
                                  <select id="tipo_material_id" class="select2 form-control @error('tipo_material_id') is-invalid @enderror" name="tipo_material_id">
                                        <option></option>

                                        @foreach ($tipoMaterial as $tm)
                                        @if ( old('tipo_material', $material->tipo_material_id) == $tm->id )
                                              <option value="{{ $tm->id }}" selected>{{ $tm->nombre }}</option>
                                        @else
                                              <option value="{{ $tm->id }}">{{ $tm->nombre }}</option>
                                        @endif
                                        @endforeach
                                  </select>

                                  @error('tipo_material')
                                  <span class="invalid-feedback" role="alert">
                                        <strong>{{$message}}</strong>
                                  </span>
                                  @enderror
                            </div>
                          <div class="form-group">
                                <label for="descripcion">Descripción </label>
                                <input type="text" class="form-control"  id="descripcion" name="descripcion" value="{{old('descripcion', $material->descripcion)}}">

                          </div>
                          <div class="form-group">
                                <label for="cantidad">Cantidad </label>
                                <input type="text" class="form-control"  id="cantidad" name="cantidad" value="{{old('cantidad', $material->cantidad)}}">

                          </div>
                          <div class="d-flex justify-content-center mt-4 ">
                                <button type="submit" class="btn btn-primary mx-2" >Editar material</button>
                                <a href="{{route('materiales')}}" class="btn btn-secondary mx-2">Volver</a>
                          </div>
                  </form>
          </div>
    </div>
@endsection
