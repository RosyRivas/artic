@extends('layouts.app')

@section('titulo', 'Crear material')

@section('contenido')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="{{route('home')}}">Inicio</a></li>
      <li class="breadcrumb-item active" aria-current="page"><a href="{{route('materiales.crear')}}">Crear materiales</a></li>
  </ol>
</nav>
    <div class="col-auto col-md-8 col-lg-6 offset-md-2 offset-lg-3 p-0">
          <div class="card mx-1 my-3 ">

                  <div class="card-header">
                      Crear material

                  </div>
                  <form class="card-body" action="{{route('materiales.crear')}}" method="post">
                          @csrf
                          <div class="text-danger mb-3 small">(*) Campo obligatorio</div>
                          <div class="form-group">
                                <label for="nombre">Nombre <span class="text-danger">*</span></label>
                                <input type="text" class="form-control  @error('nombre') is-invalid @enderror" id="nombre" name="nombre" value="{{old('nombre')}}">
                                @error('nombre')
                                <span class="invalid-feedback" role="alert">
                                      <strong>{{$message}}</strong>
                                </span>
                                @enderror

                          </div>
                          <div class="form-group ">
                                  <label for="tipo_material_id"> Tipo de material <span class="text-danger">*</span></label>
                                  <select id="tipo_material_id" class="select2 form-control @error('tipo_material_id') is-invalid @enderror" name="tipo_material_id">
                                        <option></option>

                                        @foreach ($tipoMaterial as $tm)
                                        @if ( old('tipo_material') == $tm->id )
                                              <option value="{{ $tm->id }}" selected>{{ $tm->nombre }}</option>
                                        @else
                                              <option value="{{ $tm->id }}">{{ $tm->nombre }}</option>
                                        @endif
                                        @endforeach
                                  </select>

                                  @error('tipo_material')
                                  <span class="invalid-feedback" role="alert">
                                        <strong>{{$message}}</strong>
                                  </span>
                                  @enderror
                            </div>
                          <div class="form-group">
                                <label for="descripcion">Descripcion</label>
                                <input type="text" class="form-control"  id="descripcion" name="descripcion" value="{{old('descripcion')}}">

                          </div>
                          <div class="form-group">
                                <label for="cantidad">Cantidad </label>
                                <input type="text" class="form-control"  id="cantidad" name="cantidad" value="{{old('cantidad')}}">

                          </div>
                          <div class="d-flex justify-content-center mt-4 ">
                                <button type="submit" class="btn btn-primary mx-2" >Crear material</button>
                                <a href="{{route('materiales')}}" class="btn btn-secondary mx-2">Volver</a>
                          </div>
                  </form>
          </div>

    </div>
@endsection

@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />

    <style>
      .select2-selection__rendered {
          line-height: 38px !important;
      }

      .select2-container .select2-selection--single {
          height: 38px !important;
      }

      .select2-selection__arrow {
          height: 36px !important;
      }

      .selection {
        width: 100% !important;
        line-height: 38px !important;
      }
    </style>
@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>

<script>
    $(document).ready(function() {
        $('.select2').select2({
            placeholder: 'Seleccione una opción',
            theme: 'classic',
            width: '100%',
        });
    });
</script>
@endsection
