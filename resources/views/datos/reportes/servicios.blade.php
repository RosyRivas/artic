<!DOCTYPE html>
<html>
<head>
    <style>
        @page {
            margin: 0cm 0cm;
            font-family: Arial;
        }

        body {
            margin:2cm 2cm;
        }

        header {
            position: fixed;

            top: 1cm;
            left: 2cm;
            right: 0cm;
            height: 2.5cm;
            text-align: right;
            line-height: 1.5;
            padding-right: 2cm;
            font-family: "Hatton";
        }

        footer {
            position: fixed;
            bottom: 0cm;
            left: 0cm;
            right: 0cm;
            height: 3cm;
            color: black;
            text-align: center;
            line-height: 35px;
        }

        img{

        }
        main{
            margin-top: 2cm;
        }

        table {
            table-layout: fixed;
            width: 100%;
            border-collapse: collapse;
            border: 1px solid grey;
        }

        thead th:nth-child(1) {
            width: 125px;
        }

        thead th:nth-child(2) {
            width: 25%;
        }

        thead th:nth-child(3) {
            width: 25%;
        }

        thead th:nth-child(4) {
            width: 25%;
        }

        th, td {
            padding: 20px;
            border-collapse: collapse;
            border: 1px solid grey;
        }

        table td:nth-child(4) {
            text-align: right;
        }
        hr{
          padding-top: 2px;

        }

        .row{
            height: 2.5;

        }

    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
    <body>
        <header>
        <img
            src="/home/rosa/Documentos/artic/public/images/logoApa.jpg"
            alt="Logo"
            width="100px"
            align="left"
          >
              <div>
                <h4 style="margin-bottom:5px; margin-top:0;">Cooperativa Academia Productura de Arte</h4>
                <div style="font-size:10pt;">
                  academiaproductoradeartes@gmail.com
                  <br>3758 51-2972
                  <br>San Jose - Misiones
              </div>
        </header>

        <main>
          <div class="container-fluid">
                <hr style="width:150%;">
          </div>
            <h3>Reporte de Servicios</h3>
              <h4>Filtros aplicados: </h4>
            @if(!is_null($minimo))
                <p>Costo minimo:{{$minimo}} </p>
            @endif
            @if(!is_null($maximo))
                <p>Costo maximo:{{$maximo}} </p>
            @endif
            @if(!is_null($jefatura))
                <p>Jefatura: {{$jefatura->nombre}}</p>
            @endif

            <table class="collapse">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Descripcion</th>
                        <th>Jefatura</th>
                        <th>Costo</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($servicios as $s)
                    <tr>
                        <td>
                            {{$s->nombre}}
                        </td>
                        <td>{{$s->descripcion}}</td>
                        <td>{{$s->jefatura->nombre}}</td>
                        <td>$ {{$s->costo}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </main>

        <footer>

        </footer>
        <script type="text/php">
            if (isset($pdf)) {
                $text = "página {PAGE_NUM} de {PAGE_COUNT}";
                $size = 10;
                $font = $fontMetrics->getFont("Arial");
                $width = $fontMetrics->get_text_width($text, $font, $size) / 2;
                $x = ($pdf->get_width() - $width);
                $y = $pdf->get_height() - 35;
                $pdf->page_text($x, $y, $text, $font, $size);
                $pdf->page_text(40, $y, "Generado por: ".Auth::user()->name." - ".now()->format('d/m/Y H:i:s'), $font, $size);
            }
        </script>
    </body>
</html>
