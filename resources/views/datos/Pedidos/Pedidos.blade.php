@extends('layouts.app')

@section('titulo', 'Pedido')

@section('contenido')
<div class="container">
  <div class="row">
      <div class="h2 col-12">Servicios contratados</div>
      <div class="h3 col-12 mt-3">
          Servicio:   {{$a->servicio_contratado->servicio_id}}
      </div>

    <table class="col-12 m-0 w-100 table table-hover">
      <thead>
        <tr>
            <th>Nombre del servicio</th>
            <th>Fecha</th>
            <th class="text-right">Precio</th>
            <th class="text-right">Accion</th>
        </tr>
      </thead>
      <tbody>
        <tr>
            <td>{{$a->servicio_contratado->servicio->nombre}}</td>
            <td>{{date('d/m/y', strtotime($a->servicio_contratado->fecha_prestacion))}}</td>
            <td class="text-right"> ${{number_format($a->servicio_contratado->servicio->costo, 2,',', '.')}}</td>
            <td class="text-right">
                  <button type="submit" class="btn btn-primary mx-2" class="btn btn-primary" data-toggle="modal" data-target="#modal{{$a->id}}"><i class="fas fa-eye"></i></button>
            </td>
        </tr>
      </tbody>
    </table>



  </div>
</div>
@endsection
