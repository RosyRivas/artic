@extends('layouts.app')

@section('titulo', 'Editar user')

@section('contenido')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="{{route('home')}}">Inicio</a></li>
      <li class="breadcrumb-item active" aria-current="page"><a href="{{route('users.editar', $user->id)}}">Editar Usuario</a></li>
  </ol>
</nav>
    <div class="col-auto col-md-8 col-lg-6 offset-md-2 offset-lg-3 p-0">
          <div class="card mx-1 my-3 ">

                  <div class="card-header">
                      Editar usuario

                  </div>
                  <form class="card-body" action="{{route('users.editar', $user)}}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="text-danger mb-3 small">(*) Campo obligatorio</div>
                        <div class='row'>
                                <div class="form-group col">
                                      <label for="name">Nombre <span class="text-danger">*</span></label>
                                      <input type="text" class="form-control @error('name') is-invalid @enderror"
                                      id="name" name="name" value="{{old('name', $user->name)}}" maxlength="50">

                                      @error('name')
                                      <span class="invalid-feedback" role="alert">
                                            <strong>{{$message}}</strong>
                                      </span>
                                      @enderror
                                  </div>
                                  <div class="form-group col">
                                    <label for="apellido">Apellido <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control  @error('apellido') is-invalid @enderror" id="apellido" name="apellido" value="{{old('apellido', $user->apellido)}}">
                                    @error('apellido')
                                    <span class="invalid-feedback" role="alert">
                                      <strong>{{$message}}</strong>
                                    </span>
                                    @enderror

                                  </div>
                                </div>

                                <div class="row">
                                  <div class="form-group col">
                                        <label for="email">Email <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control  @error('email') is-invalid @enderror" id="email" name="email" value="{{old('email', $user->email)}}">
                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                              <strong>{{$message}}</strong>
                                        </span>
                                        @enderror

                                  </div>


                                <div class="form-group col">
                                      <label for="password">Password</label>
                                      <input type="password" class="form-control" id="password" name="password">
                                </div>


                            </div>
                            <div class="row">
                              <div class="form-group col">
                                    <label for="saldo">Saldo <span class="text-danger">*</span></label>
                                    <input type="number" class="form-control  @error('saldo') is-invalid @enderror" id="saldo" name="saldo" value="{{old('saldo', $user->saldo)}}">
                                    @error('saldo')
                                    <span class="invalid-feedback" role="alert">
                                          <strong>{{$message}}</strong>
                                    </span>
                                    @enderror

                              </div>
                              <div class="form-group col">
                                    <label for="rol_id">Rol <span class="text-danger">*</span></label>
                                    <select id="rol_id" class="select2 form-control @error('rol_id') is-invalid @enderror" name="rol_id">
                                          <option></option>

                                          @foreach ($roles as $r)
                                          @if ( old('rol_id', $user->rol_id) == $r->id )
                                                <option value="{{ $r->id }}" selected>{{ $r->nombre }}</option>
                                          @else
                                                <option value="{{ $r->id }}">{{ $r->nombre }}</option>
                                          @endif
                                          @endforeach
                                    </select>
                                    @error('rol_id')
                                    <span class="invalid-feedback" role="alert">
                                          <strong>{{$message}}</strong>
                                    </span>
                                    @enderror

                              </div>
                            </div>


                          <div class="d-flex justify-content-center mt-4 ">
                              <button type="submit" class="btn btn-primary mx-2" >Editar usuario</button>
                              <a href="{{route('users')}}" class="btn btn-secondary mx-2">Volver</a>
                          </div>
                  </form>
          </div>
    </div>
@endsection


@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />

    <style>
      .select2-selection__rendered {
          line-height: 38px !important;
      }

      .select2-container .select2-selection--single {
          height: 38px !important;
      }

      .select2-selection__arrow {
          height: 36px !important;
      }

      .selection {
        width: 100% !important;
        line-height: 38px !important;
      }
    </style>
@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>

<script>
    $(document).ready(function() {
        $('.select2').select2({
            placeholder: 'Seleccione una opción',
            theme: 'classic',
            width: '100%',
        });
    });
</script>
@endsection
