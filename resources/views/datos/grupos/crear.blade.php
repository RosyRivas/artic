@extends('layouts.app')

@section('titulo', 'Crear Grupos')

@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />

    <style>
        .select2-selection__rendered {
            line-height: 38px !important;
        }

        .select2-container .select2-selection--single {
            height: 38px !important;
        }

        .select2-selection__arrow {
            height: 36px !important;
        }

        .selection {
            width: 100% !important;
            line-height: 38px !important;
        }
    </style>
@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>

    <script>
        $(document).ready(function() {
            $('.select2').select2({
                placeholder: 'Seleccione una opción',
                theme: 'classic',
                width: '100%',
            });
        });
    </script>
@endsection

@section('contenido')
    <div class="container">
        <div class="card mx-1 my-3 ">
            <div class="card-header">
                Crear grupo
            </div>

            <form class="card-body container" action="{{route('grupos.crear')}}" method="post">
                @csrf
                <div class="row">
                    <div class="col-12 text-danger mb-3 small">(*) Campo obligatorio</div>
                </div>

                <div class="row">
                    <div class="form-group col">
                        <label for="nombre ">Nombre <span class="text-danger">*</span></label>
                        <input type="text" class="form-control @error('nombre') is-invalid @enderror"
                        id="nombre" name="nombre" value="{{old('nombre')}}">

                        @error('nombre')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{$message}}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group col">
                        <label for="jefe_id"> Responsable</label>
                        <select id="jefe_id" class="select2 form-control @error('jefe_id') is-invalid @enderror" name="jefe_id">
                            <option></option>

                            @foreach ($socios  as $s)
                                @if ( old('jefe_id') == $s->id )
                                    <option value="{{ $s->id }}" selected>{{ $s->apellido }}, {{ $s->nombres }}</option>
                                @else
                                    <option value="{{ $s->id }}">{{ $s->apellido }}, {{ $s->nombres }}</option>
                                @endif
                            @endforeach
                        </select>

                        @error('jefe_id')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{$message}}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <label for="socios">Socios <span class="text-danger">*</span></label>
                        <select name="socios[]" id="socios[]" placeholder="Seleccione los socios" class="select2 js-example-basic-multiple form-control @error('socios[]') is-invalid @enderror" multiple="multiple" required>
                            <option></option>
                            @foreach ($socios as $s)
                                @if (is_array(old('socios')))
                                    @if ( in_array($s->id, old('socios')))
                                        <option value="{{ $s->id }}" selected>{{ $s->apellido }}, {{ $s->nombres }}</option>
                                    @else
                                        <option value="{{ $s->id }}">{{ $s->apellido }}, {{ $s->nombres }}</option>
                                    @endif
                                @else
                                    <option value="{{ $s->id }}">{{ $s->apellido }}, {{ $s->nombres }}</option>
                                @endif
                            @endforeach
                        </select>

                        @error('socios[]')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{$message}}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="d-flex justify-content-center mt-4 ">
                    <button type="submit" class="btn btn-primary mx-2" >Crear Grupo</button>
                    <a href="{{route('grupos')}}" class="btn btn-secondary mx-2">Volver</a>
                </div>
            </form>
        </div>
    </div>
@endsection
