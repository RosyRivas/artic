@extends('layouts.app')

@section('titulo', 'Grupos')


@section('contenido')

    <div class="container-fluid">
        <div class="row">
            <div class="h2 mb-3">{{$grupo->nombre}} </div>
        </div>

        <div class="row">
            <div class="col-6">
                <div class="card w-100">
                    <div class="card-header">
                        Responsable del grupo
                    </div>
                    <div class="card-body container-fluid">
                        <ul>
                            <li> {{$grupo->jefe->nombres}}, {{$grupo->jefe->apellido}}</li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-6">
                <div class="card w-100">
                    <div class="card-header">
                        Calificacion de servicios:
                    </div>
                    <div class="card-body container-fluid">
                        <ul>
                            @forelse ($grupo->calificaciones_grupo as $g)
                                <div class="container">
                                    <div class="row">
                                        <div class="col" >{{$g->servicio->nombre}}</div>

                                        <div class="col-md-6">
                                            <div class="progress">
                                                <div class="progress-bar " role="progressbar" style="width: {{($g->calificacion - 1 ) * 25}}%;" aria-valuenow="{{($g->calificacion - 1 ) * 25}}" aria-valuemin="0" aria-valuemax="100">{{$g->calificacion}}/5</div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                @empty
                                    <div class="font-italic">
                                        Ninguno
                                    </div>
                                @endforelse
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 p-3">
                    <div class="card w-100">
                        <div class="card-header">
                            Socios que pertenecen al grupo:
                        </div>
                        <div class="card-body container-fluid">
                            <ul>
                                @forelse ($grupo->socios as $s)
                                    <li> {{$s->nombres}}, {{$s->apellido}}</li>
                                @empty
                                    <div class="font-italic">
                                        Ninguno
                                    </div>
                                @endforelse
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
