<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">
      @auth
        <button type="button" id="sidebarCollapse" class="btn btn-info">
            <i class="fas fa-align-left"></i>
        </button>
      @else
        <img
            src="/images/logoApaRecorte.png"
            alt="Logo"
            width="80px"
            align="left"
          >
      @endauth
      

        <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <i class="fas fa-align-justify"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">

            <ul class="nav navbar-nav ml-auto">
              @guest
                  <li class="nav-item active">
                      <a class="nav-link" href="{{ route('login') }}">Iniciar sesión</a>
                  </li>
                  <li class="nav-item active">
                      <a class="nav-link" href="{{ route('register') }}">Registrarse</a>
                  </li>
              @else
                @if (auth()->user()->rol_id == 4)
                  <li class="nav-item active">
                    <a class="nav-link" href="{{route('carrito')}}"><i class="fas fa-shopping-cart" ></i> <span id="items" class="badge badge-pill badge-danger font-weight-normal" style="position:relative; top:-10px; left:-10px">{{ session()->get('items') }}</span> Mi carrito</a>
                  </li>
                  @endif


                  <li class="nav-item active">
                      <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Cerrar sesión</a>
                  </li>
                  <form id="logout-form" action="{{route('logout') }}" method="POST" style="display:none;"> @csrf</form>
              @endguest
            </ul>
        </div>
    </div>
</nav>
