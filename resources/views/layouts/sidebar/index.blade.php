<nav id="sidebar">
    <a href="{{route('home')}}">
      <div class="sidebar-header">
          <h3>ArTic</h3>
          <strong>AT</strong>
      </div>
<hr />
    </a>
        <ul class="list-unstyled components mb-0">
            <li>
                @guest
                    Bienvenido.
                @else
                <a>
                    <p class="py-0 mb-0 ml-2 text-white">{{ auth()->user()->name }} {{ auth()->user()->apellido }}</p>
                    <p class="py-0 mb-0 ml-2 text-white" style="font-size: smaller;">{{ auth()->user()->rol->nombre }} </p>
                </a>
                @endguest
            </li>
        </ul>
<hr />

<ul class="list-unstyled components pt0">
<!--///////// panel para el ADMINISTRADOR///////////-->
    @if (auth()->user()->rol_id == 1)

        <li>
             <a href="{{route('users')}}">
               <i class="fas fa-user fa-fw"></i>
                 Usuarios
             </a>
         </li>
         <li>
             <a href="#estadistica" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                 <i class="far fa-chart-bar fa-fw"></i>
                 Estadisticas
             </a>
             <ul class="collapse list-unstyled" id="estadistica">
                 <li>
                     <a href="{{route('estadisticas.ingresos')}}">Ingresos</a>
                 </li>

                 <li>
                     <a href="{{route('estadisticas.servicios')}}">Servicios mas contratados</a>
                 </li>
             </ul>
         </li>
    @endif

    <!--///////// panel para el auditor ///////////-->
    @if (auth()->user()->rol_id == 2)
            <li>
                <a href="{{route('logs.todos')}}">

                    Auditorias
                </a>
              </li>
    @endif

<!--///////// panel para el SysAdmin  ///////////-->
@if (auth()->user()->rol_id == 5)
        <li>
            <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                <i class="fas fa-cogs"></i>
                Parametros
            </a>

            <ul class="collapse list-unstyled" id="homeSubmenu">
                <li>
                    <a href="{{route('jefaturas')}} ">Jefatura</a>
                </li>

                <li>
                    <a href="{{route('tipos-materiales')}}">Tipos de Materiales</a>
                </li>

                <li>
                  <a href="{{route('materiales')}}">Materiales</a>
                </li>
            </ul>
        </li>
        <li class="active">
            <li>
                <a href="{{route('servicios')}}">
                    <i class="fas fa-briefcase fa-fw"></i>
                    Servicios
                </a>
             </li>

              <li>
                  <a href="{{route('socios')}}">
                      <i class="far fa-handshake"></i>
                      Socios
                  </a>
                </li>
                <li>
                    <a href="{{route('grupos')}}">
                        <i class="fas fa-users fa-fw"></i>
                        Grupos
                    </a>
                </li>
                <li>
                      <a href="{{route('pagos')}}">
                          <i class="fas fa-money-bill-wave fa-fw"></i>
                          Pagos
                      </a>
                  </li>
                     <li>
                          <a href="{{route('recursos_prestados')}}">
                              <i class="fas fa-file-signature"></i>
                             Prestamos
                          </a>
                    </li>
                    <li>
                        <a href="#estadistica" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                            <i class="far fa-chart-bar fa-fw"></i>
                            Estadisticas
                        </a>
                        <ul class="collapse list-unstyled" id="estadistica">
                            <li>
                                <a href="{{route('estadisticas.materiales')}}">Materiales</a>
                            </li>

                        </ul>
                    </li>

            </li>
    @endif
<!--///////// panel para el Usuario Comun  ///////////-->
@if (auth()->user()->rol_id == 4)
            <li>
                <a href="{{route('servicios')}}">
                    <i class="fas fa-briefcase fa-fw"></i>
                    Servicios
                </a>
              </li>
              <li>
                  <a href="{{route('pedidos')}}">
                     <i class="fas fa-paste fa-fw"></i>
                      Mis Pedidos
                  </a>
                </li>
                <!--/////////
                <li>
                    <a href="#">
                        <i class="fas fa-dollar-sign"></i>
                        Mis pagos
                    </a>
                  </li>///////////-->

    @endif
<!--///////// panel para el Socio  ///////////-->
@if (auth()->user()->rol_id == 3)
              <li>
                  <a href="{{route('asignaciones')}}">
                      <i class="fas fa-tasks fa-fw"></i>
                      Mis asignaciones
                  </a>
                </li>
    @endif
    @if((auth()->user()->rol_id != 1) && (auth()->user()->rol_id != 2))
          <li>
              <a href="{{route('ayuda')}}">
                 <i class="far fa-question-circle fa-fw"></i>
                  Ayuda
              </a>
            </li>
    @endif
</ul>

</nav>
