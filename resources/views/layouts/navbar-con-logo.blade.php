<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">
        <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <i class="fas fa-align-justify"></i>
        </button>
          <a href=""><i class="fas fa-shopping-cart" ></i></a>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="nav navbar-nav ml-auto">
              @guest
                  <li class="nav-item active">
                      <a class="nav-link" href="{{ route('login') }}">Iniciar sesión</a>
                  </li>
                  <li class="nav-item">
                      <a class="nav-link" href="{{ route('register') }}">Registrarse</a>
                  </li>
              @else
                  <li class="nav-item active">
                      <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Cerrar sesión</a>
                  </li>
                  <form id="logout-form" action="{{route('logout') }}" method="POST" style="display:none;"> @csrf</form>
              @endguest
            </ul>
        </div>
    </div>
</nav>
