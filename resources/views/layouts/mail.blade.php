<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
	    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
	    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div style="background-color:#FFFFFF; font-family: 'Nunito', sans-serif; font-size: 14pt;">

			<table height="100%" width="100%" cellpadding="0" cellspacing="0" border="0">
		    	<tr>
		    		<td valign="top" align="left" background="https://i.imgur.com/hIb54CR.png" style="background-size: cover !important">

		    			<table height="100%" width="100%" cellpadding="0" cellspacing="0" border="0" style="margin: 0;">
							<tr style="background-color: #FFFFFF; text-align: center;">
								<td style="padding: 10px;">
									<h1>ArTic</h1>
								</td>
							</tr>
							<tr>
								<td>
									<div style="margin: 20px 15%; padding: 10px; background-color: white; border: 1px solid #333333; border-radius: 10px; font-size: 14pt;">
										@yield('contenido')
									</div>
								</td>
							</tr>

							<tr style="height: 60px; line-height: 60px; text-align: center; color: white; background-color: #343A40;">
								<td>
									ArTic. &copy; 2020 - {{ date('Y') }}
								</td>
							</tr>
						</table>
		    		</td>
		    	</tr>
		    </table>
		</div>

	</body>
</html>
