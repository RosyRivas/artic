
<div class="card">
  <div class="card-header text-center">
    Algunos de nuestros servicios.
  </div>
  <div class="card-body">

  <div class="card-deck">

      @foreach ($servicios as $s)
      <div class="card">
        <img src="images/servicios/{{$s->imagen}}" class="card-img-top" height="300vw"  alt="{{$s->nombre}}">

        <div class="card-body">
          <h5 class="card-title">{{$s->nombre}}</h5>
          <p class="card-text">{{$s->descripcion}}</p>
        </div>
        <div class="card-footer bg-transparent text-center border-0">
          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal{{$s->id}}">
              <i class="fas fa-cart-plus"></i> Añadir al carrito
          </button>

        </div>
      </div>


        <div class="modal fade" id="modal{{$s->id}}" tabindex="-1" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">{{$s->nombre}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                  <p>Seleccione una fecha para contratar el servicio</p>
                  <input type="date" class="form-control" id="fecha{{$s->id}}"
                  name="fecha{{$s->id}}" min="{{now()->format('Y-m-d')}}" required>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="event.preventDefault(); carrito({{$s->id}});">Confirmar</button>
              </div>
            </div>
          </div>
        </div>
      @endforeach
    </div>
  </div>
  <div class="card-footer text-center bg-transparent">
      <a href="{{route('servicios.listado')}}" class="btn btn-success btn-lg">Ver todos los servicios</a>
  </div>

</div>
