<!DOCTYPE html>
<html lang="es" style="position:relative; min-height: 100%;">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>ArTic</title>

    <!--CSRF TOKEN  -->
    <meta name= "csrf-token" content="{{ csrf_token() }}">

    <!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <!-- Our Custom CSS -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">

    <!-- Font Awesome JS -->
    <script defer src="/js/all.js"></script>

    <!-- Fonts  -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
</head>

<body>

    <div class="wrapper">
      @auth
        @include('layouts.sidebar.index')
      @endauth
        <!-- Page Content  -->
        <div id="content" class="container-fluid" >
            <div class="row " style="height: 70px;">
              <div class="col p-0 my-auto">
                  @include('layouts.navbar')
                </div>
            </div>
    <!-- HOME PARA USUARIO CLIENTE  -->

            <div class="row mt-5" style="min-height: calc(100vh - 130px)">
              <div class="col-8 offset-2 py-3">
                @include('home.plantillas.carrousel')
              </div>

              <div class="col-12 my-3">
                @include('home.plantillas.servicios')
              </div>
            </div>

    
            <div class="row bg-dark text-white text-truncate" style="height: 60px;">
                <div class="col my-auto px-3 px-sm-5 text-center">
                    ArTic. &copy; 2020 - {{ date('Y') }}
                </div>
            </div>

         </div>
    </div>

    <!-- jQuery CDN -->
    <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>

    <!-- Popper.JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>

    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <script>
          function carrito(servicio){
              fecha = document.getElementById(`fecha${servicio}`).value;
              $.ajax({
                  type: "GET",
                  dataType: "json",
                  url: `/carrito/agregar/${servicio}/${fecha}`,
                  data: {},
                  success: function(data){
                    alert(data.success);
                    $('#items').html(data.cantidad);
                  }
              });
          }


              $(document).ready(function () {
                  $('#sidebarCollapse').on('click', function () {
                      $('#sidebar').toggleClass('active');
                  });
              });

    </script>
</body>

</html>
