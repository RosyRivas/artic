@extends('layouts.app')

@section('titulo', 'Servicios')

@section('contenido')

<div class="container">
    <div class="card mx-1 my-3 ">
      <div class="row">
       <div class="card-header">
            Contratar servicio
       </div>
       </div>
       <form class="card-body" action="{{route('servicios.contratar')}}"method="post">
         @csrf
            <div class="row">
              <div class="card">
                <div class="card-body">
                  <p class="card-text">{{$s->descripcion}}</p>
                  <a href="#" class="card-link">Anterior</a>
                  <a href="#" class="card-link">Siguiente</a>
                </div>
                </div>
            </div>
       </form>
    </div>
</div>
@endsection
