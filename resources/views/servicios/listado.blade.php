@extends('layouts.app')

@section('titulo', 'Servicios')

@section('contenido')
<div class="container-fluid">
    <div class="row py-3">
        <div class="h2 col">Servicios</div>
    </div>
    <div class="row">
        <form class="card py-3 bg-ligth border-dark container-fluid" action="{{route('servicios.listado')}}" method="GET">

            <div class="row">
                <div class="col-12 font-weight-bold text-center text-uppercase">
                    Filtros
                </div>
                <div class="form-group col-12">
                    <label for="nombre">Nombre</label>
                    <input type="text" class="form-control" id="nombre"
                        name="nombre" value="{{$filtros['nombre']}}">
                </div>
                <div class="form-group col-4">
                    <label for="jefatura">Jefaturas</label>
                    <div class="container-fluid p-0">
                      <select name="jefaturas[]" id="jefaturas[]" class="select2 js-example-basic-multiple form-control "  multiple="multiple">
                          @foreach ($jefaturas as $j)
                              @if (is_array($filtros['jefaturas']))
                                  @if ( in_array($j->id, $filtros['jefaturas']))
                                      <option value="{{ $j->id }}" selected>{{ $j->nombre }}</option>
                                  @else
                                      <option value="{{ $j->id }}">{{ $j->nombre }}</option>
                                  @endif
                              @else
                                  <option value="{{ $j->id }}">{{ $j->nombre }}</option>
                              @endif
                          @endforeach
                      </select>
                    </div>
                </div>
                <div class="form-group col-4">
                      <label for="min">Precio mínimo</label>
                      <input type="number" class="form-control" id="min" name="min"
                          value="{{$filtros['min']}}" min="0.00" step="0.01">
                </div>
                <div class="form-group col-4">
                      <label for="max">Precio máximo</label>
                      <input type="number" class="form-control" id="max" name="max"
                          value="{{$filtros['max']}}" min="0.00" step="0.01">
                </div>
                <div class="col-12 text-center py-2">
                    <button type="submit" class="btn btn-info">filtrar</button>
                </div>
            </div>
        </form>
    </div>
    <div class="row">
        @if (count($servicios) > 0)
            <div class="col-auto mx-auto my-3">
                {{$servicios->links()}}
            </div>

            @foreach($servicios as $s)
                <div class="col-12 px-0">
                    @component('servicios.plantilla')
                        @slot('s', $s)
                    @endcomponent
                  </div>
            @endforeach

            <div class="col-auto mx-auto my-3">
                {{$servicios->links()}}
            </div>
        @else
            <div class="h4 col my-5  text-center">
                No hay resultados.<br>
                Intente con otros parámetros.
            </div>
        @endif
    </div>
</div>

@endsection

@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />

    <style>
      .selection {
        width: 100% !important;
        min-height: 38px !important;
      }
    </style>
@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>

  <script>
    $(document).ready(function() {
        $('.select2').select2({
            theme: 'classic',
            width: '100%',
        });
    });

    function carrito(servicio){
        fecha = document.getElementById(`fecha${servicio}`).value;
        $.ajax({
            type: "GET",
            dataType: "json",
            url: `/carrito/agregar/${servicio}/${fecha}`,
            data: {},
            success: function(data){
              alert(data.success);
              $('#items').html(data.cantidad);
            }
        });
    }
  </script>
@endsection
