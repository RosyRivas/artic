<div class="card w-100 my-3 border-dark container-fluid">
    <div class="row ">
        <div class="col-auto p-0">
            <img src="images/servicios/{{$s->imagen}}" style="height:200px; "  alt="{{$s->nombre}}">
        </div>
        <div class="col">
            <div class="container-fluid h-100">
                <div class="row">
                    <div class="col-12 py-3 h4">
                        {{$s->nombre}}
                    </div>
                    <div class="col-12">
                        {{$s->descripcion}}
                    </div>
                </div>
                <div class="row w-100 pr-3" style="position:absolute; bottom:0;">
                    <div class="col h4 my-auto">
                        $ {{number_format($s->costo, 2, ',', '')}}
                    </div>
                    <div class="col-auto">
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal{{$s->id}}">
                            <i class="fas fa-cart-plus"></i> Añadir al carrito
                        </button>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal{{$s->id}}" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">{{$s->nombre}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <p>Seleccione una fecha para contratar el servicio</p>
          <input type="date" class="form-control" id="fecha{{$s->id}}"
          name="fecha{{$s->id}}" min="{{now()->format('Y-m-d')}}" required>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="event.preventDefault(); carrito({{$s->id}});">Confirmar</button>
      </div>
    </div>
  </div>
</div>
