@extends('layouts.app')

@section('fondo', 'fondo editado 20.jpg')

@section('titulo', 'Logs de auditoría')

@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />

    <style>
      .select2-selection__rendered {
          line-height: 38px !important;
      }

      .select2-container .select2-selection--single {
          height: 38px !important;
      }

      .select2-selection__arrow {
          height: 36px !important;
      }

      .selection {
        width: 100% !important;
        line-height: 38px !important;
      }
    </style>
@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>

    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

    <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js">
    </script>

    <script>
        $(document).ready(function() {
            $('.select2').select2({
                placeholder: 'Seleccione una opción',
                allowClear: true,
                theme: 'classic',
                width: '100%',
            });

            $('#registros').DataTable({
                columns: [
                    { data: 'accion' },
                    { data: 'fecha' },
                    { data: 'tabla' },
                    { data: 'rid' },
                    { data: 'clave' },
                    { data: 'anterior' },
                    { data: 'nuevo' },
                ],

                aaSorting: [],

                searching: false,

                language: {
                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "Mostrar _MENU_ registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "Ningún dato disponible en esta tabla",
                    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            });
        } );
    </script>
@endsection

@section('contenido')
    <div class="p-3" style="min-height: calc(100vh - 144px);">
        <div class="d-block d-md-none mb-4 text-center h2 my-2">
            @yield('titulo')
        </div>

        <div class="d-none d-md-block mb-3 h1">
            @yield('titulo')
        </div>

        @include('auditor.filtros')

        <div class="container-fluid alert alert-warning text-dark py-3 mt-4 mb-3">
            <div class="row text-center text-uppercase font-weight-bold mb-5">
                <div class="col">
                    Filtros aplicados
                </div>
            </div>
            <div class="row justify-content-around text-center font-weight-bold">
                @if (! is_null($request->accion))
                    <div class="col-auto px-1">
                        <div class="">
                            <span class="badge badge-info">ACCIÓN</span>
                        </div>
                        @if ($request->accion == 'C')
                            Creación
                        @elseif ($request->accion == 'M')
                            Modificación
                        @else
                            Borrado
                        @endif
                    </div>
                @endif

                @if (! is_null($request->user_id))
                    <div class="col-auto px-1">
                        <div class="font-weight-bold">
                            <span class="badge badge-info">USUARIO</span>
                        </div>

                        {{ $request->user_id }}
                    </div>
                @endif

                @if (! is_null($request->desde))
                    <div class="col-auto px-1">
                        <div class="font-weight-bold">
                            <span class="badge badge-info">DESDE</span>
                        </div>

                        {{ DateTime::createFromFormat('Y-m-d', $request->desde)->format('d/m/Y') }}
                    </div>
                @endif

                @if (! is_null($request->hasta))
                    <div class="col-auto px-1">
                        <div class="font-weight-bold">
                            <span class="badge badge-info">HASTA</span>
                        </div>

                        {{ DateTime::createFromFormat('Y-m-d', $request->hasta)->format('d/m/Y') }}
                    </div>
                @endif

                @if (! is_null($request->tabla))
                    <div class="col-auto px-1">
                        <div class="font-weight-bold">
                            <span class="badge badge-info">TABLA</span>
                        </div>

                        {{ $request->tabla }}
                    </div>
                @endif

                @if (! is_null($request->rid))
                    <div class="col-auto px-1">
                        <div class="font-weight-bold">
                            <span class="badge badge-info">ID</span>
                        </div>

                        {{ $request->rid }}
                    </div>
                @endif

                @if (! is_null($request->clave))
                    <div class="col-auto px-1">
                        <div class="font-weight-bold">
                            <span class="badge badge-info">CLAVE</span>
                        </div>

                        {{ $request->clave }}
                    </div>
                @endif

                @if (! is_null($request->valor_anterior))
                    <div class="col-auto px-1">
                        <div class="font-weight-bold">
                            <span class="badge badge-info">ANTERIOR</span>
                        </div>

                        {{ $request->valor_anterior }}
                    </div>
                @endif

                @if (! is_null($request->valor_nuevo))
                    <div class="col-auto px-1">
                        <div class="font-weight-bold">
                            <span class="badge badge-info">NUEVO</span>
                        </div>

                        {{ $request->valor_nuevo }}
                    </div>
                @endif
            </div>
        </div>

        <div class="card mt-4 p-3">
            <table id="registros" class="display responsive nowrap w-100">
                <thead>
                    <tr>
                        @if (is_null($request->accion))
                            <th>Acción</th>
                        @endif

                        @if (is_null($request->user_id))
                            <th>Usuario</th>
                        @endif

                        <th>Fecha</th>

                        @if (is_null($request->tabla))
                            <th>Tabla</th>
                        @endif

                        @if (is_null($request->rid))
                            <th>ID</th>
                        @endif

                        @if (is_null($request->clave))
                            <th>Clave</th>
                        @endif

                        @if (is_null($request->valor_anterior))
                            <th>Anterior</th>
                        @endif

                        @if (is_null($request->valor_nuevo))
                            <th>Nuevo</th>
                        @endif
                    </tr>
                </thead>
                <tbody>
                    @foreach ($logs as $l)
                        <tr>
                            @if (is_null($request->accion))
                                <td>
                                    @if ($l->accion == 'C')
                                        <span class="badge badge-success">CREACIÓN</span>
                                    @elseif ($l->accion == 'M')
                                        <span class="badge badge-warning">MODIFICACIÓN</span>
                                    @else
                                        <span class="badge badge-danger">BORRADO</span>
                                    @endif
                                </td>
                            @endif

                            @if (is_null($request->user_id))
                                <td>{{ $l->user_id }}</td>
                            @endif

                            <td>
                                <div class="text-break">
                                    {{ DateTime::createFromFormat('Y-m-d H:i:s', $l->fecha)->format('d/m/Y') }}

                                    <span class="text-muted">
                                        {{ substr($l->fecha, -8) }}
                                    </span>
                                </div>
                            </td>

                            @if (is_null($request->tabla))
                                <td>{{ $l->tabla }}</td>
                            @endif

                            @if (is_null($request->rid))
                                <td>{{ $l->rid }}</td>
                            @endif

                            @if (is_null($request->clave))
                                <td>{{ $l->clave }}</td>
                            @endif

                            @if (is_null($request->valor_anterior))
                                <td>
                                    <span class="text-wrap text-break">
                                        {{ $l->valor_anterior }}
                                    </span>
                                </td>
                            @endif

                            @if (is_null($request->valor_nuevo))
                                <td>
                                    <span class="text-wrap text-break">
                                        {{ $l->valor_nuevo }}
                                    </span>
                                </td>
                            @endif
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
