@extends('layouts.mail')

@section('contenido')
    <p>Hola.</p>
    <p>Danos tu opinión</p>
    <a href="https://imgur.com/myJVYQa"><img heigt="20", width="100" src="https://i.imgur.com/myJVYQa.png" title="source: imgur.com" /></a>
    <br><p>Califica el servicio que has recibido</p>
    <p> {{$datos->servicio->nombre}} - {{date('d/m/y', strtotime($datos->fecha_prestacion))}}</p>

    <p style="text-align: center; margin: 40px auto;">
        <a href="{{ route('servicio.calificar', $datos->id) }}" style="font-family: 'Nunito', sans-serif; color: #FFF; text-decoration: none; font-size: smaller; background-color: #3490DC; border-radius: 3px; border-width: 10px 18px; border-style: solid; border-color: #3490DC;" target="_blank">Calificar</a>
    </p>
@endsection
