@extends('layouts.mail')

@section('contenido')
  <p>Hola</p>
  <p>Ha sido seleccionado para realizar un servicio</p>
  <p>Detalle del servicio:</p>
  <ul>
          <li><b>Fecha del servicio: </b> {{date('d/m/y', strtotime($datos->servicio_contratado->fecha_prestacion))}}</li>
          <li><b>Nombre del servicio: </b> {{$datos->servicio_contratado->servicio->nombre}}</li>
          <li><b>Costo: </b> $ {{number_format($datos->servicio_contratado->servicio->costo, 2, ',', '')}}</li>

  </ul>

  <p style="text-align: center; margin: 40px auto;">
        <a href="{{ route('asignacion.reservar', $datos->id) }}" style="font-family: 'Nunito', sans-serif; color: #FFF; text-decoration: none; font-size: smaller; background-color: #3490DC; border-radius: 3px; border-width: 10px 18px; border-style: solid; border-color: #3490DC;" target="_blank">Aceptar asignacion</a>
    </p>

    <p style="text-align: center; margin: 40px auto;">
          <a href="{{ route('asignacion.rechazar', $datos->id) }}" style="font-family: 'Nunito', sans-serif; color: #FFF; text-decoration: none; font-size: smaller; background-color: #e02f1b; border-radius: 3px; border-width: 10px 18px; border-style: solid; border-color:#e02f1b;" target="_blank">Rechazar asignacion</a>
    </p>
@endsection
