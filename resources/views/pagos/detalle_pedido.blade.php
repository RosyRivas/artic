@extends('layouts.app')

@section('titulo', 'Realizar pago')

@section('contenido')

<div class="container">
  @include('layouts.mensaje')
    <div class="row">
        <div>
            <div class="h2 col-12">Detalles del pedido</div>
            <div class="h4 col-12">Pedido: {{$pedido->id}}</div>
            <div class="h5 col-12">Cliente: {{$pedido->user->apellido}} {{$pedido->user->name}}</div>
            <div class="h5 col-12">Servicios contratados:</div>
            <div class="container-fluid">
                <div class="form-group">
                        <table class="table table-hover col-12">
                            <thead>
                              <tr>
                                  <th>Servicio</th>
                                  <th>Fecha de prestación</th>
                                  <th>Costo</th>
                              </tr>
                            </thead>
                            <tbody >
                                @foreach($pedido->servicios_contratados as $sc)
                                    <tr>
                                        <td>{{$sc->servicio->nombre}}</td>
                                        <td>{{date('d/m/y', strtotime($sc->fecha_prestacion))}}</td>
                                        <td  class="text-right">$ {{number_format($sc->servicio->costo, 2, ',', '')}}</td>
                                    </tr>
                                @endforeach
                                <tr class="text-center">
                                    <th>TOTAL</th>
                                    <th></th>
                                    <th>$ {{number_format($pedido->costo, 2, ',', '')}}</th>
                                </tr>
                                <tr class="text-center">
                                    <th>Seña</th>
                                    <th></th>
                                    <th>$ {{number_format($pedido->senia, 2, ',', '')}}</th>
                                </tr>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                                <tr class="h4">
                                    <th>Saldo a pagar</th>
                                    <th> </th>
                                    <th>$ {{number_format(($pedido->costo-$pedido->senia), 2, ',', '')}}</th>
                                </tr>
                            </tbody>
                        </table>
                        </div>
                        <div class="d-flex justify-content-center mt-4">
                            <a href="{{route('registrar_pago', $pedido)}}" class="btn btn-primary" >
                                Pagar
                            </a>
                        </div>
            </div>
        </div>
    </div>
</div>
@endsection
