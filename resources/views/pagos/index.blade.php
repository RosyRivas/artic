@extends('layouts.app')

@section('titulo', 'Pagos Pendientes')

@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">
@endsection

@section('js')

    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

    <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js">
    </script>

    <script>
        $(document).ready(function() {

            $('#pedidos').DataTable({

                aaSorting: [], /*para que se muestre en el mismo orden que viene del controlador*/

                columnDefs: [
                    {
                        targets: 1,
                        className: 'dt-center',
                    }
                ],

                language: {
                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "Mostrar _MENU_ registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "Ningún dato disponible en esta tabla",
                    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            });
        } );
    </script>
@endsection

@section('contenido')
<div class="container">
  @include('layouts.mensaje')
        <div class="h2 col">Pagos pendientes</div>
        <div class="container-fluid mt-3">
              <table id="pedidos" class="display responsive nowrap w-100">
                    <thead>
                      <tr>
                          <th>Pedido</th>
                          <th>Cliente</th>
                          <th class="text-right">Accion</th>
                      </tr>
                    </thead>
                    <tbody>
                        @forelse($pedidos as $p)
                            <tr>
                                <td>Pedido {{$p->id}}</td>
                                <td>{{$p->user->apellido}} {{$p->user->name}}</td>
                                <td class="text-right">
                                    <a href="{{route('detalle_pedido', $p)}}" class="btn btn-primary" >
                                        <i class="fas fa-eye"></i>
                                    </a>
                                </td>
                            </tr>
                        @empty
                        <tr>
                            <td colspan="4" class="text-center font-italic" >No hay pagos pendientes</td>
                        </tr>
                        @endforelse
                    </tbody>
              </table>
        </div>
</div>

@endsection
