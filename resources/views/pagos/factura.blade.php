<!DOCTYPE html>
<html>
<head>
    <style>
        @page {
            margin: 0cm 0cm;
            font-family: Arial;
        }

        body {
            margin: 3cm 2cm 2cm;
        }

        header {
            position: fixed;
            top: 0cm;
            left: 0cm;
            right: 0cm;
            height: 2.5cm;

            text-align: right;
            line-height: 30px;
            padding-right: 2cm;
            padding-top:1cm;
            font-family: "Hatton";
        }

        footer {
            position: fixed;
            bottom: 0cm;
            left: 0cm;
            right: 0cm;
            height: 3cm;
            color: black;
            text-align: center;
            line-height: 35px;
        }

        thead{
            background-color: #fedbd0;
        }

        main{
            margin-top: 2cm;
        }

        table {
            table-layout: fixed;
            width: 100%;
            border-collapse: collapse;
        }

        thead th:nth-child(1) {
            width: 25%;
        }

        thead th:nth-child(2) {
            width: 25%;
        }

        thead th:nth-child(3) {
            width: 25%;
        }

        thead th:nth-child(4) {
            width: 25%;
        }

        th, td {
            padding: 20px;
            border-collapse: collapse;
        }

        table td:nth-child(4) {
            text-align: right;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
    <body>
        <header>
            <div class="container-fluid">
                <img
                    src="/home/rosa/Documentos/artic/public/images/logoApa.jpg"
                    alt="Logo"
                    width="100px"
                    align="right"
                >
                <div style="text-align: right;" >
                  <h4 >Cooperativa Academia Productura de Arte</h4>
                  <div style="font-size:10pt;">
                    academiaproductoradeartes@gmail.com
                    <br>3758 51-2972
                    <br>San Jose - Misiones
                </div>
            </div>
        </header>

        <main>
            <div class="row">
                <div>
                    <div class="h2 col-12">Detalles del pedido</div>
                    <div class="h4 col-12">Pedido: {{$pedido->id}}</div>
                    <div class="h5 col-12">Cliente: {{$pedido->user->apellido}} {{$pedido->user->name}}</div>
                    <div class="h5 col-12">Servicios contratados:</div>
                    <div class="container-fluid">
                        <div class="form-group">
                                <table class="table table-hover col-12">
                                    <thead>
                                      <tr>
                                          <th>Servicio</th>
                                          <th>Fecha de prestación</th>
                                          <th>Costo</th>
                                      </tr>
                                    </thead>
                                    <tbody >
                                        @foreach($pedido->servicios_contratados as $sc)
                                            <tr>
                                                <td>{{$sc->servicio->nombre}}</td>
                                                <td>{{date('d/m/y', strtotime($sc->fecha_prestacion))}}</td>
                                                <td  class="text-right">$ {{number_format($sc->servicio->costo, 2, ',', '')}}</td>
                                            </tr>
                                        @endforeach
                                        <tr class="text-center">
                                            <th>TOTAL</th>
                                            <th></th>
                                            <th>$ {{number_format($pedido->costo, 2, ',', '')}}</th>
                                        </tr>
                                        <tr class="text-center">
                                            <th>Seña</th>
                                            <th></th>
                                            <th>$ {{number_format($pedido->senia, 2, ',', '')}}</th>
                                        </tr>
                                        <tr>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                        <tr class="h4">
                                            <th>Saldo a pagar</th>
                                            <th> </th>
                                            <th>$ {{number_format(($pedido->costo-$pedido->senia), 2, ',', '')}}</th>
                                        </tr>
                                    </tbody>
                                </table>
                                </div>
                    </div>
                </div>
            </div>
        </main>
        <script type="text/php">
            if (isset($pdf)) {
                $text = "página {PAGE_NUM} de {PAGE_COUNT}";
                $size = 10;
                $font = $fontMetrics->getFont("Arial");
                $width = $fontMetrics->get_text_width($text, $font, $size) / 2;
                $x = ($pdf->get_width() - $width);
                $y = $pdf->get_height() - 35;
                $pdf->page_text($x, $y, $text, $font, $size);
                $pdf->page_text(40, $y, "Generado por: ".Auth::user()->name." - ".now()->format('d/m/Y H:i:s'), $font, $size);
            }
        </script>
    </body>
</html>
