@extends('layouts.app')

@section('titulo', 'Carrito')

@section('contenido')
<div class="container">
  <div class="row">
      <div class="h2 col">Mi carrito</div>
      <div class="col-12 text-center py-3">
          <img src="/images/empty.png" alt="Carrito vacío">
      </div>
      <div class="col-12 font-weight-bold text-center h3">
          Su carrito esta vacío.
      </div>
    </div>
  </div>
@endsection
