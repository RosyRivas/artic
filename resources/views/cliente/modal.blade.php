<div class="modal fade"  id="modal{{$sc->id}}" tabindex="-1"  aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">

    <form action="{{route('calificar', $sc->asignacion)}}" method="post">
      <div class="modal-header">
        <h5 class="modal-title">Califica este servicio</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          @csrf
          <p>{{$sc->servicio->nombre}}<p>
              <img src="images/servicios/{{$sc->servicio->imagen}}" style="height:100px; "  alt="{{$sc->servicio->nombre}}">
              <p>Puntaje:</p>

              <p class="clasificacion">

                <input id="{{$sc->id}}radio1" type="radio" name="estrellas" value="5">
                <label for="{{$sc->id}}radio1">★</label>
                <input id="{{$sc->id}}radio2" type="radio" name="estrellas" value="4">
                <label for="{{$sc->id}}radio2">★</label>
                <input id="{{$sc->id}}radio3" type="radio" name="estrellas" value="3">
                <label for="{{$sc->id}}radio3">★</label>
                <input id="{{$sc->id}}radio4" type="radio" name="estrellas" value="2">
                <label for="{{$sc->id}}radio4">★</label>
                <input id="{{$sc->id}}radio5" type="radio" name="estrellas" value="1">
                <label for="{{$sc->id}}radio5">★</label>
              </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        <button type="submit" class="btn btn-primary">Calificar</button>
      </div>
  </form>
    </div>
  </div>
</div>
