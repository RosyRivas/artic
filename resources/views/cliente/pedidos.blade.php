@extends('layouts.app')

@section('titulo', 'Pedido')

@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">
    <style media="screen">

    p.clasificacion {
      position: relative;
      overflow: hidden;
      display: inline-block;
    }

    p.clasificacion input {
      position: absolute;
      top: -100px;
    }

    p.clasificacion label {
      float: right;
      color: #333;
    }

    p.clasificacion label:hover,
    p.clasificacion label:hover ~ label,
    p.clasificacion input:checked ~ label {
      color: #dd4;
    }

    </style>
@endsection

@section('js')



@section('contenido')
<div class="container">
      <div class="h2">Resumen de pedidos</div>
       <div class="row">
           <div class="col-auto mx-auto my-3">
               {{$pedidos->links()}}
           </div>
       </div>
              <table  class="table table-responsive table-hover">
                  <thead>
                      <tr>
                          <th>Pedido</th>
                          <th>Seña</th>
                          <th>Precio total</th>
                          <th> </th>
                      </tr>
                </thead>
                <tbody>
                    @foreach($pedidos as $p)
                    <tr>
                        <td> Pedido: {{$p->id}}</td>
                        <td>$ {{number_format($p->senia, 2, ',', '.')}}</td>
                        <td> $ {{number_format($p->costo, 2, ',', '.')}}</td>
                        <td>
                            <table class="table table-responsive table-hover">
                                <thead>
                                  <tr>
                                      <th style="min-width: 400px;">Servicio</th>
                                      <th>Fecha</th>
                                      <th class="text-right">Precio</th>
                                      <th></th>
                                  </tr>
                                </thead>
                                <tbody>
                                    @foreach($p->servicios_contratados as $sc )
                                    <tr>

                                      <td>
                                        {{$sc->servicio->nombre}}
                                      </td>

                                      <td>
                                        {{date('d/m/y', strtotime($sc->fecha_prestacion))}}
                                      </td>

                                      <td class="text-right">
                                           $ {{number_format($sc->servicio->costo, 2, ',', '.')}}
                                      </td>

                                      <td>
                                          @if($sc->terminado)
                                            @if(is_null($sc->asignacion->calificacion->puntaje))
                                              <button id="{{$sc->id}}" type="button" class="btn btn-primary" data-toggle="modal"  data-target="#modal{{$sc->id}}" data-show="true">
                                                 Calificar
                                              </button>
                                              @include('cliente.modal')
                                              @endif
                                          @endif

                                      </td>
                                  </tr>
                                  @endforeach
                              </tbody>
                            </table>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
              </table>

</div>
@endsection
