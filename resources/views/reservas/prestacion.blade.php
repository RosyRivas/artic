@extends('layouts.app')

@section('titulo', 'Prestamos')

@section('contenido')
<div class="container">
  @include('layouts.mensaje')
    <div class="row">
        <div class="h2 col-12">Préstamos de materiales </div>
        <div class="card-body">

            <div class="form-group">
              <table class="col-12 m-0 w-100 table table-hover">

                <thead>
                  <tr>
                      <th>Socio</th>
                      <th class="text-right">Accion</th>
                  </tr>
                </thead>
                <tbody>
                    @forelse($socios as $s)
                        <tr>
                            <td>{{$s->nombres}} {{$s->apellidos}}</td>

                            <td class="text-right">
                                <a href="{{route('prestamos',$s)}}" class="btn btn-primary">
                                    <i class="fas fa-eye"></i>
                                </a>
                            </td>
                        </tr>

                    @empty
                        <tr>
                            <td colspan="4" class="text-center font-italic" >No hay devolucion de materiales pendientes</td>
                        </tr>
                    @endforelse
                </tbody>
              </table>


            </div>
        </div>
    </div>
</div>

@endsection
