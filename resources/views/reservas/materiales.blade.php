@extends('layouts.app')

@section('titulo', 'Materiales')

@section('contenido')
<div class="container">
  @include('layouts.mensaje')
    <div class="row">
        <div class="h2 col-12">Materiales </div>
        <div class="card-body">
            <form  action ="{{route('reservas.devolucion')}}" method="post">
                @csrf
              <table class="col-12 m-0 w-100 table table-hover">
                <thead>
                  <tr>
                      <th>check</th>
                      <th>Fecha del préstamo</th>
                      <th>Cantidad</th>
                      <th>Material</th>
                      <th>Estado</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach($recursos as $r )
                    <tr>
                        <td> <input type="checkbox" name="materiales[{{$r->id}}]"/></td>
                        <td class="text-center">{{date('d/m/y', strtotime($r->fecha_prestamo))}}</td>
                        <td class="text-center">{{$r->cantidad}}</td>
                        <td> {{$r->material->nombre}}</td>
                        <td> <input type="text" name="estados[{{$r->id}}]"></td>
                    </tr>
                    @endforeach
                </tbody>
              </table>
            <div class="d-flex justify-content-center mt-4">
                <button type="submit" class="btn btn-primary">Confirmar devolución</button>
            </div>
        </div>
    </div>
</div>
@endsection
