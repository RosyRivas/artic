<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Su contraseña ha sido restablecida.',
    'sent' => 'Enviamos el enlace de restablecimiento a su correo electronico.',
    'throttled' => 'Aguarde antes de reintentar',
    'token' => 'El token de restablecimiento no es valido.',
    'user' => "No encontramos un usuario con ese correo electronico.",

];
