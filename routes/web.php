<?php

Route::get('/', 'HomeController@index')->name('home');

Route::get('carrito', 'ServicioController@carrito')->name('carrito');
Route::get('carrito/agregar/{servicio}/{fecha}', 'ServicioController@agregar');
Route::get('carrito/quitar/{servicio}', 'ServicioController@quitar');
Route::post('carrito/pagar', 'PagoController@pagar')->name('carrito.pagar');

Route::get('pedidos', 'PedidoController@index')->name('pedidos');
// buscar servicio en el catalogo
Route::get('buscar/{servicio}', 'ServicioController@buscar')->name('filtro-servicio');

Route::get('servicios' , 'ServicioController@listado')->name('servicios.listado');



Route::get('recursos_prestados' , 'ServicioController@materiales_prestados')->name('recursos_prestados');

Route::get('recursos_prestados/{socio}' , 'ServicioController@prestamos')->name('prestamos');

Route::post('devolucion' , 'ServicioController@devolucion')->name('reservas.devolucion');

Route::post('asignacion/{sc}' , 'ServicioController@terminar_prestacion')->name('socios.asignaciones');

// Manual de Usuario
Route::prefix('ayuda')->group(function () {
        Route::get('/','AyudaController@index')->name('ayuda');
        Route::get('video/{video}','AyudaController@video')->name('ayuda.video');

});


// MERCADOPAGO
Route::prefix('pagos')->group(function () {

    Route::view('failure', 'pagos.failure')->name('failure');

    Route::view('pending', 'pagos.pending')->name('pending');

});


Route::match(['get', 'post'], 'notificaciones', 'NotificacionesController@ipn_mercadopago')
    ->name('notificacion');

Auth::routes();
Route::middleware(['auth', 'sysadmin'])->prefix ('datos')->group(function(){
    //////////////// Jefaturas /////////////////////
        Route::prefix('jefaturas')-> group(function(){

            Route::get('/','JefaturaController@index')->name('jefaturas');

            Route::get('crear', 'JefaturaController@crear')->name('jefaturas.crear');

            Route::post('crear', 'JefaturaController@almacenar');

            Route::get('editar/{jefatura}', 'JefaturaController@editar')
                  ->where('jefatura', '[0-9]+')->name('jefaturas.editar');

            Route::put('editar/{jefatura}', 'JefaturaController@actualizar')
                  ->where('jefatura', '[0-9]+'); /* el where limita que el parametro sea sFoundationolo nro */

            Route::delete('eliminar/{jefatura}', 'JefaturaController@eliminar')
                  ->where('jefatura', '[0-9]+')->name('jefaturas.eliminar');
        });
        /////////////////  Tipo de Material  ////////////
            Route::prefix('tipos_materiales')-> group(function(){

                Route::get('/','TipoMaterialController@index')->name('tipos-materiales');

                Route::get('crear', 'TipoMaterialController@crear')->name('tipos-materiales.crear');

                Route::post('crear', 'TipoMaterialController@almacenar');

                Route::get('editar/{tipo_material}', 'TipoMaterialController@editar')
                      ->where('tipo_material', '[0-9]+')->name('tipos-materiales.editar');

                Route::put('editar/{tipo_material}', 'TipoMaterialController@actualizar')
                      ->where('tipo_material', '[0-9]+'); /* el where limita que el parametro sea solo nro */

                Route::delete('eliminar/{tipo_material}', 'TipoMaterialController@eliminar')
                      ->where('tipo_material', '[0-9]+')->name('tipos-materiales.eliminar');
            });


    /////////////////   Material ////////////////
        Route::prefix('materiales')-> group(function(){

            Route::get('/','MaterialController@index')->name('materiales');

            Route::get('crear', 'MaterialController@crear')->name('materiales.crear');

            Route::post('crear', 'MaterialController@almacenar');

            Route::get('editar/{material}', 'MaterialController@editar')
                  ->where('material', '[0-9]+')->name('materiales.editar');

            Route::put('editar/{material}', 'MaterialController@actualizar')
                  ->where('material', '[0-9]+'); /* el where limita que el parametro sea solo nro */

            Route::delete('eliminar/{material}', 'MaterialController@eliminar')
                  ->where('material', '[0-9]+')->name('materiales.eliminar');
        });
    /////////////////   Servicios  ////////////////
        Route::prefix('servicios')-> group(function(){

            Route::get('/','ServicioController@index')->name('servicios');

            Route::get('crear', 'ServicioController@crear')->name('servicios.crear');

            Route::post('crear', 'ServicioController@almacenar');

            Route::get('editar/{servicio}', 'ServicioController@editar')
                  ->where('servicio', '[0-9]+')->name('servicios.editar');

            Route::put('editar/{servicio}', 'ServicioController@actualizar')
                  ->where('servicio', '[0-9]+'); /* el where limita que el parametro sea solo nro */

            Route::get('visualizar/{servicio}', 'ServicioController@visualizar')
                  ->where('servicio', '[0-9]+')->name('servicios.visualizar');

            Route::delete('eliminar/{servicio}', 'ServicioController@eliminar')
                  ->where('servicio', '[0-9]+')->name('servicios.eliminar');

            Route::match(['get' , 'post'],'/', 'ServicioController@index' )->name('servicios');

            Route::get('habilitar/{servicio}/{status}', 'ServicioController@habilitar')
                  ->where('servicio', '[0-9]+')->where('status', '[0,1]');

        });

        /////////////////   Socio ////////////////
            Route::prefix('socios')-> group(function(){

                Route::get('/','SocioController@index')->name('socios');

                Route::get('crear', 'SocioController@crear')->name('socios.crear');

                Route::post('crear', 'SocioController@almacenar');

                Route::get('editar/{socio}', 'SocioController@editar')
                      ->where('socio', '[0-9]+')->name('socios.editar');

                Route::put('editar/{socio}', 'SocioController@actualizar')
                      ->where('socio', '[0-9]+'); /* el where limita que el parametro sea solo nro */

                Route::delete('eliminar/{socio}', 'SocioController@eliminar')
                        ->where('socio', '[0-9]+')->name('socios.eliminar');

                Route::get('visualizar/{socio}', 'SocioController@visualizar')
                        ->where('socio', '[0-9]+')->name('socios.visualizar');


            });

            /////////////////  Grupos ////////////////
                Route::prefix('grupos')-> group(function(){

                    Route::get('/','GrupoController@index')->name('grupos');

                    Route::get('crear', 'GrupoController@crear')->name('grupos.crear');

                    Route::post('crear', 'GrupoController@almacenar');

                    Route::get('editar/{grupo}', 'GrupoController@editar')
                          ->where('grupo', '[0-9]+')->name('grupos.editar');

                    Route::put('editar/{grupo}', 'GrupoController@actualizar')
                          ->where('grupo', '[0-9]+'); /* el where limita que el parametro sea solo nro */

                    Route::delete('eliminar/{grupo}', 'GrupoController@eliminar')
                          ->where('grupo', '[0-9]+')->name('grupos.eliminar');

                    Route::get('visualizar/{grupo}', 'GrupoController@visualizar')
                            ->where('grupos', '[0-9]+')->name('grupos.visualizar');


                });


               Route::prefix('pagos')-> group(function(){
                   Route::get('pagos', 'PagoController@ver_pagos')->name('pagos');
                   Route::get('pagos/{pedido}' , 'PagoController@pago_pedido')->name('detalle_pedido');
                   Route::get('pagos_registro/{pedido}' , 'PagoController@registrar_pago')->name('registrar_pago');
               });

               Route::prefix('estadisticas')-> group(function(){
                   Route::match(['get','post'], 'materiales','EstadisticaController@materiales_mas_reservados')->name('estadisticas.materiales');
                  // Route::match(['get','post'],'servicios','EstadisticaController@servicios_mas_pedidos')->name('estadisticas.servicios');
               });


});
Route::middleware(['auth', 'administrador'])->prefix('administrador')->group(function () {

    //////////////// User /////////////////////
        Route::prefix('users')-> group(function(){

            Route::get('/','UserController@index')->name('users');

            Route::get('crear', 'UserController@crear')->name('users.crear');

            Route::post('crear', 'UserController@almacenar');

            Route::get('editar/{user}', 'UserController@editar')
                  ->where('user', '[0-9]+')->name('users.editar');

            Route::put('editar/{user}', 'UserController@actualizar')
                  ->where('user', '[0-9]+'); /* el where limita que el parametro sea sFoundationolo nro */

            Route::delete('eliminar/{user}', 'UserController@eliminar')
                  ->where('user', '[0-9]+')->name('users.eliminar');
        });


    Route::prefix('estadisticas')-> group(function(){
        Route::match(['get','post'], 'ingresos','EstadisticaController@ingresos')->name('estadisticas.ingresos');
        Route::match(['get','post'],'servicios','EstadisticaController@servicios_mas_pedidos')->name('estadisticas.servicios');
    });

});

// AUDITOR
Route::middleware(['auth', 'auditor'])
    ->prefix('auditor')->group(function () {

    Route::get('logs', 'AuditoriaController@todos')->name('logs.todos');

    Route::post('logs', 'AuditoriaController@filtrados')->name('logs.filtrados');

});
///////// SOCIO
Route::middleware(['auth', 'socio'])->prefix ('servicios')->group(function(){
    Route::get('rechazar/{asignacion}' , 'ServicioController@rechazar_asignacion')
    ->where('asignacion', '[0-9]+')->name('asignacion.rechazar');

    Route::post('rechazar/{asignacion}' , 'ServicioController@guardar_rechazo')
    ->where('asignacion', '[0-9]+');

    Route::get('reservar/{asignacion}', 'ServicioController@reservar_materiales')
    ->where('asignacion', '[0-9]+')->name('asignacion.reservar');

    Route::post('reservar/{asignacion}' , 'ServicioController@aceptar_asignacion')
    ->where('asignacion', '[0-9]+');

    Route::get('reserva_forzosa/{asignacion}', 'ServicioController@materiales_reserva')
    ->where('asignacion', '[0-9]+')->name('asignacion.reserva_forzosa');

    Route::post('reserva_forzosa/{asignacion}', 'ServicioController@recursos_asignacion_forzosa')
    ->where('asignacion', '[0-9]+');

    Route::get('asignaciones', 'ServicioController@listar_asignacion')->name('asignaciones');


});
////// USUARIO
Route::middleware(['auth', 'usuario'])->prefix ('servicios')->group(function(){
      Route::get('contratar/{servicio}', 'ServicioController@contratar')
              ->where('servicio', '[0-9]+')->name('servicio.contratar');

      Route::post('calificando/{asignacion}', 'ServicioController@calificar')
            ->where('asignacion', '[0-9]+')->name('calificar');

      Route::get('calificar/{asignacion}', 'PedidoController@index')
      ->where('asignacion', '[0-9]+')->name('servicio.calificar');
});
