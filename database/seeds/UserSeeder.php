<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{

    public function run()
    {
        //user 1
        User::create ([
            'name'  => 'Rosa Rivas',
            'apellido'  => 'Rivas',
            'email'  => 'sysartic@gmail.com',
            'password'=> bcrypt('administrador'),
            'saldo'  => '500',
            'rol_id'=> 1,
        ]);
        //user 2
        User::create ([
            'name'  => 'Mirian  Rodriguez',
            'apellido'  => 'Rodriguez',
            'email'  => 'mirian@hotmail.com',
            'password'=> bcrypt('auditora'),
            'saldo'  => '500',
            'rol_id'=> 2,
        ]);
        //user 3
        User::create ([
            'name'  => 'Alejandro',
            'apellido'  => 'Pezuk',
            'email'  => 'ale@hotmail.com',
            'password'=> bcrypt('alejandro'),
            'saldo'  => '500',
            'rol_id'=> 3,
        ]);
        //user 4
        User::create ([
            'name'  => 'Tatiana',
            'apellido'  => 'Krayeski',
            'email'  => 'tati@hotmail.com',
            'password'=> bcrypt('tatiana8'),
            'saldo'  => '500',
            'rol_id'=> 4,
        ]);
        //user 5
        User::create ([
            'name'  => 'Marta',
            'apellido'  => 'Benitez',
            'email'  => 'marta@hotmail.com',
            'password'=> bcrypt('tatiana8'),
            'saldo'  => '500',
            'rol_id'=> 4,
        ]);
        //user 6
        User::create ([
            'name'  => 'Jorge',
            'apellido'  => 'Rodriguez',
            'email'  => 'jorge@hotmail.com',
            'password'=> bcrypt('rodriguez'),
            'saldo'  => '500',
            'rol_id'=> 5,
        ]);
        //user 7
        User::create ([
            'name'  => 'Agustina',
            'apellido'  => 'Maidana',
            'email'  => 'agus@hotmail.com',
            'password'=> bcrypt('agustinita02'),
            'saldo'  => '500',
            'rol_id'=> 5,
        ]);
        //user 8
        User::create ([
            'name'  => 'Mario',
            'apellido'  => 'Rivas',
            'email'  => 'rivasMario@gmail.com',
            'password'=> bcrypt('mario'),
            'saldo'  => '500',
            'rol_id'=> 3,
        ]);
        //user 9
        User::create ([
            'name'  => 'Manuel',
            'apellido'  => 'Rivas Rodriguez',
            'email'  => 'manu@hotmail.com',
            'password'=> bcrypt('manu'),
            'saldo'  => '500',
            'rol_id'=> 3,
        ]);
        //user 10
        User::create ([
            'name'  => 'Gabriela',
            'apellido'  => 'Rivas Rodriguez',
            'email'  => 'rossyrivas4018@gmail.com',
            'password'=> bcrypt('gabriela1'),
            'saldo'  => '500',
            'rol_id'=> 3,
        ]);
        //user 11
        User::create ([
            'name'  => 'Rene',
            'apellido'  => 'Rivas Rodriguez',
            'email'  => 'rene@hotmail.com',
            'password'=> bcrypt('rene'),
            'saldo'  => '500',
            'rol_id'=> 3,
        ]);
        //user 12
        User::create ([
            'name'  => 'Adriana',
            'apellido'  => 'Rivas Rodriguez',
            'email'  => 'adri@hotmail.com',
            'password'=> bcrypt('adri'),
            'saldo'  => '500',
            'rol_id'=> 3,
        ]);
        //user 13
        User::create ([
            'name'  => 'Mirta',
            'apellido'  => 'Rodriguez',
            'email'  => 'mirta@hotmail.com',
            'password'=> bcrypt('mirta'),
            'saldo'  => '500',
            'rol_id'=> 3,
        ]);
        //user 14
        User::create ([
            'name'  => 'Domingo',
            'apellido'  => 'Rivas',
            'email'  => 'domingor@hotmail.com',
            'password'=> bcrypt('domingo'),
            'saldo'  => '500',
            'rol_id'=> 3,
        ]);
        //user 15
        User::create ([
            'name'  => 'Anicia',
            'apellido'  => 'Rivas',
            'email'  => 'anicia@hotmail.com',
            'password'=> bcrypt('anicia'),
            'saldo'  => '500',
            'rol_id'=> 3,
        ]);
        //user 16
        User::create ([
            'name'  => 'Vanesa',
            'apellido'  => 'Pinto',
            'email'  => 'vane@hotmail.com',
            'password'=> bcrypt('vane'),
            'saldo'  => '500',
            'rol_id'=> 3,
        ]);
        //user 17
        User::create ([
            'name'  => 'Jose',
            'apellido'  => 'Rios',
            'email'  => 'jose@hotmail.com',
            'password'=> bcrypt('jose'),
            'saldo'  => '500',
            'rol_id'=> 3,
        ]);
        //user 18
        User::create ([
            'name'  => 'Alejandro',
            'apellido'  => 'Andrusizyn',
            'email'  => 'andru@hotmail.com',
            'password'=> bcrypt('ale'),
            'saldo'  => '500',
            'rol_id'=> 3,
        ]);
        //user 19
        User::create ([
            'name'  => 'Malena',
            'apellido'  => 'Pintos ',
            'email'  => 'male@hotmail.com',
            'password'=> bcrypt('male'),
            'saldo'  => '500',
            'rol_id'=> 3,
        ]);
        //user 20
        User::create ([
            'name'  => 'Malena  ',
            'apellido'  => 'Pintos',
            'email'  => 'caba@hotmail.com',
            'password'=> bcrypt('gior'),
            'saldo'  => '500',
            'rol_id'=> 3,
        ]);
        //user 21
        User::create ([
            'name'  => 'Mauricio',
            'apellido'  => 'Perez ',
            'email'  => 'mau@hotmail.com',
            'password'=> bcrypt('mau'),
            'saldo'  => '500',
            'rol_id'=> 3,
        ]);
        //user 22
        User::create ([
            'name'  => 'Gabriel',
            'apellido'  => 'Pezuk',
            'email'  => 'alejandropezuk@gmail.com',
            'password'=> bcrypt('alejandro'),
            'saldo'  => '500',
            'rol_id'=> 3,
        ]);
        User::create ([
            'name'  => 'Romina',
            'apellido'  => 'Benitez',
            'email'  => 'rosaestherrodriguezrivas@gmail.com',
            'password'=> bcrypt('mirianitati'),
            'saldo'  => '500',
            'rol_id'=> 4,
        ]);
        User::create ([
            'name'  => 'Abigail',
            'apellido'  => 'Rodriguez',
            'email'  => 'abirodri@gmail.com',
            'password'=> bcrypt('abigail23'),
            'saldo'  => '500',
            'rol_id'=> 4,
        ]);
    }
}
