<?php

use Illuminate\Database\Seeder;

class ServicioSocioSeeder extends Seeder
{
    public function run()
    {
        /////////
        // servicio- socios murales
        //////////
          //vanesa
          DB::table('servicio_socio')->insert([
                  'servicio_id' => '1',
                  'socio_id' => '9',
          ]);

          /////////
          // servicio- socios fotografia
          ////////

          //gabriela
          DB::table('servicio_socio')->insert([
            'servicio_id' => '2',
            'socio_id' => '3',
          ]);

          //anicia
          DB::table('servicio_socio')->insert([
                  'servicio_id' => '2',
                  'socio_id' => '8',
          ]);
          /////////
          //vanesa
          DB::table('servicio_socio')->insert([
            'servicio_id' => '2',
            'socio_id' => '9',
          ]);

          // servicio-socios retrato
          ////////
          ///mario
          DB::table('servicio_socio')->insert([
                  'servicio_id' => '3',
                  'socio_id' => '1',
          ]);
          //manuel
          DB::table('servicio_socio')->insert([
                  'servicio_id' => '3',
                  'socio_id' => '2',
          ]);
          //vanesa
          DB::table('servicio_socio')->insert([
                  'servicio_id' => '3',
                  'socio_id' => '9',
          ]);

          /////////
          // servicio- socios chinche teatro
          ////////
          //mario
          DB::table('servicio_socio')->insert([
                  'servicio_id' => '4',
                  'socio_id' => '1',
          ]);
          ///gabriel
          DB::table('servicio_socio')->insert([
                  'servicio_id' => '4',
                  'socio_id' => '15',
          ]);


          /////////
          // servicio- socios historia de vida
          ////////
          // rene
          DB::table('servicio_socio')->insert([
                  'servicio_id' => '6',
                  'socio_id' => '4',
          ]);
          //adriana
          DB::table('servicio_socio')->insert([
                  'servicio_id' => '6',
                  'socio_id' => '5',
          ]);
          //manuel
          DB::table('servicio_socio')->insert([
                  'servicio_id' => '6',
                  'socio_id' => '3',
          ]);
          //

          ////////
          // servicio- sillas
          ////////
          // jose rios
          DB::table('servicio_socio')->insert([
                  'servicio_id' => '7',
                  'socio_id' => '10',
          ]);
          //alejandro
          DB::table('servicio_socio')->insert([
                  'servicio_id' => '7',
                  'socio_id' => '11',
          ]);
          ////////
          // servicio- equipos alquiler
          ////////
          //alejandro
          DB::table('servicio_socio')->insert([
                  'servicio_id' => '8',
                  'socio_id' => '11',
          ]);
          ////////
          // servicio- animacion de eventos
          ////////
          //alejandro
          DB::table('servicio_socio')->insert([
                  'servicio_id' => '9',
                  'socio_id' => '11',
          ]);
          ////////
          // servicio- equipos animacion de eventos infantiles
          ////////
          ///gabriela
          DB::table('servicio_socio')->insert([
                  'servicio_id' => '10',
                  'socio_id' => '3',
          ]);


    }
}
