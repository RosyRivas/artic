<?php

use Illuminate\Database\Seeder;

class CalificacionSeeder extends Seeder
{

    public function run()
    {

        DB::table('calificaciones')->insert([
                'fecha' => '2021-01-11',
                'puntaje' => '3',
                'servicio_id' => '1',
                'asignacion_id' => '1',
                'socio_id' => '9',
        ]);

        DB::table('calificaciones')->insert([
                'fecha' => '2021-01-12',
                'puntaje' => '3',
                'servicio_id' => '2',
                'asignacion_id' => '2',
                'socio_id' => '3',
        ]);

        DB::table('calificaciones')->insert([
                'fecha' => '2021-01-13',
                'puntaje' => '3',
                'servicio_id' => '2',
                'asignacion_id' => '3',
                'socio_id' => '8',
        ]);

        DB::table('calificaciones')->insert([
                'fecha' => '2021-01-14',
                'puntaje' => '3',
                'servicio_id' => '2',
                'asignacion_id' => '4',
                'socio_id' => '9',
        ]);


        DB::table('calificaciones')->insert([
                'fecha' => '2021-01-15',
                'puntaje' => '3',
                'servicio_id' => '3',
                'asignacion_id' => '5',
                'socio_id' => '1',
        ]);

        DB::table('calificaciones')->insert([
                'fecha' => '2021-01-16',
                'puntaje' => '3',
                'servicio_id' => '3',
                'asignacion_id' => '6',
                'socio_id' => '2',
        ]);

        DB::table('calificaciones')->insert([
                'fecha' => '2021-01-17',
                'puntaje' => '3',
                'servicio_id' => '3',
                'asignacion_id' => '7',
                'socio_id' => '9',
        ]);

        DB::table('calificaciones')->insert([
                'fecha' => '2021-01-18',
                'puntaje' => '3',
                'servicio_id' => '4',
                'asignacion_id' => '8',
                'socio_id' => '1',
        ]);

        DB::table('calificaciones')->insert([
                'fecha' => '2021-01-18',
                'puntaje' => '3',
                'servicio_id' => '4',
                'asignacion_id' => '9',
                'socio_id' => '15',
        ]);

        DB::table('calificaciones')->insert([
                'fecha' => '2021-01-18',
                'puntaje' => '3',
                'servicio_id' => '6',
                'asignacion_id' => '10',
                'socio_id' => '4',
        ]);

        DB::table('calificaciones')->insert([
                'fecha' => '2021-01-19',
                'puntaje' => '3',
                'servicio_id' => '6',
                'asignacion_id' => '11',
                'socio_id' => '5',
        ]);

        DB::table('calificaciones')->insert([
                'fecha' => '2021-01-19',
                'puntaje' => '3',
                'servicio_id' => '6',
                'asignacion_id' => '12',
                'socio_id' => '3',
        ]);


        DB::table('calificaciones')->insert([
                'fecha' => '2021-01-19',
                'puntaje' => '3',
                'servicio_id' => '7',
                'asignacion_id' => '13',
                'socio_id' => '10',
        ]);

        DB::table('calificaciones')->insert([
                'fecha' => '2021-01-20',
                'puntaje' => '3',
                'servicio_id' => '7',
                'asignacion_id' => '14',
                'socio_id' => '11',
        ]);


        DB::table('calificaciones')->insert([
                'fecha' => '2021-01-21',
                'puntaje' => '3',
                'servicio_id' => '8',
                'asignacion_id' => '15',
                'socio_id' => '11',
        ]);

        DB::table('calificaciones')->insert([
                'fecha' => '2021-01-21',
                'puntaje' => '3',
                'servicio_id' => '9',
                'asignacion_id' => '16',
                'socio_id' => '11',
        ]);

        DB::table('calificaciones')->insert([
                'fecha' => '2021-01-21',
                'puntaje' => '3',
                'servicio_id' => '10',
                'asignacion_id' => '17',
                'socio_id' => '3',
        ]);

        DB::table('calificaciones')->insert([
                'fecha' => '2021-01-21',
                'puntaje' => '3',
                'servicio_id' => '5',
                'asignacion_id' => '18',
                'grupo_id' => '4',
        ]);

        DB::table('calificaciones')->insert([
                'fecha' => '2021-01-21',
                'puntaje' => '3',
                'servicio_id' => '6',
                'asignacion_id' => '19',
                'grupo_id' => '2',
        ]);
        DB::table('calificaciones')->insert([
                'fecha' => '2021-01-21',
                'puntaje' => '3',
                'servicio_id' => '2',
                'asignacion_id' => '20',
                'socio_id' => '3',
        ]);
        DB::table('calificaciones')->insert([
                'fecha' => '2021-01-21',
                'puntaje' => '3',
                'servicio_id' => '3',
                'asignacion_id' => '21',
                'socio_id' => '9',
        ]);
    }
}
