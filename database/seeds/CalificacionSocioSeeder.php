<?php

use Illuminate\Database\Seeder;

class CalificacionSocioSeeder extends Seeder
{

    public function run()
    {
        ///socio   - servicio
        /// vanesa - murales
        DB::table('calificaciones_socios')->insert([
                'socio_id' => 9,
                'servicio_id' => 1,
                'calificacion' => 3,
        ]);

        // gabriela - fotografia
        DB::table('calificaciones_socios')->insert([
                'socio_id' => 3,
                'servicio_id' => 2,
                'calificacion' => 3,
        ]);
        // anicia -fotografia
        DB::table('calificaciones_socios')->insert([
                'socio_id' => 8,
                'servicio_id' => 2,
                'calificacion' => 3,
        ]);
        //vanesa - fotografia
        DB::table('calificaciones_socios')->insert([
                'socio_id' => 9,
                'servicio_id' => 2,
                'calificacion' => 3,
        ]);
        //mario - retrato
        DB::table('calificaciones_socios')->insert([
                'socio_id' => 1,
                'servicio_id' => 3,
                'calificacion' => 3,
        ]);
        //manuel - retrato
        DB::table('calificaciones_socios')->insert([
                'socio_id' => 2,
                'servicio_id' => 3,
                'calificacion' => 3,
        ]);
        //vanesa - retrato
        DB::table('calificaciones_socios')->insert([
                'socio_id' => 9,
                'servicio_id' => 3,
                'calificacion' => 3,
        ]);
        //mario - chinche
        DB::table('calificaciones_socios')->insert([
                'socio_id' => 1,
                'servicio_id' => 4,
                'calificacion' => 3,
        ]);
        //gabriel - chinche
        DB::table('calificaciones_socios')->insert([
                'socio_id' => 15,
                'servicio_id' => 4,
                'calificacion' => 3,
        ]);
        //rene - historia de vida
        DB::table('calificaciones_socios')->insert([
                'socio_id' => 4,
                'servicio_id' => 6,
                'calificacion' => 3,
        ]);
        //adriana - historia de vida
        DB::table('calificaciones_socios')->insert([
                'socio_id' => 5,
                'servicio_id' => 6,
                'calificacion' => 3,
        ]);
        //rene - historia de vida
        DB::table('calificaciones_socios')->insert([
                'socio_id' => 3,
                'servicio_id' => 6,
                'calificacion' => 3,
        ]);
        //jose - sillas alquiler
        DB::table('calificaciones_socios')->insert([
                'socio_id' => 10,
                'servicio_id' => 7,
                'calificacion' => 3,
        ]);
        // Alejandro - sillas alquiler
        DB::table('calificaciones_socios')->insert([
                'socio_id' => 11,
                'servicio_id' => 7,
                'calificacion' => 3,
        ]);
        // Alejandro - alquiler de equipo de sonido
        DB::table('calificaciones_socios')->insert([
                'socio_id' => 11,
                'servicio_id' => 8,
                'calificacion' => 3,
        ]);
        //servicio - animacion de eventos
        DB::table('calificaciones_socios')->insert([
                'socio_id' => 11,
                'servicio_id' => 9,
                'calificacion' => 3,
        ]);
        //servicio - animacion de eventos infantiles
        DB::table('calificaciones_socios')->insert([
                'socio_id' => 3,
                'servicio_id' => 10,
                'calificacion' => 3,
        ]);
    }
}
