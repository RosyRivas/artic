<?php

use Illuminate\Database\Seeder;

class GrupoServicioSeeder extends Seeder
{

    public function run()
    {
        /// show jazz
      DB::table('grupo_servicio')->insert([
              'grupo_id' => '4',
              'servicio_id' => '5',
      ]);
      /// creator producciones
      DB::table('grupo_servicio')->insert([
              'grupo_id' => '2',
              'servicio_id' => '6',
      ]);
    }
}
