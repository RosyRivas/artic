<?php

use Illuminate\Database\Seeder;

class AsignacionSeeder extends Seeder
{

    public function run()
    {
            /// asignaciones a socios
            ///vanesa mural
            DB::table('asignaciones')->insert([
                    'aceptado' => '1',
                    'es_grupo' => 0,
                    'motivo' => null,
                    'servicio_contratado_id' => 1,
                    'socio_id' => 9,
            ]);
            ///gabriela foto
            DB::table('asignaciones')->insert([
                    'aceptado' => '1',
                    'es_grupo' => 0,
                    'motivo' => null,
                    'servicio_contratado_id' => 2,
                    'socio_id' => 3,
            ]);
            ///anicia foto
            DB::table('asignaciones')->insert([
                    'aceptado' => '1',
                    'es_grupo' => 0,
                    'motivo' => null,
                    'servicio_contratado_id' => 3,
                    'socio_id' => 8,
            ]);
            ///vanesa foto
            DB::table('asignaciones')->insert([
                    'aceptado' => '1',
                    'es_grupo' => 0,
                    'motivo' => null,
                    'servicio_contratado_id' => 4,
                    'socio_id' => 9,
            ]);
            ///mario - retrato
            DB::table('asignaciones')->insert([
                    'aceptado' => '1',
                    'es_grupo' => 0,
                    'motivo' => null,
                    'servicio_contratado_id' => 5,
                    'socio_id' => 1,
            ]);
            ///manuel - retrato
            DB::table('asignaciones')->insert([
                    'aceptado' => '1',
                    'es_grupo' => 0,
                    'motivo' => null,
                    'servicio_contratado_id' => 6,
                    'socio_id' => 2,
            ]);
            ///vanesa - retrato
            DB::table('asignaciones')->insert([
                    'aceptado' => '1',
                    'es_grupo' => 0,
                    'motivo' => null,
                    'servicio_contratado_id' => 7,
                    'socio_id' => 9,
            ]);
            ///mario - chinche
            DB::table('asignaciones')->insert([
                    'aceptado' => '1',
                    'es_grupo' => 0,
                    'motivo' => null,
                    'servicio_contratado_id' => 8,
                    'socio_id' => 1,
            ]);
            ///gabriel - chinche
            DB::table('asignaciones')->insert([
                    'aceptado' => '1',
                    'es_grupo' => 0,
                    'motivo' => null,
                    'servicio_contratado_id' => 9,
                    'socio_id' => 15,
            ]);
            ///historia de vida - rene
            DB::table('asignaciones')->insert([
                    'aceptado' => '1',
                    'es_grupo' => 0,
                    'motivo' => null,
                    'servicio_contratado_id' => 10,
                    'socio_id' => 4,
            ]);
            ///historia de vida - adriana
            DB::table('asignaciones')->insert([
                    'aceptado' => '1',
                    'es_grupo' => 0,
                    'motivo' => null,
                    'servicio_contratado_id' => 11,
                    'socio_id' => 5,
            ]);
            ///historia de vida - manuel
            DB::table('asignaciones')->insert([
                    'aceptado' => '1',
                    'es_grupo' => 0,
                    'motivo' => null,
                    'servicio_contratado_id' => 12,
                    'socio_id' => 3,
            ]);
            ///silla - jose
            DB::table('asignaciones')->insert([
                    'aceptado' => '1',
                    'es_grupo' => 0,
                    'motivo' => null,
                    'servicio_contratado_id' => 13,
                    'socio_id' => 10,
            ]);
            ///silla - alejandro
            DB::table('asignaciones')->insert([
                    'aceptado' => '1',
                    'es_grupo' => 0,
                    'motivo' => null,
                    'servicio_contratado_id' => 14,
                    'socio_id' => 11,
            ]);
            ///alquiler equipos - alejandro
            DB::table('asignaciones')->insert([
                    'aceptado' => '1',
                    'es_grupo' => 0,
                    'motivo' => null,
                    'servicio_contratado_id' => 15,
                    'socio_id' => 11,
            ]);
            ///animacion de eventos -Alejandro
            DB::table('asignaciones')->insert([
                    'aceptado' => '1',
                    'es_grupo' => 0,
                    'motivo' => null,
                    'servicio_contratado_id' => 16,
                    'socio_id' => 11,
            ]);
            ///animacion de eventos infantiles - gabriela
            DB::table('asignaciones')->insert([
                    'aceptado' => '1',
                    'es_grupo' => 0,
                    'motivo' => null,
                    'servicio_contratado_id' => 17,
                    'socio_id' => 3,
            ]);
            ////////grupos
            ///jazz --- show jazz
            DB::table('asignaciones')->insert([
                    'aceptado' => '1',
                    'es_grupo' => 1,
                    'motivo' => null,
                    'servicio_contratado_id' => 18,
                    'grupo_id' => 4,
            ]);
            //// historia de vida -- producciones
            DB::table('asignaciones')->insert([
                    'aceptado' => '1',
                    'es_grupo' => 1,
                    'motivo' => null,
                    'servicio_contratado_id' => 19,
                    'grupo_id' => 2,
            ]);
            ///asignaciones en estado sin finalizar
            /// gabriela - fotografia
            DB::table('asignaciones')->insert([
                    'aceptado' => '1',
                    'es_grupo' => 0,
                    'motivo' => null,
                    'servicio_contratado_id' => 20,
                    'socio_id' => 3,
            ]);
            /// vanesa -retrato
            DB::table('asignaciones')->insert([
                    'aceptado' => '1',
                    'es_grupo' => 0,
                    'motivo' => null,
                    'servicio_contratado_id' => 21,
                    'socio_id' => 9,
            ]);

    }
}
