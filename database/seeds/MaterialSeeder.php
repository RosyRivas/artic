<?php

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;

use App\Material;

class MaterialSeeder extends Seeder
{

    public function run()
    {
                DB::table('materiales')->insert([
                      'nombre' => 'Lienzo',
                      'descripcion' => 'De Algodon',
                      'cantidad' => '2',
                      'tipo_material_id'=> '3',
                    ]);

                DB::table('materiales')->insert([
                      'nombre' => 'Guitarra',
                      'descripcion' => 'Acustica sin amplificador',
                      'cantidad' => '5',
                      'tipo_material_id'=> '4',
                    ]);

                DB::table('materiales')->insert([
                      'nombre' => 'Pincel',
                      'descripcion' => 'Nro 2',
                      'cantidad' => '5',
                      'tipo_material_id'=> '3',
                    ]);

                DB::table('materiales')->insert([
                      'nombre' => 'Camara Fotografica',
                      'descripcion' => 'Sony lentes 50px',
                      'cantidad' => '1',
                      'tipo_material_id'=> '5',
                    ]);
                DB::table('materiales')->insert([
                      'nombre' => 'Acordeon',
                      'descripcion' => '-',
                      'cantidad' => '1',
                      'tipo_material_id'=> '4',
                    ]);
                DB::table('materiales')->insert([
                      'nombre' => 'Bateria',
                      'descripcion' => '-',
                      'cantidad' => '1',
                      'tipo_material_id'=> '4',
                    ]);
                DB::table('materiales')->insert([
                      'nombre' => 'Flauta',
                      'descripcion' => '-',
                      'cantidad' => '1',
                      'tipo_material_id'=> '4',
                    ]);
                DB::table('materiales')->insert([
                      'nombre' => 'Tripode',
                      'descripcion' => '-',
                      'cantidad' => '1',
                      'tipo_material_id'=> '5',
                    ]);

                DB::table('materiales')->insert([
                      'nombre' => 'Camara de Filmacion',
                      'descripcion' => '-',
                      'cantidad' => '1',
                      'tipo_material_id'=> '5',
                    ]);

                DB::table('materiales')->insert([
                      'nombre' => 'Espejo luminico',
                      'descripcion' => '-',
                      'cantidad' => '1',
                      'tipo_material_id'=> '5',
                    ]);

                DB::table('materiales')->insert([
                       'nombre' => 'Pincel Plano',
                       'descripcion' => 'Nro 2',
                       'cantidad' => '5',
                       'tipo_material_id'=> '3',
                     ]);
                DB::table('materiales')->insert([
                        'nombre' => 'Pincel fino',
                        'descripcion' => 'Nro 2',
                        'cantidad' => '5',
                        'tipo_material_id'=> '3',
                      ]);

                  DB::table('materiales')->insert([
                           'nombre' => 'Vestidos de Flamenco',
                           'descripcion' => 'Tonos rojo',
                           'cantidad' => '5',
                           'tipo_material_id'=> '2',
                     ]);
                  DB::table('materiales')->insert([
                            'nombre' => 'Vestidos de chamame',
                            'descripcion' => '-',
                            'cantidad' => '5',
                            'tipo_material_id'=> '2',
                      ]);

                  DB::table('materiales')->insert([
                          'nombre' => 'Trajes de jazz',
                          'descripcion' => '-',
                          'cantidad' => '5',
                          'tipo_material_id'=> '2',
                  ]);

                  DB::table('materiales')->insert([
                          'nombre' => 'Computadora portatil',
                          'descripcion' => '-',
                          'cantidad' => '2',
                          'tipo_material_id'=> '6',
                  ]);

                  DB::table('materiales')->insert([
                          'nombre' => 'Proyector',
                          'descripcion' => '-',
                          'cantidad' => '2',
                          'tipo_material_id'=> '1',
                  ]);
                  DB::table('materiales')->insert([
                          'nombre' => 'Microfono',
                          'descripcion' => '-',
                          'cantidad' => '2',
                          'tipo_material_id'=> '1',
                  ]);
                  DB::table('materiales')->insert([
                          'nombre' => 'Parlantes',
                          'descripcion' => '-',
                          'cantidad' => '2',
                          'tipo_material_id'=> '1',
                  ]);
                  DB::table('materiales')->insert([
                          'nombre' => 'Juego de luces',
                          'descripcion' => '-',
                          'cantidad' => '2',
                          'tipo_material_id'=> '3',
                  ]);
                  DB::table('materiales')->insert([
                          'nombre' => 'Sillas Plasticas',
                          'descripcion' => '-',
                          'cantidad' => '500',
                          'tipo_material_id'=> '7',
                  ]);
                  DB::table('materiales')->insert([
                          'nombre' => 'Sillas de madera',
                          'descripcion' => '-',
                          'cantidad' => '100',
                          'tipo_material_id'=> '7',
                  ]);
                  DB::table('materiales')->insert([
                          'nombre' => 'Trajes de jazz',
                          'descripcion' => '-',
                          'cantidad' => '10',
                          'tipo_material_id'=> '2',
                  ]);
                  DB::table('materiales')->insert([
                          'nombre' => 'Castillo inflable',
                          'descripcion' => '-',
                          'cantidad' => '2',
                          'tipo_material_id'=> '7',
                  ]);
    }
}
