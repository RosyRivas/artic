<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
      $this->call(RolSeeder::class);
      $this->call(UserSeeder::class);
      $this->call(JefaturaSeeder::class);
      $this->call(TipoMaterialSeeder::class);
      $this->call(MaterialSeeder::class);
      $this->call(ServicioSeeder::class);
      $this->call(SocioSeeder::class);
      $this->call(RecursoSeeder::class);
      $this->call(GrupoSeeder::class);
      $this->call(ServicioSocioSeeder::class);
      $this->call(GrupoServicioSeeder::class);
      $this->call(GrupoSocioSeeder::class);
      $this->call(PedidoSeeder::class);
      $this->call(ServicioContratadoSeeder::class);
      $this->call(AsignacionSeeder::class);
      $this->call(CalificacionSocioSeeder::class);
      $this->call(CalificacionGrupoSeeder::class);
      $this->call(CalificacionSeeder::class);

    }
}
