<?php

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;

use App\TipoMaterial;

class TipoMaterialSeeder extends Seeder
{
public function run()
    {
        DB::table('tipos_materiales')->insert([
            'nombre' => 'Sonido',
            'descripcion' =>'todos los equipos de sonido',
        ]);

        DB::table('tipos_materiales')->insert([
            'nombre' => 'Vestimenta',
            'descripcion' =>'-',
        ]);
        DB::table('tipos_materiales')->insert([
            'nombre' => 'Elementos Plasticos',
            'descripcion' =>'Elementos que son utilizados en para expreciones murales',
        ]);
        DB::table('tipos_materiales')->insert([
            'nombre' => 'Instrumento',
            'descripcion' =>'instrumentos musicales utilizadas por bandas',
        ]);
        DB::table('tipos_materiales')->insert([
            'nombre' => 'Equipo de cinematografia',
            'descripcion' =>'Equipos de cine',
        ]);

        DB::table('tipos_materiales')->insert([
            'nombre' => 'Equipo electronicos',
            'descripcion' =>'Elementos electronicos como computadora, proyector, etc.',
        ]);
        DB::table('tipos_materiales')->insert([
            'nombre' => 'Decoracion de eventos',
            'descripcion' =>'Elementos electronicos como computadora, proyector, etc.',
        ]);
    }
}
