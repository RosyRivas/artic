<?php

use Illuminate\Database\Seeder;

class CalificacionGrupoSeeder extends Seeder
{

    public function run()
    {
        //grupo - servicio
        //show jazz - jazz moderno
        DB::table('calificaciones_grupos')->insert([
                'grupo_id' => 4,
                'servicio_id' => 5,
                'calificacion' => 3,
        ]);
        //show jazz - jazz moderno
        DB::table('calificaciones_grupos')->insert([
                'grupo_id' => 4,
                'servicio_id' => 5,
                'calificacion' => 3,
        ]);
    }
}
