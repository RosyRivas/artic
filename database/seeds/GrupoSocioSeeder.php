<?php

use Illuminate\Database\Seeder;

class GrupoSocioSeeder extends Seeder
{
    public function run()
    {
      //////Creator producciones

      DB::table('grupo_socio')->insert([
            'grupo_id' => '2',
            'socio_id' => '1',
      ]);
      DB::table('grupo_socio')->insert([
            'grupo_id' => '2',
            'socio_id' => '7',
      ]);
      DB::table('grupo_socio')->insert([
            'grupo_id' => '2',
            'socio_id' => '11',
      ]);
      ////// show jazz
      DB::table('grupo_socio')->insert([
            'grupo_id' => '4',
            'socio_id' => '3',
      ]);
      DB::table('grupo_socio')->insert([
            'grupo_id' => '4',
            'socio_id' => '6',
      ]);
      DB::table('grupo_socio')->insert([
            'grupo_id' => '4',
            'socio_id' => '8',
      ]);
      DB::table('grupo_socio')->insert([
            'grupo_id' => '4',
            'socio_id' => '12',
      ]);



    }
}
