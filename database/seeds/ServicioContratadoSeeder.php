<?php

use Illuminate\Database\Seeder;

class ServicioContratadoSeeder extends Seeder
{
    public function run()
    {
            //servicios contratados en estado terminado
            // mural vanesa
            DB::table('servicios_contratados')->insert([
                    'fecha_prestacion' => '2021-01-4',
                    'terminado' => 1,
                    'pedido_id' => 1 ,
                    'servicio_id' => 1,
            ]);

            //gabriela foto
            DB::table('servicios_contratados')->insert([
                    'fecha_prestacion' => '2021-01-6',
                    'terminado' => 1,
                    'pedido_id' => 2 ,
                    'servicio_id' => 2,
            ]);
            //anicia foto
            DB::table('servicios_contratados')->insert([
                    'fecha_prestacion' => '2021-01-7',
                    'terminado' => 1,
                    'pedido_id' => 3 ,
                    'servicio_id' => 2,
            ]);
            //vanesa foto
            DB::table('servicios_contratados')->insert([
                    'fecha_prestacion' => '2021-01-7',
                    'terminado' => 1,
                    'pedido_id' => 4 ,
                    'servicio_id' => 2,
            ]);
            //mario retrato
            DB::table('servicios_contratados')->insert([
                    'fecha_prestacion' => '2021-01-7',
                    'terminado' => 1,
                    'pedido_id' => 5 ,
                    'servicio_id' => 3,
            ]);
            //manuel retrato
            DB::table('servicios_contratados')->insert([
                    'fecha_prestacion' => '2021-01-8',
                    'terminado' => 1,
                    'pedido_id' => 6 ,
                    'servicio_id' => 3,
            ]);
            //vanesa retrato
            DB::table('servicios_contratados')->insert([
                    'fecha_prestacion' => '2021-01-8',
                    'terminado' => 1,
                    'pedido_id' => 7 ,
                    'servicio_id' => 3,
            ]);
            //mario chinche
            DB::table('servicios_contratados')->insert([
                    'fecha_prestacion' => '2021-01-9',
                    'terminado' => 1,
                    'pedido_id' => 8 ,
                    'servicio_id' => 4,
            ]);
            //gabriel chinche
            DB::table('servicios_contratados')->insert([
                    'fecha_prestacion' => '2021-01-9',
                    'terminado' => 1,
                    'pedido_id' => 9 ,
                    'servicio_id' => 4,
            ]);
            //rene historia de vida
            DB::table('servicios_contratados')->insert([
                    'fecha_prestacion' => '2021-01-10',
                    'terminado' => 1,
                    'pedido_id' => 10 ,
                    'servicio_id' => 6,
            ]);
            //adriana historia de vida
            DB::table('servicios_contratados')->insert([
                    'fecha_prestacion' => '2021-01-13',
                    'terminado' => 1,
                    'pedido_id' => 11 ,
                    'servicio_id' => 6,
            ]);
            //manuel historia de vida
            DB::table('servicios_contratados')->insert([
                    'fecha_prestacion' => '2021-01-16',
                    'terminado' => 1,
                    'pedido_id' => 12 ,
                    'servicio_id' => 6,
            ]);
            //jose alquiler de sillas
            DB::table('servicios_contratados')->insert([
                    'fecha_prestacion' => '2021-01-20',
                    'terminado' => 1,
                    'pedido_id' => 13 ,
                    'servicio_id' => 7,
            ]);
            //alejandro alquiler de sillas
            DB::table('servicios_contratados')->insert([
                    'fecha_prestacion' => '2021-01-24',
                    'terminado' => 1,
                    'pedido_id' => 14 ,
                    'servicio_id' => 7,
            ]);
            //alejandro alquiler de  equipos
            DB::table('servicios_contratados')->insert([
                    'fecha_prestacion' => '2021-01-24',
                    'terminado' => 1,
                    'pedido_id' => 14 ,
                    'servicio_id' => 8,
            ]);

            //alejandro animacion de eventos
            DB::table('servicios_contratados')->insert([
                    'fecha_prestacion' => '2021-01-24',
                    'terminado' => 1,
                    'pedido_id' => 14 ,
                    'servicio_id' => 9,
            ]);
            //gabriela animacion de eventos infantiles
            DB::table('servicios_contratados')->insert([
                    'fecha_prestacion' => '2021-01-24',
                    'terminado' => 1,
                    'pedido_id' => 14 ,
                    'servicio_id' => 10,
            ]);
            ////grupos
            /// jazz
            DB::table('servicios_contratados')->insert([
                    'fecha_prestacion' => '2021-01-25',
                    'terminado' => 1,
                    'pedido_id' => 15 ,
                    'servicio_id' => 5,
            ]);
            /// creator producciones
            DB::table('servicios_contratados')->insert([
                    'fecha_prestacion' => '2021-01-28',
                    'terminado' => 1,
                    'pedido_id' => 16 ,
                    'servicio_id' => 6,
            ]);
            // servicio del pedido 1 de tatiana---- fotografia
            DB::table('servicios_contratados')->insert([
                    'fecha_prestacion' => '2021-01-16',
                    'terminado' => 1,
                    'pedido_id' => 17 ,
                    'servicio_id' => 2,
            ]);
            //servicio del pedido 2 de tatiana ---retrato
            DB::table('servicios_contratados')->insert([
                    'fecha_prestacion' => '2021-01-16',
                    'terminado' => 1,
                    'pedido_id' => 18 ,
                    'servicio_id' => 3,
            ]);

    }
}
