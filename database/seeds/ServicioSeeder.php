<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Servicio;

class ServicioSeeder extends Seeder
{

    public function run()
    {
            DB::table('servicios')->insert([
                      'nombre' => 'Murales',
                      'descripcion' => 'Realizacion de murales en todo tipo de lugar',
                      'duracion'=> 5,
                      'costo' => '1000',
                      'jefatura_id' =>'1',
                      'imagen'=>'20201009-125915.jpg'
                  ]);

            DB::table('servicios')->insert([
                      'nombre' => 'Fotografia de bodas',
                      'descripcion' => 'sesión por hora',
                      'servicio_dia'=> 3,
                      'costo' => '2000',
                      'jefatura_id' =>'6',
                      'imagen'=>'20201007-201506.jpg'
                  ]);
            DB::table('servicios')->insert([
                      'nombre' => 'Retrato',
                      'descripcion' => 'Se realiza pintura en cuadros de 80x80',
                      'duracion' => 2,
                      'costo' => '2000',
                      'jefatura_id' =>'1',
                      'imagen'=>'20201013-163904.jpg'
                  ]);

            DB::table('servicios')->insert([
                      'nombre' => 'Chinche, una historia de perro.',
                      'descripcion' => 'Relato en primera persona',
                      'servicio_dia' => 3,
                      'costo' => '2000',
                      'jefatura_id' =>'5',
                      'imagen'=>'20201013-164050.jpg'
                  ]);

            DB::table('servicios')->insert([
                      'nombre' => 'Jazz Moderno',
                      'descripcion' => '-',
                      'servicio_dia' => 2,
                      'costo' => '1500',
                      'jefatura_id' =>'4',
                      'imagen'=>'20201009-130231.jpg'
                  ]);

            DB::table('servicios')->insert([
                      'nombre' => 'Historia de vida',
                      'descripcion' => 'video de 10 a 15 minutos',
                      'duracion' => 5,
                      'costo' => '1500',
                      'jefatura_id' =>'2',
                      'imagen'=>'20201013-163930.jpg'
                  ]);
            DB::table('servicios')->insert([
                      'nombre' => 'Alquiler de sillas',
                      'descripcion' => 'Se incluye el flete del mismo',
                      'servicio_dia' => 2,
                      'costo' => '3500',
                      'jefatura_id' =>'1',
                      'imagen'=>'20201014-185343.jpg'
                  ]);
            DB::table('servicios')->insert([
                      'nombre' => 'Alquiler de equipos de sonidos',
                      'descripcion' => 'Incluye equipo de luces',
                      'servicio_dia' => 3,
                      'costo' => '5000',
                      'jefatura_id' =>'3',
                      'imagen'=>'20201013-164255.jpg'
                  ]);

            DB::table('servicios')->insert([
                      'nombre' => 'Animacion de eventos',
                      'descripcion' => ' ',
                      'servicio_dia' => 1,
                      'costo' => '2000',
                      'jefatura_id' =>'3',
                      'imagen'=>'20201013-195046.jpg'
                  ]);
            DB::table('servicios')->insert([
                      'nombre' => 'Animacion de eventos infantiles',
                      'descripcion' => ' ',
                      'servicio_dia' => 2,
                      'costo' => '1000',
                      'jefatura_id' =>'5',
                      'imagen'=>'20201013-195739.jpg'
                  ]);


    }
}
