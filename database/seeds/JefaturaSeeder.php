<?php

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;

use App\Jefatura;

class JefaturaSeeder extends Seeder
{

    public function run()
    {

      DB::table('jefaturas')->insert([
              'nombre' => 'Artes Visuales',
          ]);

      DB::table ('jefaturas')->insert([
          'nombre'  => 'Audiovisual ',
          ]);

      DB::table ('jefaturas')->insert([
          'nombre'  => 'Musica',
          ]);

      DB::table ('jefaturas')->insert([
          'nombre'  => 'Danza',
          ]);

      DB::table ('jefaturas')->insert([
          'nombre'  => 'Teatro ',
          ]);

      DB::table ('jefaturas')->insert([
          'nombre'  => 'Fotografia ',
          ]);
    }
}
