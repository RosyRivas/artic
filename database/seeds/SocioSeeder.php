<?php

use Illuminate\Database\Seeder;

class SocioSeeder extends Seeder
{

    public function run()
    {
      //socio 1
      DB::table('socios')->insert([
                'nombres'=>'Mario',
                'apellido'=>'Rivas',
                'nombre_artistico' => 'Marito',
                'user_id' =>'8',
                'jefatura_id'=>'2',
            ]);
        //socio 2
       DB::table('socios')->insert([
                'nombres'=>'Manuel',
                'apellido'=>'Rivas Rodriguez',
                'nombre_artistico' => '',
                'user_id' =>'9',
                'jefatura_id'=>'3',
        ]);
          //socio 3
        DB::table('socios')->insert([
                  'nombres'=>'Gabriela',
                  'apellido'=>'Rivas Rodriguez',
                  'nombre_artistico' => '',
                  'user_id' =>'10',
                  'jefatura_id'=>'4',
        ]);
          //socio 4
        DB::table('socios')->insert([
                  'nombres'=>'Rene',
                  'apellido'=>'Rivas Rodriguez',
                  'nombre_artistico' => '',
                  'user_id' =>'11',
                  'jefatura_id'=>'1',
        ]);
          //socio 5
        DB::table('socios')->insert([
                  'nombres'=>'Adriana',
                  'apellido'=>'Rivas',
                  'nombre_artistico' => '',
                  'user_id' =>'12',
                  'jefatura_id'=>'3',
        ]);
          //socio 6
        DB::table('socios')->insert([
                  'nombres'=>'Mirta',
                  'apellido'=>'Rodriguez',
                  'nombre_artistico' => '',
                  'user_id' =>'13',
                  'jefatura_id'=>'4',
        ]);
          //socio 7
        DB::table('socios')->insert([
                  'nombres'=>'Domingo',
                  'apellido'=>'Rivas',
                  'nombre_artistico' => '',
                  'user_id' =>'14',
                  'jefatura_id'=>'2',
        ]);
          //socio 8
        DB::table('socios')->insert([
                  'nombres'=>'Anicia',
                  'apellido'=>'Rivas',
                  'nombre_artistico' => '',
                  'user_id' =>'15',
                  'jefatura_id'=>'4',
        ]);
          //socio 9
        DB::table('socios')->insert([
                  'nombres'=>'Vanesa',
                  'apellido'=>'Pinto',
                  'nombre_artistico' => '',
                  'user_id' =>'16',
                  'jefatura_id'=>'2',
        ]);
          //socio 10
        DB::table('socios')->insert([
                  'nombres'=>'Jose',
                  'apellido'=>'Rios',
                  'nombre_artistico' => '',
                  'user_id' =>'17',
                  'jefatura_id'=>'5',
        ]);
          //socio 11
        DB::table('socios')->insert([
                  'nombres'=>'Alejandro',
                  'apellido'=>'Andrusizyn',
                  'nombre_artistico' => '',
                  'user_id' =>'18',
                  'jefatura_id'=>'2',
        ]);
          //socio 12
        DB::table('socios')->insert([
                  'nombres'=>'Malena',
                  'apellido'=>'Pintos',
                  'nombre_artistico' => '',
                  'user_id' =>'19',
                  'jefatura_id'=>'4',
        ]);
          //socio 13
        DB::table('socios')->insert([
                  'nombres'=>'Giordina',
                  'apellido'=>'Cabaña',
                  'nombre_artistico' => '',
                  'user_id' =>'20',
                  'jefatura_id'=>'5',
        ]);
          //socio 14
        DB::table('socios')->insert([
                  'nombres'=>'Mauricio',
                  'apellido'=>'Perez',
                  'nombre_artistico' => '',
                  'user_id' =>21,
                  'jefatura_id'=>'1',
        ]);
          //socio 15
        DB::table('socios')->insert([
                  'nombres'=>'Gabriel',
                  'apellido'=>'Pezuk',
                  'nombre_artistico' => 'Steve',
                  'user_id' =>22,
                  'jefatura_id'=>5,
        ]);


    }
}
