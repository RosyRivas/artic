<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolSeeder extends Seeder
{
  
    public function run()
    {
            DB::table('roles')->insert([
                'nombre' => 'Administrador',
            ]);

            DB::table('roles')->insert([
                'nombre' => 'Auditor',
            ]);


            DB::table('roles')->insert([
                'nombre' => 'Socio',
            ]);

            DB::table('roles')->insert([
                'nombre' => 'Usuario',
            ]);
            DB::table('roles')->insert([// rol que se ingresar abm parametrizables
                'nombre' => 'SysAdmin',
            ]);
    }
}
