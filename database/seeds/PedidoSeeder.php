<?php

use Illuminate\Database\Seeder;

class PedidoSeeder extends Seeder
{

    public function run()
    {

        //mural vanesa 1
        DB::table('pedidos')->insert([
                'costo' => '1000',
                'senia' =>'500',
                'user_id' => 23,
        ]);
        //foto gabi 2
        DB::table('pedidos')->insert([
                'costo' => '2000',
                'senia' =>'1000',
                'user_id' => 23,
        ]);
        //foto anicia 3
        DB::table('pedidos')->insert([
                'costo' => '2000',
                'senia' =>'1000',
                'user_id' => 23,
        ]);
        //foto vanesa 4
        DB::table('pedidos')->insert([
                'costo' => '2000',
                'senia' =>'1000',
                'user_id' => 24,
        ]);
        //retrato mario 5
        DB::table('pedidos')->insert([
                'costo' => '1000',
                'senia' =>'500',
                'user_id' => 24,
        ]);
        //retrato manuel 6
        DB::table('pedidos')->insert([
                'costo' => '1000',
                'senia' =>'500',
                'user_id' => 4,
        ]);
        //retrato vanesa 7
        DB::table('pedidos')->insert([
                'costo' => '1000',
                'senia' =>'500',
                'user_id' => 24,
        ]);
        //chinche mario 8
        DB::table('pedidos')->insert([
                'costo' => '2000',
                'senia' =>'1000',
                'user_id' => 24,
        ]);
        //chinche gabriel 9
        DB::table('pedidos')->insert([
                'costo' => '2000',
                'senia' =>'1000',
                'user_id' => 23,
        ]);
        //historia vida rene 10
        DB::table('pedidos')->insert([
                'costo' => '1500',
                'senia' =>'750',
                'user_id' => 4,
        ]);
        //historia vida adriana 11
        DB::table('pedidos')->insert([
                'costo' => '1500',
                'senia' =>'750',
                'user_id' => 23,
        ]);
        //historia vida manuel 12
        DB::table('pedidos')->insert([
                'costo' => '1500',
                'senia' =>'750',
                'user_id' => 24,
        ]);
        //silla jose 13
        DB::table('pedidos')->insert([
                'costo' => '3500',
                'senia' =>'1750',
                'user_id' => 4,
        ]);
        //silla-equipo y animacion de evento-gabriela alejandro 14
        DB::table('pedidos')->insert([
                'costo' => '10500',
                'senia' =>'5250',
                'user_id' => 23,
        ]);
        ///grupos
        //jazz 15
        DB::table('pedidos')->insert([
                'costo' => '1500',
                'senia' =>'750',
                'user_id' => 24,
        ]);
        //historia vida - creator produccion 16
        DB::table('pedidos')->insert([
                'costo' => '1500',
                'senia' =>'750',
                'user_id' => 23,
        ]);
        // pedido de tatiana
        //pedido 17
        DB::table('pedidos')->insert([
                'costo' => '2000',
                'senia' =>'1000',
                'user_id' => 4,
        ]);
        //pedido 18
        DB::table('pedidos')->insert([
                'costo' => '2000',
                'senia' =>'1000' ,
                'user_id' => 4,
        ]);
    }
}
