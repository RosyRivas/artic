<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Recurso;
class RecursoSeeder extends Seeder
{

    public function run()
    {
          /////////
          // Recursos para  el servicio de murales
          /////////
          //
              DB::table('recursos')->insert([
                        'cantidad' => '1',
                        'servicio_id' => '1',
                        'material_id'=> '3',
              ]);
              //

          /////////
          // Recursos para  el servicio de fotografia
          /////////
          //camara
              DB::table('recursos')->insert([
                      'cantidad' => '1',
                      'servicio_id' => '2',
                      'material_id'=> '4',
              ]);
              //tripode
              DB::table('recursos')->insert([
                      'cantidad' => '1',
                      'servicio_id' => '2',
                      'material_id'=> '8',
              ]);

          /////////
          // Recursos para  el servicio de retrato
          /////////
            //pincel
              DB::table('recursos')->insert([
                      'cantidad' => '1',
                      'servicio_id' => '3',
                      'material_id'=> '3',
              ]);
              //lienzo
              DB::table('recursos')->insert([
                       'cantidad' => '1',
                       'servicio_id' => '3',
                       'material_id'=> '1',
              ]);

          /////////
          // Recursos para jazz moderno
          /////////

              DB::table('recursos')->insert([
                      'cantidad' => '6',
                      'servicio_id' => '5',
                      'material_id'=> '23',
              ]);

          /////////
          // Recursos para historia de vida
          /////////
          //computadora
              DB::table('recursos')->insert([
                      'cantidad' => '1',
                      'servicio_id' => '6',
                      'material_id'=> '16',
              ]);
          /////////
          // Recursos para alquiler sillas
          /////////
          //sillas
              DB::table('recursos')->insert([
                      'cantidad' => '500',
                      'servicio_id' => '7',
                      'material_id'=> '21',
              ]);
              DB::table('recursos')->insert([
                      'cantidad' => '100',
                      'servicio_id' => '7',
                      'material_id'=> '22',
              ]);
              // Recursos para animacion de eventos
              /////////
              //

              DB::table('recursos')->insert([
                      'cantidad' => '1',
                      'servicio_id' => '9',
                      'material_id'=> '16',
              ]);
              DB::table('recursos')->insert([
                      'cantidad' => '1',
                      'servicio_id' => '9',
                      'material_id'=> '17',
              ]);
              DB::table('recursos')->insert([
                      'cantidad' => '1',
                      'servicio_id' => '9',
                      'material_id'=> '18',
              ]);
              DB::table('recursos')->insert([
                      'cantidad' => '1',
                      'servicio_id' => '9',
                      'material_id'=> 19,
              ]);
              DB::table('recursos')->insert([
                      'cantidad' => '1',
                      'servicio_id' => '9',
                      'material_id'=> 21,
              ]);
              DB::table('recursos')->insert([
                      'cantidad' => '1',
                      'servicio_id' => '9',
                      'material_id'=> 22,
              ]);
      // Recursos para animacion de eventos infantiles
      /////////
      //    castillo inflable
              DB::table('recursos')->insert([
                      'cantidad' => '1',
                      'servicio_id' => '9',
                      'material_id'=> 24,
              ]);

///////////////////////////////////////////////
// Recursos para  los socios                 //
///////////////////////////////////////////////

      //// Mario Rivas
              DB::table('recursos')->insert([
                      'cantidad' => '5',
                      'socio_id' => '1',
                      'material_id'=> '1',
              ]);

              DB::table('recursos')->insert([
                      'cantidad' => '5',
                      'socio_id' => '1',
                      'material_id'=> '3',
              ]);

              DB::table('recursos')->insert([
                      'cantidad' => '1',
                      'socio_id' => '1',
                      'material_id'=> '4',
              ]);

              DB::table('recursos')->insert([
                      'cantidad' => '1',
                      'socio_id' => '1',
                      'material_id'=> '5',
              ]);

              DB::table('recursos')->insert([
                      'cantidad' => '1',
                      'socio_id' => '1',
                      'material_id'=> '10',
              ]);

              DB::table('recursos')->insert([
                      'cantidad' => '1',
                      'socio_id' => '1',
                      'material_id'=> '19',
              ]);
        ////  Manuel Rivas
              DB::table('recursos')->insert([
                      'cantidad' => '1',
                      'socio_id' => '2',
                      'material_id'=> '10',
              ]);

              DB::table('recursos')->insert([
                      'cantidad' => '1',
                      'socio_id' => '2',
                      'material_id'=> '5',
              ]);

              DB::table('recursos')->insert([
                      'cantidad' => '1',
                      'socio_id' => '2',
                      'material_id'=> '19',
              ]);

          /// Gabriela
              DB::table('recursos')->insert([
                      'cantidad' => '2',
                      'socio_id' => '3',
                      'material_id'=> '16',
              ]);
          //// Adriana
              DB::table('recursos')->insert([
                      'cantidad' => '2',
                      'socio_id' => '5',
                      'material_id'=> '16',
              ]);

              DB::table('recursos')->insert([
                      'cantidad' => '2',
                      'socio_id' => '5',
                      'material_id'=> '16',
              ]);

          //// Alejandro Andrusizyn
              DB::table('recursos')->insert([
                      'cantidad' => '2',
                      'socio_id' => '11',
                      'material_id'=> '19',
              ]);

              DB::table('recursos')->insert([
                      'cantidad' => '2',
                      'socio_id' => '11',
                      'material_id'=> '5',
              ]);


    /////////
    // Recursos de alquiler de sonido
    /////////
                DB::table('recursos')->insert([
                        'cantidad' => '1',
                        'servicio_id' => '8',
                        'material_id'=> '16',
                ]);
                DB::table('recursos')->insert([
                        'cantidad' => '1',
                        'servicio_id' => '8',
                        'material_id'=> '17',
                ]);
                DB::table('recursos')->insert([
                        'cantidad' => '1',
                        'servicio_id' => '8',
                        'material_id'=> '18',
                ]);
                DB::table('recursos')->insert([
                        'cantidad' => '1',
                        'servicio_id' => '8',
                        'material_id'=> 19,
                ]);
                DB::table('recursos')->insert([
                        'cantidad' => '1',
                        'servicio_id' => '8',
                        'material_id'=> 21,
                ]);
                DB::table('recursos')->insert([
                        'cantidad' => '1',
                        'servicio_id' => '8',
                        'material_id'=> 22,
                ]);

                /////////
                // Recursos de animacion de eventos
                /////////
                            DB::table('recursos')->insert([
                                    'cantidad' => '1',
                                    'servicio_id' => '9',
                                    'material_id'=> '16',
                            ]);
                            DB::table('recursos')->insert([
                                    'cantidad' => '1',
                                    'servicio_id' => '9',
                                    'material_id'=> '17',
                            ]);
                            DB::table('recursos')->insert([
                                    'cantidad' => '1',
                                    'servicio_id' => '9',
                                    'material_id'=> '18',
                            ]);
                            DB::table('recursos')->insert([
                                    'cantidad' => '1',
                                    'servicio_id' => '9',
                                    'material_id'=> 19,
                            ]);
                            DB::table('recursos')->insert([
                                    'cantidad' => '1',
                                    'servicio_id' => '9',
                                    'material_id'=> 21,
                            ]);
                            DB::table('recursos')->insert([
                                    'cantidad' => '1',
                                    'servicio_id' => '9',
                                    'material_id'=> 22,
                            ]);
                /////////
                // Recursos de castillo inflable
                /////////
                DB::table('recursos')->insert([
                        'cantidad' => '2',
                        'servicio_id' => '10',
                        'material_id'=> '24',
                ]);


    }
}
