<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJefaturasTable extends Migration
{

    public function up()
    {
        Schema::create('jefaturas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre');
            $table->double('porcentaje_senia')->default(50);

        });
    }

    public function down()
    {
        Schema::dropIfExists('jefaturas');
    }
}
