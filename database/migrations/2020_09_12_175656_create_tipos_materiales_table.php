<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTiposMaterialesTable extends Migration
{
  
    public function up()
    {
        Schema::create('tipos_materiales', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre');
            $table->string('descripcion')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('tipos_materiales');
    }
}
