<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiciosContratadosTable extends Migration
{

    public function up()
    {
        Schema::create('servicios_contratados', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('fecha_prestacion')->nullable();
            $table->boolean('terminado')->default(false)->nullable();
            $table->unsignedBigInteger('pedido_id');
            $table->unsignedBigInteger('servicio_id');
        });
    }

    public function down()
    {
        Schema::dropIfExists('servicios_contratados');
    }
}
