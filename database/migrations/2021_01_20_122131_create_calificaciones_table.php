<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCalificacionesTable extends Migration
{

    public function up()
    {
        Schema::create('calificaciones', function (Blueprint $table) {
          $table->bigIncrements('id');
          $table->date('fecha');
          $table->integer('puntaje')->nullable();
          $table->unsignedBigInteger('servicio_id');
          $table->unsignedBigInteger('asignacion_id');
          $table->unsignedBigInteger('grupo_id')->nullable();
          $table->unsignedBigInteger('socio_id')->nullable();

        });
    }
    public function down()
    {
        Schema::dropIfExists('calificaciones');
    }
}
