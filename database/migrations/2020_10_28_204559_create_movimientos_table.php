<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMovimientosTable extends Migration
{
    public function up()
    {
        Schema::create('movimientos', function (Blueprint $table) {
          $table->bigIncrements('id');
          $table->date('fecha');
          $table->string('numero');
          $table->string('tipo');// breve descripcion de que se trate el concepto
          $table->string('importe');
          $table->string('saldo');// a modo de historico como homebanking
          $table->unsignedBigInteger('user_id');
        });
    }

    public function down()
    {
        Schema::dropIfExists('movimientos');
    }
}
