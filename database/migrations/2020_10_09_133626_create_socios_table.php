<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSociosTable extends Migration
{
    public function up()
    {
        Schema::create('socios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombres');
            $table->string('apellido');
            $table->string('nombre_artistico')->nullable();
            $table->boolean('esta_activo')->default(true);
            $table->unsignedBigInteger('user_id')->nullable();
            $table->unsignedBigInteger('jefatura_id')->required();
        });
    }

    public function down()
    {
        Schema::dropIfExists('socios');
    }
}
