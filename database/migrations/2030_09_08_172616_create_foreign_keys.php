<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateForeignKeys extends Migration
{

    public function up()
    {

          Schema::table('users', function (Blueprint $table) {
              $table->foreign('rol_id')->references('id')->on('roles');

          });
          Schema::table('materiales', function (Blueprint $table) {
              $table->foreign('tipo_material_id')->references('id')->on('tipos_materiales');

          });
          Schema::table('recursos', function (Blueprint $table) {
            $table->foreign('material_id')->references('id')->on('materiales');
            $table->foreign('servicio_id')->references('id')->on('servicios');
            $table->foreign('socio_id')->references('id')->on('socios');

          });
          Schema::table('servicios', function (Blueprint $table) {
            $table->foreign('jefatura_id')->references('id')->on('jefaturas');

          });

          Schema::table('socios', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('jefatura_id')->references('id')->on('jefaturas');
          });


          Schema::table('grupo_socio', function (Blueprint $table) {
            $table->foreign('socio_id')->references('id')->on('socios');
            $table->foreign('grupo_id')->references('id')->on('grupos');
          });

          Schema::table('servicio_socio', function (Blueprint $table) {
            $table->foreign('socio_id')->references('id')->on('socios');
            $table->foreign('servicio_id')->references('id')->on('servicios');
          });

          Schema::table('grupos', function (Blueprint $table) {
            $table->foreign('jefe_id')->references('id')->on('socios');
          });

          Schema::table('grupo_servicio', function (Blueprint $table) {
            $table->foreign('grupo_id')->references('id')->on('grupos');
            $table->foreign('servicio_id')->references('id')->on('servicios');
          });

          Schema::table('items', function (Blueprint $table) {
            $table->foreign('compra_id')->references('id')->on('compras');
            $table->foreign('servicio_id')->references('id')->on('servicios');
          });

          Schema::table('movimientos', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
          });

          Schema::table('pedidos', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
          });

          Schema::table('servicios_contratados', function (Blueprint $table) {
            $table->foreign('pedido_id')->references('id')->on('pedidos');
            $table->foreign('servicio_id')->references('id')->on('servicios');
          });

          Schema::table('asignaciones', function (Blueprint $table) {
            $table->foreign('servicio_contratado_id')->references('id')->on('servicios_contratados');
            $table->foreign('socio_id')->references('id')->on('socios');
            $table->foreign('grupo_id')->references('id')->on('grupos');
          });

          Schema::table('recursos_prestados', function (Blueprint $table) {
            $table->foreign('responsable_id')->references('id')->on('socios');
            $table->foreign('material_id')->references('id')->on('materiales');
            $table->foreign('servicio_id')->references('id')->on('servicios');
          });

          Schema::table('calificaciones', function (Blueprint $table) {
            $table->foreign('socio_id')->references('id')->on('socios');
            $table->foreign('grupo_id')->references('id')->on('grupos');
            $table->foreign('servicio_id')->references('id')->on('servicios');
            $table->foreign('asignacion_id')->references('id')->on('asignaciones');
          });

          Schema::table('calificaciones_socios', function (Blueprint $table) {
            $table->foreign('socio_id')->references('id')->on('socios');
            $table->foreign('servicio_id')->references('id')->on('servicios');
            
          });

          Schema::table('calificaciones_grupos', function (Blueprint $table) {
            $table->foreign('grupo_id')->references('id')->on('grupos');
            $table->foreign('servicio_id')->references('id')->on('servicios');

          });
    }


    public function down()
    {

    }
}
