<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecursosPrestadosTable extends Migration
{

    public function up()
    {
        Schema::create('recursos_prestados', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('fecha_prestamo');
            $table->integer('cantidad');
            $table->unsignedBigInteger('material_id');
            $table->unsignedBigInteger('servicio_id');
            $table->unsignedBigInteger('responsable_id');
            $table->string('estado_devolucion')->nullable();
            $table->boolean('esta_devuelto')->default(false);
            $table->date('fecha_devolucion')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('recursos_prestados');
    }
}
