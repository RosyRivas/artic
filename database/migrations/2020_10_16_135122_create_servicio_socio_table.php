<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServicioSocioTable extends Migration
{
  /* TABLA INTERMEDIA ENTRE SOCIO Y Servicio */
    public function up()
    {
        Schema::create('servicio_socio', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('socio_id');
            $table->unsignedBigInteger('servicio_id');
        });
    }


    public function down()
    {
        Schema::dropIfExists('servicio_socio');
    }
}
