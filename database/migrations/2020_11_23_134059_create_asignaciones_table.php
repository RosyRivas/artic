<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAsignacionesTable extends Migration
{
    public function up()
    {
        Schema::create('asignaciones', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->boolean('aceptado')->nullable();
            $table->boolean('es_grupo');
            //campo descriptivo del motivo por el cual se rechaza la asignacion
            $table->string('motivo')->nullable();
            $table->unsignedBigInteger('servicio_contratado_id');
            $table->unsignedBigInteger('socio_id')->nullable();
            $table->unsignedBigInteger('grupo_id')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('asignaciones');
    }
}
