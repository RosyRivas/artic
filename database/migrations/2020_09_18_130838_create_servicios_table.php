<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiciosTable extends Migration
{

    public function up()
    {
        Schema::create('servicios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre');
            $table->string('descripcion')->nullable();
            // esta tabla para la cantidad de dias estimativos para realizar el servicio
            $table->integer('duracion')->nullable();
            // campo para la cantidad de servicios que se pueden hacer por dias
            $table->integer('servicio_dia')->nullable();

            $table->double('costo');
            $table->boolean('esta_activo')->default(true);
            $table->unsignedBigInteger('jefatura_id'); //claves foraneas siempre este formato
            $table->string('imagen');
        });
    }


    public function down()
    {
        Schema::dropIfExists('servicios');
    }
}
