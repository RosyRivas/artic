<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGrupoSocioTable extends Migration
{
  /*TABLA INTERMEDIA ENTRE SOCIO Y grupo
   */
    public function up()
    {
        Schema::create('grupo_socio', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('socio_id');
            $table->unsignedBigInteger('grupo_id');
        });
    }

    public function down()
    {
        Schema::dropIfExists('grupo_socio');
    }
}
