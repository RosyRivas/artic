<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePedidosTable extends Migration
{

    public function up()
    {
        Schema::create('pedidos', function (Blueprint $table) {
          $table->bigIncrements('id');
          $table->boolean('esta_pagado')->default(false)->nullable();
          $table->double('costo');
          $table->double('senia');
          $table->unsignedBigInteger('user_id');
        });
    }


    public function down()
    {
        Schema::dropIfExists('pedidos');
    }
}
