<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecursosTable extends Migration
{

    public function up()
    {
      Schema::create('recursos', function (Blueprint $table) {
          $table->bigIncrements('id');
          $table->string('cantidad');
          $table->unsignedBigInteger('servicio_id')->nullable();
          $table->unsignedBigInteger('socio_id')->nullable();
          $table->unsignedBigInteger('material_id');
      });
    }

    public function down()
    {
          Schema::dropIfExists('recursos');
    }
}
