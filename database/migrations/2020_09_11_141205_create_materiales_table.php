<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMaterialesTable extends Migration
{

    public function up()
    {
        Schema::create('materiales', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre');
            $table->string('descripcion')->nullable();
            $table->integer('cantidad');
            $table->unsignedBigInteger('tipo_material_id')->nullable; //claves foraneas siempre este formato

        });
    }

    public function down()
    {
        Schema::dropIfExists('materiales');
    }
}
