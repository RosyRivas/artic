<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CalificacionesSociosTable extends Migration
{

    public function up()
    {
        Schema::create('calificaciones_socios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('socio_id');
            $table->unsignedBigInteger('servicio_id');
            $table->unsignedBigInteger('calificacion');
        });
    }
    public function down(){
        Schema::dropIfExists('calificaciones_socios');
    }
}
