<?php

namespace App;

class Item extends Auditable
{
  public $timestamps = false;
  protected $fillable = ['costo','fecha','compra_id','servicio_id'];

  public function servicio(){
      return $this->belongsTo('App\Servicio');
  }

  public function fecha(){
    return date("d/m/Y",strtotime($this->fecha));
  }
}
