<?php

namespace App;


class Movimiento extends Auditable
{
  public $timestamps = false;

  protected $fillable = ['numero','fecha', 'tipo', 'importe', 'saldo','user_id'];


}
