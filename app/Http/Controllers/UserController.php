<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Requests\UserRequest;
use App\Http\Requests\RolRequest;
use App\Rol;


class UserController extends Controller
{

    public function index(){
          $users= User::orderBy('id','desc')->get();

          return view('datos.users.index', compact('users') );
      }

    public function crear(){
        $roles = Rol::all();
        return view('datos.users.crear', compact('roles'));

      }

    public function almacenar(UserRequest $request){
        $request->validate([
          'password' => 'required',
        ],[
          'required' => 'Campo obligatorio'
        ]);

        $request->merge([
          'password' => bcrypt($request->password),
        ]);
        User::create($request->all());
        return redirect()->route('users')->with(['message'=>'Usuario creado exitosamento']);
    }

    public function editar(User $user){

        $roles = Rol::all();
        return view('datos.users.editar', compact('user','roles'));
    }


    public function actualizar(UserRequest $request, User $user){
        if (is_null($request->password)) {
          $request->merge([
            'password' =>$user->password,
          ]);
        }
        else{
          $request->merge([
            'password' => bcrypt($request->password),
          ]);
        }
        $user->update($request->all());
        return redirect()->route('users')->with('message', 'Registro modificado exitosamente.');
    }

    public function eliminar(User $user){

        $referencias=0;
        if($referencias>0){
            return redirect()->route('users')
                            ->withErrors('No se puede eliminar el registro porque existen datos asociados.');
        }

        $user->delete();/* borra el registro */
        return redirect()->route('users')->with('message', 'Registro eliminado exitosamente.');
    }
}
