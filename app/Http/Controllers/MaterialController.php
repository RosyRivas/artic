<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\MaterialRequest;
use App\Http\Requests\TipoMaterialRequest;
use App\Material;
use App\TipoMaterial;

class MaterialController extends Controller
{

        public function index(){
            $materiales = Material::with('tipoMaterial')->orderBy('id', 'desc')->get();
            //dd($materiales);
            return view('datos.materiales.index', compact('materiales') );
        }

        public function crear(){
            $tipoMaterial= TipoMaterial::all();
            return view('datos.materiales.crear', compact('tipoMaterial') );
        }

        public function almacenar(MaterialRequest $request){
            Material::create($request->all());
            return redirect()->route('materiales')->with(['message'=>'Material creado exitosamente']);
        }


        public function editar(Material $material){
            //dd($material);   para ver el contenido de la variable
            $tipoMaterial= TipoMaterial::all();
            return view('datos.materiales.editar', compact('material', 'tipoMaterial'));
        }

        public function actualizar(MaterialRequest $request, Material $material){
            $material->update($request->all());
            return redirect()->route('materiales')->with('message', 'Registro modificado exitosamente.');
        }

        public function eliminar(Material $material){
            //$referencias = Material::whereJefatura_id($material->id)->count(); /* cuenta las referencias en la tabla relacionada */
            $referencias=0;
            if($referencias>0){
                return redirect()->route('materiales')
                                ->withErrors('No se puede eliminar el registro porque existen datos asociados.');
            }

            $material->delete();/* borra el registro */
            return redirect()->route('materiales')->with('message', 'Registro eliminado exitosamente.');
        }



}
