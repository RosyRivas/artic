<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use App\Socio;
use App\Http\Requests\SocioRequest;
use App\User;
use App\Jefatura;
use App\Http\Requests\JefaturaRequest;
use App\Rol;
use App\Recurso;
use App\Material;
use App\Http\Requests\RecursoRequest;
use App\Http\Requests\MaterialRequest;


class SocioController extends Controller
{
      public function index(){
        $sql = Socio::with(['recursos' , 'recursos.material'])->orderBy('id','desc');
    //    $jefaturas = Jefatura::all();

        $socios = $sql->get();
        return view('datos.socios.index', compact('socios') );

      }

      public function crear(){
        $jefaturas = Jefatura::all();
        $materiales = Material::all();
        return view('datos.socios.crear', compact('jefaturas','materiales') );

      }

      public function almacenar(SocioRequest $request){
          $name=$request->nombres.$request->apellido;
          //  siempre rol de socio
          $request->merge(['name'=>$name,'rol_id'=>3]);
          $s  = Socio::create($request->all());
          $request->merge([
            'password' => bcrypt($request->password),
          ]);
          $u  = User::create($request->all());

          $s->update([
              'user_id'     =>  $u  ->  id,
          ]);
          // control para agregar recursos a mi servicio
          if (!empty($request->cantidades)){
            foreach ($request->cantidades as $i => $cant) {
              Recurso::create([
                    'cantidad'    =>  $cant,
                    'material_id' =>  $request->materiales[$i],
                    'socio_id' =>  $s->id,
              ]);
            }
          }

          return redirect()->route('socios')->with(['message'=>'Socio creado exitosamente']);

      }
      public function editar(Socio $socio){
        $jefaturas = Jefatura::all();
        $materiales = Material::all();
        return view('datos.socios.editar', compact('jefaturas', 'socio','materiales' ));

      }

      public function actualizar(SocioRequest $request, Socio $socio){
        $socio->update($request->all());

        //  comparacion de materiales
        if (count($socio->recursos) > 0){
            foreach ($socio->recursos as $r) {
                if(!is_array($request->materiales) || !in_array($r->material_id, $request->materiales))
                      $r->delete();
            }
        }
        //array de materiales
        if (is_array($request->materiales)){
          //dd('pepa');
            foreach ($request->materiales as $index => $m) {
                Recurso::updateOrCreate([
                    'material_id' => $m,
                    'socio_id' => $socio->id,
                ],[
                    'cantidad'    => $request->cantidades[$index],
                ]);
            }
        }
        return redirect()->route('socios')->with('message', 'Registro modificado exitosamente.');

      }

      public function eliminar(Socio $socio){
        // cuenta ;as referencias duras
        //dd($socio->grupos->count());
        $referencias=$socio->grupos->count();
        if($referencias>0){
            return redirect()->route('socios')
                            ->withErrors('No se puede eliminar el registro porque existen datos asociados.');
        }
        //elimina referencias de los recurso asociados
        foreach ($socio->recursos as $r ) {
            $r->delete();
        }
        /// elimina asociacion entre servicio y socio
        $socio->servicios()->detach();
        $socio->delete();/* borra el registro */
        return redirect()->route('socios')->with('message', 'Registro eliminado exitosamente.');

      }

      public function visualizar(Socio $socio){
          //dd($material);   para ver el contenido de la variable
          //dd($socio->grupos);
          return view('datos.socios.visualizar', compact(  'socio'));
      }

      public function listar_asignacion(){
        $asignaciones = Asignacion::with();

      }
}
