<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pedido;
class PedidoController extends Controller
{
  public function index(){
    $pedidos =Pedido::whereUser_id( auth()->id() )
                        ->orderBy('id' , 'desc')
                        ->paginate(3);
  //  dd($compras);
    return view('cliente.pedidos', compact('pedidos'));
  }

}
