<?php

namespace App\Http\Controllers;

use App\Compra;
use App\Movimiento;
use App\Pedido;
use Illuminate\Http\Request;
use MercadoPago\MerchantOrder;
use MercadoPago\Payment;
use MercadoPago\SDK;
use App\ServicioContratado;

class NotificacionesController extends Controller
{
    public function ipn_mercadopago()
    {
        // túnel para que vuelva con los parámetros correctos
        if (isset($_GET['payment_id']) )
            return redirect()->route('notificacion', [
                'topic' => 'payment', 'id' => $_GET['payment_id']
            ]);

        // chequeo los parámetros necesarios
        if (! isset($_GET['id'], $_GET['topic']) )
            return response()->noContent(400, []);


        // chequeo que sean números
        if (! ctype_digit( $_GET['id'] ) )
            return response()->noContent(400, []);


        // verifico que no registre dos veces la misma notificación
        if ( Movimiento::whereNumero( $_GET['id'] )->count() > 0 )
            return response('', 200);


        SDK::setAccessToken( config('parametros.mercadopago_access_token') );

        $merchant_order = null;


        if ( $_GET['topic'] === 'merchant_order' )
            $merchant_order = MerchantOrder::find_by_id( $_GET['id'] );

        if ( $_GET['topic'] === 'payment')
        {
            $payment = Payment::find_by_id( $_GET['id'] );

            if ( ! property_exists( $payment->order ,'id') )
                return response()->noContent(400, []);

            $merchant_order = MerchantOrder::find_by_id($payment->order->id);
        }


        if ( is_null($merchant_order) )
            return response()->noContent(400, []);

        $compra = Compra::findOrFail($merchant_order->external_reference);
        $monto  = $merchant_order->total_amount;

        if ($monto > 0)
        {
            // MercadoPago usa 'Y-m-d\TH:i:s.vP' como formato
            $fecha = substr($merchant_order->last_updated, 0, 19);
            $fecha = str_replace('T', ' ', $fecha);

            $usuario = $compra->user;
            $usuario->update(['saldo'=> $usuario->saldo + $monto]);

            $m = Movimiento::create([
                'numero'  => $_GET['id'],
                'fecha'   => $fecha,// fecha del movimiento de mercadoP.
                'tipo'    => 'Pago de servicio',//descripcion
                'importe' => number_format($monto, 2, ',', ''),
                'saldo'   => number_format($usuario->saldo, 2, ',', ''),
                'user_id' => $usuario->id,
            ]);
            //dd($compra->items);

            $p = Pedido::create([
                'costo' => 0,
                'senia' => $monto,
                'user_id' => $usuario->id,
            ]);
            $costo_pedido = 0;
            foreach ($compra->items as $i)
            {
                $sc=ServicioContratado::create([
                    'fecha_prestacion' => $i->fecha,
                    'pedido_id' => $p->id,
                    'servicio_id' => $i->servicio_id,
                ]);
                $sc->asignar();
                $costo_pedido += $sc->servicio->costo;
            }
            $p->update([
                'costo' => $costo_pedido,
            ]);
            return redirect()->route('home')
                             ->with(['message' => 'Pago registrado exitosamente']);
        }
	}
}
