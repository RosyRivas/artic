<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Servicio;
use App\Pedido;
use App\Asignacion;
use Illuminate\Support\Facades\DB;
class HomeController extends Controller
{
    public function index()
    {
      if (!auth()->check()) {
          $s= Servicio::all();
          $servicios = $s->random(4);
          return view('home.user',compact('servicios'));
      }
      else{
          $rol= auth()->user()->rol_id;

          switch ($rol) {
              // administrador
              case '1':
                      return redirect()->route('estadisticas.ingresos');
                 break;

            //AUDITOR
              case '2':
                  $tablas = DB::select("SHOW TABLES");

                  $request = [
                      'tabla'          => null,
                      'rid'            => null,
                      'clave'          => null,
                      'accion'         => null,
                      'valor_anterior' => null,
                      'valor_nuevo'    => null,
                      'user_id'        => null,
                      'desde'          => null,
                      'hasta'          => null,
                  ];

                  return view('auditor.todos', compact('request', 'tablas'));
                  break;
            //socio
            case '3':
                $socio = auth()->user()->socio;

                $asignaciones = Asignacion::select('asignaciones.*')
                                    ->join('servicios_contratados as sc', 'sc.id', 'asignaciones.servicio_contratado_id')
                                    ->whereSocio_id($socio->id)
                                    ->whereTerminado(false)
                                    ->whereAceptado(true)
                                    ->get();
                return view('socios.asignaciones', compact('asignaciones'));
            // Cliente - Usuario cumun
            case '4':
                $s= Servicio::all();
                $servicios = $s->random(4);
                return view('home.user',compact('servicios'));
            // SYSADMIN empleado de la coop
            case '5':
            $pedidos = Pedido::whereEsta_pagado(false)
                                ->get();
            return view('pagos.index', compact('pedidos'));

          }
      }
    }
}
