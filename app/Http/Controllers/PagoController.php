<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Servicio;
use App\Compra;
use App\Item;
use MercadoPago\MerchantOrder;
use MercadoPago\Payment;
use MercadoPago\SDK;
use App\ServicioContratado;
use App\Pedido;
use App\Movimiento;
use Illuminate\Support\Facades\Mail;
use App\Mail\PagoServicio;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;

class PagoController extends Controller
{
    public function pagar(Request $request){
      //  dd($request->all());
          $servicios_contratados= $request->ids;
          $reglas = array();
          foreach($servicios_contratados as $i => $sc){
            $reglas['fecha.' . $i] = 'required|date|after_or_equal:now';
          }
            request()->validate(
              $reglas, [
              'required' => 'Campo obligatorio',
              'date' => 'Fecha no valida',
              'after_or_equal'=> 'La fecha debe ser posterior o igual a hoy',
            ]);


            foreach ($servicios_contratados as $i => $sc){
              $s= Servicio::find($sc);

              if (! $s->esta_disponible($request['fecha'][$i])){
                foreach($request['fecha'] as $i => $f)
                    session()->put('fechas.' . $i, $f);

                return redirect()->route('carrito')->withErrors('El servicio ' . $s->nombre . ' no esta disponible para la fecha seleccionada');
              }
            }
          //dd($request->all());

          $c = Compra::create(['user_id' => auth()->id()]);
          $items = [];
          $total= 0;
          //dd($request->all());
          foreach($servicios_contratados as $i => $sc){
            //  $s= Servicio::find($i);


              Item::create([
                  'costo' =>$request->precios[$i]/2,
                  'fecha' => $request->fecha[$i],
                  'compra_id' => $c->id,
                  'servicio_id' => $request->ids[$i],
              ]);
            $total+=$request->precios[$i]/2;

          }
        //  $preference= new \MercadoPago\Preference();
        session()->forget('servicios');
        session()->put('items', 0);
          return redirect()->away($c->init_point_mercadopago($total));
    }

    public function ver_pagos(){
        $pedidos = Pedido::whereEsta_pagado(false)
                            ->get();
        return view('pagos.index', compact('pedidos'));

    }

    public function pago_pedido(Pedido $pedido){

            return view('pagos.detalle_pedido', compact('pedido'));
    }

    public function registrar_pago(Pedido $pedido){
        $pedido->update([
            'esta_pagado' => true,
        ]);
        $usuario= $pedido->user;
        $usuario->update(['saldo'=> $usuario->saldo + ($pedido->costo-$pedido->senia)]);

        $m = Movimiento::create([
            'numero'  => sprintf('%011d', Movimiento::count()+1),
            'fecha'   => \DateTime::createFromFormat('Y-m-d H:i:s', now()),
            'tipo'    => 'Pago saldo del pedido',//descripcion
            'importe' => number_format(($pedido->costo-$pedido->senia), 2, ',', ''),
            'saldo'   => number_format($usuario->saldo, 2, ',', ''),
            'user_id' => $usuario->id,
        ]);
        $this->comprobante_pago($pedido);
        return $this->ver_pagos()->with([
          'message' => 'Se realizo el pago correctamente'
        ]);
    }
    public function comprobante_pago($pedido){
        $usuario= $pedido->user;
        $datos['titulo']= 'comprobante';
        $pdf = \App::make('dompdf.wrapper');
        $pdf->getDomPDF()->set_option("enable_php", true);
        $data = [
            'pedido'     => $pedido,
        ];
        $pdf->loadView('pagos.ticket', $data);
        Mail::to($usuario)->send(new PagoServicio($datos,$pdf->output()));

    }

    public function factura($pedido){
            $usuario = $pedido->user;
            $pdf = \App::make('dompdf.wrapper');
          //  dd($j);
            $pdf->getDomPDF()->set_option("enable_php", true);
            $data = [
                'pedido'    =>   $pedido,
            ];
            $pdf->loadView('pagos.factura', $data);
            return $pdf->stream();
        }

}
