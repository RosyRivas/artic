<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class EstadisticaController extends Controller
{
    public function ingresos(Request $request){
        $request->validate([
            'desde' =>'nullable|date|before_or_equal:now',
            'hasta' =>'nullable|date|before_or_equal:now|after_or_equal:desde',

        ],[
            'date'=> 'fecha no valida',
            'desde.before_or_equal' =>'fecha no valida',
            'hasta.before_or_equal'=>'fecha no valida',
            'after_or_equal'=>'fecha no valida',
        ]);

        $desde = $request->desde;
        $hasta = $request->hasta;

        $sql= DB::table('servicios_contratados')
                    ->join('servicios','servicios_contratados.servicio_id', 'servicios.id')
                    ->select(DB::raw("sum(costo) as total, servicios.nombre as servicio"))
                    ->groupBy('servicios.nombre');


        if (!is_null($desde)) {
            $sql=$sql->where('fecha_prestacion','>=',$desde);
        }

        if (!is_null($hasta)) {
            $sql=$sql->where('fecha_prestacion','<=',$hasta);
        }

        $ingresos=$sql->get();
        foreach ($ingresos as $i) {
                $i->importe = '$ '.number_format($i->total,2,',','.');
        }
        return view('estadisticas.ingreso', compact('ingresos','desde','hasta'));
    }

    public function servicios_mas_pedidos(Request $request){

        $request->validate([
            'desde' =>'nullable|date|before_or_equal:now',
            'hasta' =>'nullable|date|before_or_equal:now|after_or_equal:desde',

        ],[
            'date'=> 'fecha no valida',
            'desde.before_or_equal' =>'fecha no valida',
            'hasta.before_or_equal'=>'fecha no valida',
            'after_or_equal'=>'fecha no valida',
        ]);
        $desde = $request->desde;
        $hasta = $request->hasta;

        $sql= DB::table('servicios_contratados')
                    ->join('servicios','servicios_contratados.servicio_id', 'servicios.id')
                    ->select(DB::raw("count(*) as contrataciones, servicios.nombre as servicio"))
                    ->groupBy('servicios.nombre');

        if (!is_null($desde)) {
            $sql=$sql->where('fecha_prestacion','>=',$desde);
        }

        if (!is_null($hasta)) {
            $sql=$sql->where('fecha_prestacion','<=',$hasta);
        }
        $servicios= $sql->get();

        return view('estadisticas.servicios_contratados', compact('servicios','desde','hasta'));
    }

    public function materiales_mas_reservados(Request $request){

        $request->validate([
            'desde' =>'nullable|date|before_or_equal:now',
            'hasta' =>'nullable|date|before_or_equal:now|after_or_equal:desde',

        ],[
            'date'=> 'fecha no valida',
            'desde.before_or_equal' =>'fecha no valida',
            'hasta.before_or_equal'=>'fecha no valida',
            'after_or_equal'=>'fecha no valida',
        ]);
        $desde = $request->desde;
        $hasta = $request->hasta;

        $sql= DB::table('recursos_prestados')
                    ->join('materiales','recursos_prestados.material_id', 'materiales.id')
                    ->select(DB::raw("count(*) as prestamos, materiales.nombre as material"))
                    ->groupBy('materiales.nombre');

        if (!is_null($desde)) {
            $sql=$sql->where('fecha_prestamo','>=',$desde);
        }

        if (!is_null($hasta)) {
            $sql=$sql->where('fecha_prestamo','<=',$hasta);
        }
        $materiales= $sql->get();

        return view('estadisticas.materiales_prestados', compact('materiales','desde','hasta'));
    }
}
