<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AuditoriaController extends Controller
{
    public function todos()
    {
        $tablas = DB::select("SHOW TABLES");

        $request = [
            'tabla'          => null,
            'rid'            => null,
            'clave'          => null,
            'accion'         => null,
            'valor_anterior' => null,
            'valor_nuevo'    => null,
            'user_id'        => null,
            'desde'          => null,
            'hasta'          => null,
        ];

        return view('auditor.todos', compact('request', 'tablas'));
    }


    public function filtrados(Request $request)
    {
        $request->validate([
            'desde' => 'nullable|date|after_or_equal:2018-07-04|before_or_equal:now',
            'hasta' => 'nullable|date|after_or_equal:2018-07-04|after_or_equal:desde' .          '|before_or_equal:now',
        ], [
            'date'                  => 'Ingresá una fecha válida',
            'desde.after_or_equal'  => 'La fecha de inicio no puede ser inferior al 04/07/2018',
            'hasta.after_or_equal'  => 'Revisá la fecha de fin',
            'hasta.before_or_equal' => 'Revisá la fecha de fin',
        ]);

        $filtros = array();
        $vacio   = true;

        if ( ! is_null($request->tabla) )
        {
            $filtros['tabla'] = "tabla = '" . filter_var($request->tabla, FILTER_SANITIZE_STRING) . "'";
            $vacio = false;
        }

        if ( ! is_null($request->rid) )
        {
            $filtros['rid'] = "rid = " . filter_var($request->rid, FILTER_SANITIZE_STRING);
            $vacio = false;
        }

        if ( ! is_null($request->clave) )
        {
            $filtros['clave'] = "clave = '" . filter_var($request->clave, FILTER_SANITIZE_STRING) . "'";
            $vacio = false;
        }

        if ( $request->accion != 'C' && ! is_null($request->valor_anterior) )
        {
            $filtros['valor_anterior'] = "valor_anterior = '" . filter_var($request->valor_anterior, FILTER_SANITIZE_STRING) . "'";
            $vacio = false;
        }

        if ( $request->accion != 'B' && ! is_null($request->valor_nuevo) )
        {
            $filtros['valor_nuevo'] = "valor_nuevo = '" . filter_var($request->valor_nuevo, FILTER_SANITIZE_STRING) . "'";
            $vacio = false;
        }

        if ( ! is_null($request->user_id) )
        {
            $filtros['user_id'] = "user_id = " . filter_var($request->user_id, FILTER_SANITIZE_STRING);
            $vacio = false;
        }

        if ( ! is_null($request->desde) )
        {
            $filtros['desde'] = "fecha >= '" . filter_var($request->desde, FILTER_SANITIZE_STRING) . " 00:00:00'";
            $vacio = false;
        }

        if ( ! is_null($request->hasta) )
        {
            $filtros['hasta'] = "fecha <= '" . filter_var($request->hasta, FILTER_SANITIZE_STRING) . " 23:59:59'";
            $vacio = false;
        }

        if ($vacio)
            return redirect()->route('logs.todos');

        $cadena = '';

        foreach ($filtros as $f)
            $cadena = $cadena . ' AND ' . $f;

        if ($cadena != '')
            $cadena = ' WHERE' . substr($cadena, 4);

        $sql = '';

        switch ($request->accion)
        {
            case ('C'):
                $sql = "SELECT * FROM (SELECT 'C' as accion, id_creado AS rid, fecha, tabla, clave, null AS valor_anterior, valor AS valor_nuevo, user_id FROM creados) AS tc" . $cadena;
                break;
            case ('M'):
                $sql = "SELECT * FROM (SELECT 'M' as accion, id_modificado AS rid, fecha, tabla, clave, valor_anterior, valor_nuevo, user_id FROM modificados) AS tc" . $cadena;
                break;
            case ('B'):
                $sql = "SELECT * FROM (SELECT 'B' as accion, id_borrado AS rid, fecha, tabla, clave, valor AS valor_anterior, null as valor_nuevo, user_id FROM borrados) AS tc" . $cadena;
                break;
            default:
                $sql = "SELECT * FROM (SELECT 'C' as accion, id_creado AS rid, fecha, tabla, clave, null AS valor_anterior, valor AS valor_nuevo, user_id FROM creados UNION SELECT 'M' as accion, id_modificado AS rid, fecha, tabla, clave, valor_anterior, valor_nuevo, user_id FROM modificados UNION SELECT 'B' as accion, id_borrado AS rid, fecha, tabla, clave, valor AS valor_anterior, null as valor_nuevo, user_id FROM borrados) AS tc" . $cadena;
                break;
        }


        $logs   = DB::select($sql . " ORDER BY fecha desc");
        $tablas = DB::select("SHOW TABLES");

        return view('auditor.filtrados', compact('logs', 'tablas', 'request'));
    }

    public function logs()
      {
          $logs = DB::select("SELECT 'C' as accion, id_creado AS rid, fecha, tabla, clave, null AS valor_anterior, valor AS valor_nuevo, user_id FROM creados UNION SELECT 'M' AS accion, id_modificado AS rid, fecha, tabla, clave, valor_anterior, valor_nuevo, user_id FROM modificados UNION SELECT 'B' AS accion, id_borrado AS rid, fecha, tabla, clave, valor AS valor_anterior, null AS valor_nuevo, user_id FROM borrados ORDER BY fecha desc LIMIT 5000");

          return datatables()->of($logs)->toJson();
      }
}
