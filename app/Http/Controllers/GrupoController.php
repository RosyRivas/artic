<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Grupo;
use App\Socio;
use App\Http\Requests\GrupoRequest;

class GrupoController extends Controller
{
        public function index(){
            $grupos = Grupo::orderBy('id','desc')->get();
            //dd($materiales);

            return view('datos.grupos.index', compact('grupos') );
        }
        public function crear(){
            $socios= Socio::all();
            //dd($socios);

            return view('datos.grupos.crear', compact('socios') );
        }
        public function  almacenar(GrupoRequest $request){
              $g = Grupo::create($request->all());
              $g->socios()->attach($request->socios);
              $g->socios()->attach($request->jefe_id);
              return redirect()->route('grupos')->with('message','Grupo creado exitosamente');

        }

        public function editar(Grupo $grupo){
            //dd($material);   para ver el contenido de la variable
            $socios = Socio::all();
            return view('datos.grupos.editar', compact('socios', 'grupo'));
        }

        public function actualizar(GrupoRequest $request, Grupo $grupo){
            $grupo->update($request->all());
            $grupo->socios()->sync($request->socios);
            $grupo->socios()->sync($request->jefe_id);
            return redirect()->route('grupos')->with('message', 'Registro modificado exitosamente.');
        }

        public function eliminar(Grupo $grupo){
              // ver mas adelante si este grupo  realizo un servicio
              //en ese caso tengo que desabilitar el grupo
              // no se deberia elimanar asi nomas
              $grupo->socios()->detach();
              $grupo->delete();
              return redirect()->route('grupos')->with('message', 'Registro eliminado exitosamente.');
          }

          public function visualizar(Grupo $grupo){
              //dd($material);   para ver el contenido de la variable
              return view('datos.grupos.visualizar', compact('grupo'));
          }
}
