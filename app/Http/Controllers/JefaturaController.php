<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Jefatura;

use App\Http\Requests\JefaturaRequest;

class JefaturaController extends Controller
{
    public function index(){
        $jefaturas = Jefatura::orderBy('id','desc')->get();
        //dd($jefaturas);
        return view('datos.jefaturas.index', compact('jefaturas') );
    }

    public function crear(){
        return view('datos.jefaturas.crear' );
    }

    public function almacenar(JefaturaRequest $request){
        Jefatura::create($request->all());
        return redirect()->route('jefaturas')->with(['message'=>'Jefatura creada exitosamente']);
    }


    public function editar(Jefatura $jefatura){
        //dd($jefatura);   para ver el contenido de la variable
        return view('datos.jefaturas.editar', compact('jefatura'));
    }

    public function actualizar(JefaturaRequest $request, Jefatura $jefatura){
        $jefatura->update($request->all());
        return redirect()->route('jefaturas')->with('message', 'Registro modificado exitosamente.');
    }

    public function eliminar(Jefatura $jefatura){
        //$referencias = Material::whereJefatura_id($jefatura->id)->count(); /* cuenta las referencias en la tabla relacionada */
        $referencias=0;
        if($referencias>0){
            return redirect()->route('jefaturas')
                            ->withErrors('No se puede eliminar el registro porque existen datos asociados.');
        }

        $jefatura->delete();/* borra el registro */
        return redirect()->route('jefaturas')->with('message', 'Registro eliminado exitosamente.');
    }

}
