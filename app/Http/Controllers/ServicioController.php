<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ServicioRequest;
use App\Servicio;
use App\Jefatura;
use App\JefaturaRequest;
use App\Recurso;
use App\RecursoPrestado;
use App\Http\Requests\RecursoRequest;
use App\Http\Requests\SocioRequest;
use App\Material;
use App\Socio;
use App\Grupo;
use App\Asignacion;
use App\ServicioContratado;
use App\Http\Requests\MaterialRequest;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use App\Movimiento;
use App\Calificacion;
use App\CalificacionSocio;
use App\CalificacionGrupo;
use Illuminate\Support\Facades\Mail;
use App\Mail\SolicitudCalificacion;

class ServicioController extends Controller
{
          public function index(Request $request){

              $minimo = $request->minimo;
              $maximo = $request->maximo;
              $jefatura = $request->jefatura;


              if ( ! is_null($minimo) && ! is_null($maximo) ) {
                  $request->validate([
                      'minimo' => 'lte:maximo',
                  ],[
                      'lte' => 'Ingrese un valor menor al maximo',
                  ]);
              }

              $sql= Servicio::with(['jefatura','recursos','recursos.material'])->orderBy('id', 'desc');

              if(!is_null($minimo)){
                  $sql= $sql->where('costo', '>=', $minimo);
              }

              if(!is_null($maximo)){
                  $sql= $sql->where('costo', '<=', $maximo);
              }

              if(!is_null($jefatura)){
                  $sql= $sql->whereJefatura_id($jefatura);
              }

              $servicios = $sql->get();
              $jefaturas = Jefatura::all();
              if($request->exists('pdf')){
              //  dd($minimo);
                  return $this->descargar($servicios,$minimo, $maximo, $jefatura);
              }
              //dd($servicios);
                //dd(Servicio::whereJefatura_id($jefatura)->get());
              return view('datos.servicios.index', compact('servicios','jefaturas','jefatura','minimo','maximo') );
          }

          public function crear(){
              $jefaturas= Jefatura::all();
              $materiales= Material::all();
              $socios= Socio::all();
              $grupos= grupo::all();
              return view('datos.servicios.crear', compact('jefaturas', 'materiales', 'socios' ,'grupos') );
          }

          public function almacenar(ServicioRequest $request){
              //dd($request->all());
              request()->validate([
                'archivo' =>'required|image|mimes:jpeg,png,jpg'
              ], [
                'required' => 'Campo obligatorio',
                'image' => 'El archivo seleccionado no es una imagen',
                'mimes'=> 'Formato no soportado'
              ]);
              // controles para la subir imagen
              $mime=$request->archivo->getMimeType();
              $ext='.' . substr($mime, strpos($mime, '/')+ 1);

              $request->merge(['imagen'=> now()->format('Ymd-His') . $ext]);
              //mueve la imagen a la carpeta public
              $request->archivo->move(
                public_path('/images/servicios'), $request->imagen
              );


              $s = Servicio::create($request->all());
              $s->socios()->attach($request->socios);
              $s->grupos()->attach($request->grupos);
              foreach ($s->socios as $socio ) {
                  CalificacionSocio::create([
                      'socio_id' => $socio->id,
                      'servicio_id' => $s->id,
                      'calificacion' => 3,
                  ]);
              }

              foreach ($s->grupos as $g ) {
                  CalificacionSocio::create([
                      'grupo_id' => $g->id,
                      'servicio_id' => $s->id,
                      'calificacion' => 3,
                  ]);
              }
              // control para agregar recursos a mi servicio
              if (!empty($request->cantidades)){
                foreach ($request->cantidades as $i => $cant) {
                  Recurso::create([
                        'cantidad'    =>  $cant,
                        'material_id' =>  $request->materiales[$i],
                        'servicio_id' =>  $s->id,
                  ]);
                }
              }



              return redirect()->route('servicios')->with('message','Servicio creado exitosamente');
          }

          public function editar(Servicio $servicio){
              //dd($material);   para ver el contenido de la variable
              $materiales=Material::all();
              $jefaturas= Jefatura::all();
              $grupos= Grupo::all();
              $socios=Socio::all();
              return view('datos.servicios.editar', compact('jefaturas', 'materiales', 'servicio', 'grupos', 'socios'));
          }

          public function actualizar(ServicioRequest $request, Servicio $servicio){
              request()->validate([
                'archivo' =>'nullable|image|mimes:jpeg,png,jpg'
              ], [
                'image' => 'El archivo seleccionado no es una imagen',
                'mimes'=> 'Formato no soportado'
              ]);

              $servicio->update($request->all());// groupby desc

              $servicio->socios()->sync($request->socios);
              $servicio->grupos()->sync($request->grupos);
              //  comparacion de materiales
              if (count($servicio->recursos) > 0){
                  foreach ($servicio->recursos as $r) {
                      if(!is_array($request->materiales) || !in_array($r->material_id, $request->materiales))
                            $r->delete();
                  }
              }
              //array de materiales
              if (is_array($request->materiales)){
                //dd('pepa');
                  foreach ($request->materiales as $index => $m) {
                      Recurso::updateOrCreate([
                          'material_id' => $m,
                          'servicio_id' => $servicio->id,
                      ],[
                          'cantidad'    => $request->cantidades[$index],
                      ]);
                  }
              }

              return redirect()->route('servicios')->with('message','Registro modificado exitosamente.');
          }

          public function eliminar(Servicio $servicio){

              $referencias=0;
              if($referencias>0){
                  return redirect()->route('servicios')
                                  ->withErrors('No se puede eliminar el registro porque existen datos asociados.');
              }
              $servicio->socios()->detach();
              foreach ($servicio->recursos as $r ) {
                  $r->delete();
              }

              $servicio->delete();/* borra el registro */
              return redirect()->route('servicios')->with('message', 'Registro eliminado exitosamente.');
          }

          public function descargar($servicios, $minimo, $maximo, $j){
              $jefatura = Jefatura::find($j);
              $pdf = \App::make('dompdf.wrapper');
            //  dd($j);
              $pdf->getDomPDF()->set_option("enable_php", true);
              $data = [
                  'servicios'    =>   $servicios,
                  'jefatura'     =>   $jefatura,
                  'minimo'       =>   $minimo,
                  'maximo'       =>   $maximo,
              ];
              $pdf->loadView('datos.reportes.servicios', $data);
              return $pdf->stream();
          }
          //visualiza el contenido de cada servicio
          public function visualizar(Servicio $servicio){
              //dd($material);   para ver el contenido de la variable


              return view('datos.servicios.visualizar', compact(  'servicio'));
          }
          //////////// cambio de estado del booleano estado del servicio
          public function habilitar(Servicio $servicio, $status){
              $servicio->update(['esta_activo' => (bool) $status]);
              return response()->json(['success'=>'Se ha cambiado el estado del servicio.']);
            }
            ////// funcion para ver cosas al carrito de compra
          public function carrito(){

                $servicios_contratados= session()->get('servicios');

                //dd($servicios_contratados);
                if(is_null($servicios_contratados) || count($servicios_contratados)== 0)
                  return view('carrito-vacio');

                session()->put('items', count($servicios_contratados));

                $items= array();
                $total= 0;

                foreach ($servicios_contratados as $i => $costo) {
                  $servicio= Servicio::find($i);
                  $items[$i]['nombre']=$servicio->nombre;
                  $items[$i]['imagen']=$servicio->imagen;
                  $items[$i]['precio']=$costo;
                  $items[$i]['fecha']=session()->get('fechas.' . $i);
                  $total+= $costo;
                }


                session()->put('items', count($servicios_contratados));
                return view('carrito', compact('items', 'total'));
            }
            // agrega items al carrito
            public function agregar(Servicio $servicio, $fecha){
                if ($servicio->esta_disponible($fecha)) {
                  session()->put('servicios.' . $servicio->id, $servicio->costo);
                  session()->put('fechas.' . $servicio->id, $fecha);
                  session()->put('items', count(session()->get('servicios')));
                  //dd(session()->get('servicios'));
                  return response()->json([
                    'cantidad'=>count(session()->get('servicios')),
                    'success'=>'Servicio agregado al carrito.'
                  ]);
                }
                else{
                  return response()->json([
                    'cantidad'=>is_null(session()->get('servicios')) ? 0 : count(session()->get('servicios')),
                    'success'=>'El servicio no se encuentra disponible, seleccione una nueva fecha'
                  ]);
                }
            }
            // quitar el item del carrito
            public function quitar(Servicio $servicio){
                session()->forget('servicios.' . $servicio->id);
                session()->forget('fechas.' . $servicio->id);
                session()->put('items', count(session()->get('servicios')));
                return response()->json([
                  'cantidad'=>count(session()->get('servicios')),
                  'success'=>'Servicio eliminado del carrito.'
                ]);
            }

            public function buscar(Request $request){
                $nombre= $request->get('nombre');
                $servicios= Servicio::buscar($nombre);
                //$servicio= Servicio::where('nombre','LIKE' ,"%$nombre%" )->get();
                return view('filtros-servicio')->with('servicios',$servicios);
            }

            public function listado(){
              //servicio grupo

                $prestados = array_column(DB::select("SELECT servicio_id FROM servicio_socio UNION SELECT servicio_id FROM grupo_servicio GROUP BY servicio_id"), 'servicio_id');
                $sql = Servicio::whereIn('id', $prestados);
                $filtros = array();

                if (isset($_GET['nombre'])){
                  //dd($_GET['nombre']);
                  $nombre = $_GET['nombre'];
                  $sql = $sql->where('nombre','LIKE' ,"%$nombre%" );
                  $filtros['nombre'] = $nombre;
                }
                else {
                  $filtros['nombre'] = '';
                }

                if (isset($_GET['jefaturas'])){
                  //dd($_GET['jefaturas']);
                  $jefaturas = $_GET['jefaturas'];
                  $sql = $sql->whereIn('jefatura_id', $jefaturas);
                  $filtros['jefaturas'] = $jefaturas;
                }
                else{
                  $filtros['jefaturas'] = array();
                }

                if(isset($_GET['min']) && $_GET['min'] != "" ){
                    $min = $_GET['min'];
                    $sql= $sql->where('costo', '>=', $min);
                    $filtros['min']= $min;
                }
                else{
                    $filtros['min'] = '';
                }

                if(isset($_GET['max'])&& $_GET['max'] != "" ){
                    $max = $_GET['max'];
                    $sql= $sql->where('costo', '<=', $max);
                    $filtros['max']= $max;
                }
                else{
                    $filtros['max'] = '';
                }

                $jefaturas = Jefatura::all();
                $servicios = $sql->paginate(5)->appends(request()->query());
                //dd($servicios);
                return view('servicios.listado', compact('servicios', 'filtros', 'jefaturas'));

            }

            public function aceptar_asignacion(Asignacion $asignacion, Request $request){
                if(!is_null($asignacion->aceptado))
                    return redirect()->route('asignaciones')->withErrors('No se puede aceptar el servicio');

                //    dd($asignacion->servicio_contratado->servicio);

                if (! is_null($asignacion->grupo)){
                    $responsable = $asignacion->grupo->jefe;
                }else{
                    $responsable = $asignacion->socio;
                }


                foreach ($request->recursos as $key => $value) {
                  if (!is_null($value)) {
                      RecursoPrestado::create([
                          'fecha_prestamo'  => $asignacion->servicio_contratado->fecha_prestacion,
                          'cantidad'        => $value,
                          'material_id'     => $key,
                          'servicio_id'     => $asignacion->servicio_contratado->servicio_id,
                          'responsable_id'  => $responsable->id,
                      ]);
                  }
                }
                $asignacion->update([
                      'aceptado' => true,
                ]);
                return redirect()->route('asignaciones')->with([
                  'message' => 'Asignacion aceptada correctamente '
                ]);
            }

            public function rechazar_asignacion(Asignacion $asignacion){
                if(!is_null($asignacion->aceptado))
                    return redirect()->route('asignaciones')
                    ->withErrors('No se puede rechazar el servicio');


                return view('socios.rechazo' , compact('asignacion'));

            }

            public function guardar_rechazo(Asignacion $asignacion){
                $asignacion->update([
                      'aceptado' => false,
                ]);
                $asignacion->servicio_contratado->asignar();
                return redirect()->route('home')->with([
                  'message' => 'Respuesta enviada correctamente'
                ]);
            }

            public function listar_asignacion(){
                $socio = auth()->user()->socio;
                // filtrar asignaciones terminado en falso.

                $asignaciones = Asignacion::select('asignaciones.*')
                                    ->join('servicios_contratados as sc', 'sc.id', 'asignaciones.servicio_contratado_id')
                                    ->whereSocio_id($socio->id)
                                    ->whereTerminado(false)
                                    ->whereAceptado(true)
                                    ->get();
                                    //dd($asignaciones);
              return view('socios.asignaciones', compact('asignaciones'));
            }
            //funcion de resarva de materiales
            public function reservar_materiales(Asignacion $asignacion){
              if(!is_null($asignacion->aceptado))
                  return redirect()->route('asignaciones')->withErrors('No se puede aceptar el servicio');

              return view('socios.reservas', compact('asignacion'));
            }
            // reserva de materiales para asignaciones forzosas
            public function materiales_reserva(Asignacion $asignacion){
                return view('socios.reserva_forzosa', compact('asignacion'));
            }


            public function recursos_asignacion_forzosa(Asignacion $asignacion, Request $request){
                if (! is_null($asignacion->grupo)){
                    $responsable = $asignacion->grupo->jefe;
                }else{
                    $responsable = $asignacion->socio;
                }


                foreach ($request->recursos as $key => $value) {
                  if (!is_null($value)) {
                      RecursoPrestado::create([
                          'fecha_prestamo'  => $asignacion->servicio_contratado->fecha_prestacion,
                          'cantidad'        => $value,
                          'material_id'     => $key,
                          'servicio_id'     => $asignacion->servicio_contratado->servicio_id,
                          'responsable_id'  => $responsable->id,
                      ]);
                  }
                }

                return redirect()->route('asignaciones')->with([
                  'message' => 'Recursos reservados'
                ]);
            }
            // funcion para clasificar los recursos de un socio
            public function materiales_prestados(){
                    $recursos_prestados = RecursoPrestado::whereEsta_devuelto(false)->get();
                    $socios = array();
                    foreach ($recursos_prestados as $rp)
                        $socios[$rp->responsable_id] = $rp->responsable;

                    //dd($prestamos);
                    return view('reservas.prestacion',compact('socios'));
            }
            // funcion para ver los prestamos por devolver
            public function prestamos(Socio $socio){
                $recursos = RecursoPrestado::whereEsta_devuelto(false)
                                            ->whereResponsable_id($socio->id)->get();
                return view('reservas.materiales',compact('socio','recursos'));
            }

            //funcion para devolver  los materiales
            public function devolucion( Request $request){
                //dd($request->all());
                $fecha = now('America/Argentina/Buenos_Aires');

                foreach ($request->materiales as $key => $value)
                    RecursoPrestado::find($key)->update([
                        'esta_devuelto'         => true,
                        'estado_devolucion'     => $request->estados[$key],
                        'fecha_devolucion'      => $fecha,
                    ]);

                return redirect()->route('recursos_prestados')->with([
                  'message' => 'Se registro la devolucion de materiales exitosamente '
                ]);
            }

            //cambia el estado de el campo terminado a true para
            //la finalizacion del servicios contratado
            public function terminar_prestacion(ServicioContratado $sc){
            //    dd($sc->asignacion);
                $sc->update([
                    'terminado'     =>true,
                ]);
                $usuario = $sc->pedido->user;
                $usuario->update(['saldo'=> $usuario->saldo - $sc->servicio->costo ]);
                $m = Movimiento::create([
                    'numero'  => sprintf('%011d', Movimiento::count()+1),
                    'fecha'   => \DateTime::createFromFormat('Y-m-d H:i:s', now()),
                    'tipo'    => 'Costo de servicio prestado',//descripcion
                    'importe' => number_format(-$sc->servicio->costo, 2, ',', ''),
                    'saldo'   => number_format($usuario->saldo, 2, ',', ''),
                    'user_id' => $usuario->id,
                ]);
                if (!is_null($sc->asignacion->socio_id)) {
                    $calificacion= Calificacion::create([
                        'fecha'   => \DateTime::createFromFormat('Y-m-d H:i:s', now()),
                        'puntaje' => null,
                        'socio_id'=> $sc->asignacion->socio_id,
                        'asignacion_id'=>$sc->asignacion->id,
                        'servicio_id'=>$sc->servicio_id,
                    ]);
                }
                if (!is_null($sc->asignacion->grupo_id)) {
                    $calificacion= Calificacion::create([
                        'fecha'   => \DateTime::createFromFormat('Y-m-d H:i:s', now()),
                        'puntaje' => null,
                        'grupo_id'=> $sc->asignacion->grupo_id,
                        'asignacion_id'=>$sc->asignacion->id,
                        'servicio_id'=>$sc->servicio_id,
                    ]);
                }
                Mail::to($usuario)->send(new SolicitudCalificacion($sc));
                return redirect()->route('asignaciones')->with([
                  'message' => 'Servicio finalizado correctamente'
                ]);
            }

            public function calificar(Request $request, Asignacion $asignacion){
                //dd($asignacion->socio_id);

                if (!is_null($asignacion->calificacion->puntaje))
                    return redirect()->route('home')->with([
                        'message' =>'Error al intentar calificar este servicio'
                    ]);
                //dd($request->all());

                $asignacion->calificacion->update([
                    'puntaje' => $request->estrellas,
                ]);
                if ( (!is_null($asignacion->socio_id)) && ($asignacion->aceptado != 0) ) {
                    $cs = CalificacionSocio::whereSocio_id($asignacion->socio_id)
                                        ->whereServicio_id($asignacion->servicio_contratado->servicio_id)
                                        ->first();
                    //dd($cs);
                    $calificaciones = Calificacion::whereSocio_id($asignacion->socio_id)
                                                    ->whereServicio_id($asignacion->servicio_contratado->servicio_id)
                                                    ->orderBy('fecha','desc')->take(10)->get();
                    $cs->update([
                        'calificacion' => $calificaciones->avg('puntaje'),
                    ]);

                }
                return redirect()->route('home')->with([
                    'message' =>'Calificación enviada'
                ]);
            }
}
