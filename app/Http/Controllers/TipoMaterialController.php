<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\TipoMaterial;

use App\Http\Requests\TipoMaterialRequest;

class TipoMaterialController extends Controller
{

          public function index(){
              $tipos_materiales = TipoMaterial::orderBy('id','desc')->get();
              //dd($tiposDeMateriales);
              return view('datos.tipos-materiales.index', compact('tipos_materiales') );
          }

          public function crear(){
              return view('datos.tipos-materiales.crear' );
          }

          public function almacenar(TipoMaterialRequest $request){
              TipoMaterial::create($request->all());
              return redirect()->route('tipos-materiales')
                  ->with(['message'=>'Tipo de material creado exitosamente']);
          }

          public function editar(TipoMaterial $tipo_material){
                //dd($tipoDeMaterial);   para ver el contenido de la variable
                return view('datos.tipos-materiales.editar', compact('tipo_material'));
            }

          public function actualizar(TipoMaterialRequest $request, TipoMaterial $tipo_material){
              $tipo_material->update($request->all());
              return redirect()->route('tipos-materiales')
                  ->with('message', 'Registro modificado exitosamente.');
          }

          public function eliminar(TipoMaterial $tipo_material){
              //$referencias = Material::whereTipoDeMaterial_id($tipoDeMaterial->id)->count(); /* cuenta las referencias en la tabla relacionada */
              $referencias=0;
              if($referencias>0){
                  return redirect()->route('tipos-materiales')
                                  ->withErrors('No se puede eliminar el registro porque existen datos asociados.');
              }
              $tipo_material->delete();/* borra el registro */
              return redirect()->route('tipos-materiales')
                ->with('message', 'Registro eliminado exitosamente.');
          }
  }
