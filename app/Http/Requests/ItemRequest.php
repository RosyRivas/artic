<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ItemRequest extends FormRequest
{
    public function authorize()
    {
        'costo'=> 'required',
        'fecha'=> 'date',
    }

    public function rules()
    {
        return [
          'required'=> 'campo invalido'
          'date'=> 'fecha invalida'
        ];
    }
}
