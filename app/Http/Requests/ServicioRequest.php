<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ServicioRequest extends FormRequest
{

    public function authorize()
    {
          return auth()->user()->rol_id ==5;
    }

    public function rules(){
        return [
          'nombre' => 'required',
          'jefatura_id'  => 'required',
          'costo'  => 'required',
        ];
    }
    public function messages(){
        return [
          'required' => 'Campo obligatorio',
        ];

    }
}
