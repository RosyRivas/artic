<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GrupoRequest extends FormRequest
{

    public function authorize()
    {
        return auth()->user()->rol_id ==5;
    }

    public function rules()
    {
        return [
            'nombre'=> 'required',
            'jefe_id'=> 'required',
            'socios'=> 'required|array',
        ];
    }

    public function messages(){
      return [
          'required'=> 'Campo obligatorio',
          'array' => 'Dato no valido',
      ];
    }
}
