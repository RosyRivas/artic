<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SocioRequest extends FormRequest
{
    public function authorize()
    {
          return auth()->user()->rol_id ==5;
    }


    public function rules(){
        return [
          'nombres' => 'required',
          'apellido' => 'required',
          'email' => 'required|email',

        ];
    }
    public function messages(){
        return [
          'required' => 'Campo obligatorio',
          'email' => 'Correo electronico no valido'
        ];

    }
}
