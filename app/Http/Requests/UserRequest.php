<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{

    public function authorize()
    {
          return auth()->user()->rol_id ==5;
    }

    public function rules()
    {
        return [
            'name'      =>  'required',
            'apellido'  =>  'required',
            'email'     =>  'required|email',
            'saldo'     =>  'required',
            'rol_id'    =>  'required',
        ];
    }


    public function messages(){
      return [
            'email'     =>  'Correo no valido',
            'required'  => 'Campo obligatorio',
      ];

    }
}
