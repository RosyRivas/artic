<?php

namespace App;


class CalificacionSocio extends Auditable
{
    protected $table ='calificaciones_socios';
    public $timestamps = false;
    protected $fillable = ['socio_id','servicio_id', 'calificacion'];

    public function socio(){
          return $this->belongsTo('App\Socio');
      }
    public function servicio(){
          return $this->belongsTo('App\Servicio');
    }

}
