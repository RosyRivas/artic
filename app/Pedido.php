<?php

namespace App;



class Pedido extends Auditable
{
  public $timestamps = false;

  protected $fillable = ['esta_pagado','costo','senia','user_id'];

  public function user(){
        return $this->belongsTo('App\User');
    }

  public function servicios_contratados(){
          return $this->hasMany('App\ServicioContratado');
    }
}
