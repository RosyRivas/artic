<?php

namespace App;


class Calificacion extends Auditable
{
  public $timestamps = false;
    protected $table= 'calificaciones';
    protected $fillable = ['fecha', 'puntaje','socio_id', 'grupo_id','asignacion_id','servicio_id'];



}
