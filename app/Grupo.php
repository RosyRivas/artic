<?php

namespace App;

class Grupo extends Auditable
{
    public $timestamps = false;
    protected $table = 'grupos';
    protected $fillable = ['nombre','jefe_id','socio_id'];

    public function jefe(){
        return $this->belongsTo('App\Socio');
    }
    // relacion mucho a mucho con Socios
    public function socios(){
          return $this->belongsToMany('App\Socio');
      }
    // Relacion con servicios
    public function servicios(){
            return $this->belongsToMany('App\Servicio');
      }

      public function calificaciones_grupo(){
              return $this->hasMany('App\CalificacionGrupo');
      }
    public function esta_disponible($fecha, Servicio $servicio){
          // ultima asignacion
          $anterior = Asignacion::join('servicios_contratados as sc' , 'servicio_contratado_id', 'sc.id')
                                    ->join('servicios as s', 'servicio_id', 's.id')
                                    ->whereGrupo_id($this->id)
                                    ->where('fecha_prestacion','<=',$fecha)
                                    ->orderBy('fecha_prestacion','desc')
                                    ->first();

          $posterior = Asignacion::join('servicios_contratados as sc' , 'servicio_contratado_id', 'sc.id')
                                    ->join('servicios as s', 'servicio_id', 's.id')
                                    ->whereGrupo_id($this->id)
                                    ->where('fecha_prestacion','>=',$fecha)
                                    ->orderBy('fecha_prestacion','asc')
                                    ->first();

          if (is_null($anterior) && is_null($posterior)) {
            return true;
          }

          if (!is_null($anterior)) {
              if (!is_null($servicio->duracion)) {
                //fecha [ara la prestacion
                $prestacion = new \DateTime($fecha);
                // fecha de finalizacion de la ultima prestacion
                $fin = new \DateTime($anterior->fecha_prestacion);
                if ($prestacion->diff($fin)->format('%a') < $servicio->duracion) {
                    return false;
                }
              }
          }

          if (!is_null($posterior)) {
              if (!is_null($servicio->duracion)) {
                $prestacion = new \DateTime($fecha);
                $fin = new \DateTime($posterior->fecha_prestacion);
                if ($fin->diff($prestacion)->format('%a') < $servicio->duracion) {
                    return false;
                }
              }
          }

          return true;
        }
}
