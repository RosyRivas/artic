<?php

namespace App;


use MercadoPago\SDK;
use MercadoPago\Preference;
use MercadoPago\Item;
use MercadoPago\Payer;

//modificar extender de autidable en todoas
class Compra extends Auditable
{
  public $timestamps = false;
  protected $fillable = ['user_id'];

    public function user(){
      return $this->belongsTo('App\User');
    }
    public function items(){
        return $this->hasMany('App\Item');
    }
    public function init_point_mercadopago($importe)
    {
        // datos de MercadoPago
        SDK::setAccessToken('TEST-2327499198350323-113023-3e44dfd4384fbb91f7182b5d372d98ef-258583490');

        // creo un objeto Item
        $item              = new Item();
        $item->id          = 0;
        $item->title       = 'Pago de servicios';
        $item->quantity    = 1;
        $item->currency_id = 'ARS';
        $item->unit_price  = $importe;

        // creo un objeto Preference y lo agrego
        $preference                   = new Preference();
        $preference->items            = array($item);
        $preference->back_urls        = config('parametros.back_urls');
        $preference->auto_return      = 'approved';//
        $preference->notification_url = 'http://artic.test/notificaciones';

        //crear un objeto Payer
        $payer             = new Payer();
        $payer->email      = $this->user->email;
        $preference->payer = $payer;

        //guardar y publicar el Preference
        $preference->external_reference = $this->id;
        $preference->save();

        return $preference->init_point;
    }

}
