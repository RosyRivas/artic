<?php

namespace App;

class Socio extends Auditable
{
      public $timestamps = false;

      protected $table = 'socios';

      protected $fillable = ['nombres','apellido','nombre_artistico','user_id','jefatura_id' ];


        // Relacion con usuario
      public function user(){
            return $this->belongsTo('App\User');
        }
        // Relacion con jefaturas
      public function jefatura(){
              return $this->belongsTo('App\Jefatura');
        }
        // Relacion con grupos
      public function grupos(){
              return $this->belongsToMany('App\Grupo');
        }
        // Relacion con servicios
      public function servicios(){
              return $this->belongsToMany('App\Servicio');
        }
        // Relacion con Recursos
      public function recursos(){
            return $this->hasMany('App\Recurso');
        }

        public function calificaciones_socio(){
                return $this->hasMany('App\CalificacionSocio');
        }

      public function asignaciones(){
            return $this->hasMany('App\Asignacion');
        }

      public function esta_disponible($fecha, Servicio $servicio){
          // ultima asignacion
          $anterior = Asignacion::join('servicios_contratados as sc' , 'servicio_contratado_id', 'sc.id')
                                    ->join('servicios as s', 'servicio_id', 's.id')
                                    ->whereSocio_id($this->id)
                                    ->where('fecha_prestacion','<=',$fecha)
                                    ->orderBy('fecha_prestacion','desc')
                                    ->first();

          $posterior = Asignacion::join('servicios_contratados as sc' , 'servicio_contratado_id', 'sc.id')
                                    ->join('servicios as s', 'servicio_id', 's.id')
                                    ->whereSocio_id($this->id)
                                    ->where('fecha_prestacion','>=',$fecha)
                                    ->orderBy('fecha_prestacion','asc')
                                    ->first();

          if (is_null($anterior) && is_null($posterior)) {
            return true;
          }

          if (!is_null($anterior)) {
              if (!is_null($servicio->duracion)) {
                $prestacion = new \DateTime($fecha);
                $fin = new \DateTime($anterior->fecha_prestacion);
                if ($prestacion->diff($fin)->format('%a') < $servicio->duracion) {
                    return false;
                }
              }
          }

          if (!is_null($posterior)) {
              if (!is_null($servicio->duracion)) {
                $prestacion = new \DateTime($fecha);
                $fin = new \DateTime($posterior->fecha_prestacion);
                if ($fin->diff($prestacion)->format('%a') < $servicio->duracion) {
                    return false;
                }
              }
          }

          return true;
        }
}
