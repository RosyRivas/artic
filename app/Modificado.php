<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modificado extends Model
{
    public $timestamps = false;

    protected $fillable = [ 'tabla', 'id_modificado', 'clave', 'valor_anterior',
                            'valor_nuevo', 'fecha', 'user_id' ];
}
