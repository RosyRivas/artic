<?php

namespace App;

class Servicio extends Auditable
{
  public $timestamps = false;
  protected $table = 'servicios';
  protected $fillable = ['nombre', 'descripcion','duracion','servicio_dia','costo', 'esta_activo', 'jefatura_id', 'imagen','socio_id', 'grupo_id' ];

  //Relacion con Jefatura
    public function jefatura(){
        return $this->belongsTo('App\Jefatura');
    }
  // Relacion con Recursos
    public function recursos(){
        return $this->hasMany('App\Recurso');
    }

    public function costo(){
        return number_format($this->costo, 2, ',', '.');
    }
    // Relacion muchos a muchos con Socios
    public function socios(){
          return $this->belongsToMany('App\Socio');
      }

    // Relacion muchos a muchos con grupo
    public function grupos(){
          return $this->belongsToMany('App\Grupo');
      }

    public function esta_disponible($fecha){
      $hay_prestador = false;

      //buscamos $hay_prestador
      // primero socios
      foreach ($this->socios as $s) {
          if ($s->esta_disponible($fecha,$this)){
              $hay_prestador = true;
              break;
          }
      }
        //si no hay prestador nos fijamos luego los grupos
      if(!$hay_prestador){
        foreach ($this->grupos as $g) {
            if ($g->esta_disponible($fecha,$this)){
                $hay_prestador = true;
                break;
            }
        }
      }

      if (!$hay_prestador) {
        return false;
      }

      foreach($this->recursos as $r){
          if (! $r->esta_disponible($fecha))
              return false;
      }

      return true;
    }
}
