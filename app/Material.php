<?php

namespace App;

class Material extends Auditable
{
  public $timestamps = false;
  protected $table = 'materiales';
  protected $fillable = ['nombre', 'descripcion','cantidad' ,'tipo_material_id',];

  public function tipoMaterial(){
        return $this->belongsTo('App\TipoMaterial');
    }

}
