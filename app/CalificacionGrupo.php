<?php

namespace App;

class CalificacionGrupo extends Auditable
{
    protected $table ='calificaciones_grupos';
    public $timestamps = false;
    protected $fillable = ['grupo_id','servicio_id','calificacion'];

    public function grupo(){
            return $this->belongsTo('App\Grupo');
      }
    public function servicio(){
            return $this->belongsTo('App\Servicio');
      }

}
