<?php

namespace App;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
class Recurso extends Auditable
{
  public $timestamps = false;

  protected $fillable = ['cantidad','servicio_id','socio_id','material_id'];

  public function material(){
        return $this->belongsTo('App\Material');
    }

    public function esta_disponible($fecha){
        // ultima asignaciont
        $fecha=new Carbon(new \DateTime($fecha), new \DateTimeZone('America/Argentina/Buenos_Aires'));
        $fecha3 = $fecha->addDays(1)->format('Y-m-d');
        $fecha1= $fecha->addDays(-4)->format('Y-m-d');

        $prestamos = RecursoPrestado::whereMaterial_id($this->material_id)
                                  ->where('fecha_prestamo','>',$fecha1)
                                  ->where('fecha_prestamo','<=',$fecha3)
                                  ->get()->sum('cantidad');

        if ($prestamos >= $this->cantidad) {
              return false;
        }

        $fecha2= $fecha->addDays(8)->format('Y-m-d');

        $prestamos = RecursoPrestado::whereMaterial_id($this->material_id)
                                  ->where('fecha_prestamo','<',$fecha2)
                                  ->where('fecha_prestamo','>=',$fecha3)
                                  ->get()->sum('cantidad');

        if ($prestamos >= $this->cantidad) {
              return false;
        }

        return true;
      }
}
