<?php

namespace App;

class Jefatura extends Auditable
{
    public $timestamps = false;
    /* protected $table = 'peces';  manera de declarar solo cuando el plural es especial
    */
    protected $fillable = ['nombre','porcentaje_senia'];

}
