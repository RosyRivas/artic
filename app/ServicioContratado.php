<?php

namespace App;

use Illuminate\Support\Facades\Mail;
use App\Mail\AvisoAsignacion;
use App\Mail\AsignacionForzada;



class ServicioContratado extends Auditable
{
  public $timestamps = false;
  protected $table= 'servicios_contratados';
  protected $fillable = ['fecha_prestacion','terminado','pedido_id','servicio_id'];

    public function servicio(){
        return $this->belongsTo('App\Servicio');
      }

    public function pedido(){
          return $this->belongsTo('App\Pedido');
      }
    public function asignacion(){
          return $this->hasOne('App\Asignacion');
      }

    public function fecha_prestacion(){
      return date("d/m/Y",strtotime($this->fecha_prestacion));
      }

    public function asignar(){

      $prestadores= 0;
      // asignaciones rechazadas por socios
      $rechazos_s = Asignacion::whereServicio_contratado_id($this->id)
                                ->whereAceptado(false)
                                ->whereEs_grupo(false)
                                ->pluck('socio_id');

      $prestadores+= Socio::select('socios.*')
                            ->join('calificaciones_socios as cs', 'socios.id' , 'cs.socio_id')
                            ->whereServicio_id($this->servicio_id)
                            ->whereNotIn('socio_id', $rechazos_s)
                            ->count();

      $mejor_socio = Socio::select('socios.*')
                            ->join('calificaciones_socios as cs', 'socios.id' , 'cs.socio_id')
                            ->whereServicio_id($this->servicio_id)
                            ->whereNotIn('socio_id', $rechazos_s)
                            ->orderBy('calificacion', 'desc')->first();

      // asignaciones rechazadas por grupos
      $rechazos_g = Asignacion::whereServicio_contratado_id($this->id)
                                ->whereAceptado(false)
                                ->whereEs_grupo(true)
                                ->pluck('grupo_id');
      $prestadores+= Grupo::join('calificaciones_grupos as cg', 'grupos.id' , 'cg.grupo_id')
                                ->whereServicio_id($this->servicio_id)
                                ->whereNotIn('grupo_id', $rechazos_s)
                                ->count();
      $mejor_grupo = Grupo::join('calificaciones_grupos as cg', 'grupos.id' , 'cg.grupo_id')
                            ->whereServicio_id($this->servicio_id)
                            ->whereNotIn('grupo_id', $rechazos_s)
                            ->orderBy('calificacion', 'desc')
                            ->first();
                            //dd($prestadores);
        if ($prestadores == 1) {
            if(!is_null($mejor_grupo)){
              $a = Asignacion::create([
                    'es_grupo' => true,
                    'aceptado' => true,
                    'servicio_contratado_id' => $this->id,
                    'grupo_id' => $mejor_grupo->id,
              ]);

              Mail::to($mejor_grupo->socios[0]->user)->send(new AsignacionForzada($a));

              return;
          }

          if(!is_null($mejor_socio)){
            $a = Asignacion::create([
                  'es_grupo' => false,
                  'aceptado' => true,
                  'servicio_contratado_id' => $this->id,
                  'socio_id' => $mejor_socio->id,
            ]);

            Mail::to($mejor_socio->user)->send(new AsignacionForzada($a));

            return;
          }
        }
     // dd([$mejor_socio, $mejor_grupo]);
     // dd($mejor_grupo);
      if(is_null($mejor_socio)){
          if(!is_null($mejor_grupo)){
            $a = Asignacion::create([
                  'es_grupo' => true,
                  'servicio_contratado_id' => $this->id,
                  'grupo_id' => $mejor_grupo->id,
            ]);

            Mail::to($mejor_grupo->socios[0]->user)->send(new AvisoAsignacion($a));

            return;
          }
      }

      if(is_null($mejor_grupo)){
          if(!is_null($mejor_socio)){
            $a = Asignacion::create([
                  'es_grupo' => false,
                  'servicio_contratado_id' => $this->id,
                  'socio_id' => $mejor_socio->id,
            ]);

            Mail::to($mejor_socio->user)->send(new AvisoAsignacion($a));

            return;
          }
      }
      //dd('kiki');
      if($mejor_socio->calificacion > $mejor_grupo->calificacion){
          $a = Asignacion::create([
                'es_grupo' => false,
                'servicio_contratado_id' => $this->id,
                'socio_id' => $mejor_socio->id,
          ]);

          Mail::to($mejor_socio->user)->send(new AvisoAsignacion($a));

          return;
      }else{
        $a = Asignacion::create([
              'es_grupo' => true,
              'servicio_contratado_id' => $this->id,
              'grupo_id' => $mejor_grupo->id,
        ]);

        Mail::to($mejor_grupo->socios[0]->user)->send(new AvisoAsignacion($a));

        return;
      }
    }

}
