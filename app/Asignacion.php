<?php

namespace App;
use App\RecursoPrestado;

class Asignacion extends Auditable
{
  public $timestamps = false;
    protected $table= 'asignaciones';
  protected $fillable = [ 'aceptado', 'es_grupo', 'motivo', 'servicio_contratado_id',
                           'socio_id', 'grupo_id' ];

  public function servicio_contratado(){
       return $this->belongsTo('App\ServicioContratado');
   }

   public function socio(){
        return $this->belongsTo('App\Socio');
    }
    public function grupo(){
         return $this->belongsTo('App\Grupo');
     }

     public function calificacion(){
             return $this->hasOne('App\Calificacion');
         }

    public function recursos_prestados(){
        $socio= $this->socio;
        $recursos = RecursoPrestado::whereResponsable_id($socio->id)
                                    ->whereServicio_id($this->servicio_contratado->servicio_id)
                                    ->whereFecha_prestamo($this->servicio_contratado->fecha_prestacion)
                                    ->get();
                                    //dd($recursos);
        return $recursos;

    }
}
