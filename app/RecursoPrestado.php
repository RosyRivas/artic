<?php

namespace App;



class RecursoPrestado extends Auditable
{
  public $timestamps = false;
  protected $table ='recursos_prestados';
  protected $fillable = ['fecha_prestamo','cantidad','material_id','servicio_id','responsable_id','estado_devolucion', 'esta_devuelto','fecha_devolucion'];



  public function material(){
        return $this->belongsTo('App\Material');
    }

  public function responsable(){
        return $this->belongsTo('App\Socio');
    }

  public function servicios(){
        return $this->belongsTo('App\Servicio');
    }



}
