<?php

namespace App;

class TipoMaterial extends Auditable
{
      public $timestamps = false;

      protected $table = 'tipos_materiales'; // manera de declarar solo cuando el plural es especial
      protected $fillable = ['nombre', 'descripcion'];
}
