<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Notifications\Notifiable;

class User extends UserAuditable
{
    use Notifiable;
    public $timestamps = false;
    protected $fillable = [
        'name','apellido', 'email', 'password','saldo', 'rol_id',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];


    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function rol(){
          return $this->belongsTo('App\Rol');
    }
    public function pedidos(){
          return $this->hasMany('App\Pedido');
    }
    public function socio(){
          return $this->hasOne('App\Socio');
    }
}
